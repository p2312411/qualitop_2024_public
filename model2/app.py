import numpy as np
import pandas as pd
from flask import Flask, request, jsonify
from sklearn.neighbors import NearestNeighbors
from sklearn.preprocessing import StandardScaler

from features_extraction import extraction
from features_extraction import features_code
from logger import setup_logger

logger = setup_logger("logs/subtypes.log")

app = Flask(__name__)


@app.route('/subtypes', methods=['POST'])
def predict_subtypes():
    data = request.json

    logger.info(f"Received subtypes request with data: {data}")

    age = data['age']
    year = 2024 - int(age)
    genre = data['sex']
    height = int(data['height'])
    weight = int(data['weight'])
    b_symptoms = int(data['b_symptoms'])
    lactate = int(data['lactate'])
    c_reactive_protein = float(data['c_reactive_protein'])
    hemoglobin = float(data['hemoglobin'])
    white_blood_cell = float(data['white_blood_cell'])
    lymphocites = float(data['lymphocites'])
    red_blood_cell = float(data['red_blood_cell'])
    glucose = int(data['glucose'])

    new_patient = {
        "Year of Birth": year,
        "Height": height,
        "Sex": genre,
        "B-symptoms?": b_symptoms,
        "Weight": weight,
        "ldh": lactate,
        "crp": c_reactive_protein,
        "HB": hemoglobin,
        "Leukocytes": white_blood_cell,
        "Lymphocites": lymphocites,
        "RBC": red_blood_cell,
        "Glucose": glucose
    }

    dfP = pd.read_excel('./data/portugal_data_clinical_pretraitement.xlsx')
    dfQ = pd.read_excel('./data/portugal_data_question_pretraitement.xlsx')
    dfP_input = dfP[
        ["Year of Birth", "Height", "Sex", "B-symptoms?", "Weight", "ldh", "crp", "HB", "Leukocytes", "Lymphocites",
         "RBC",
         "Glucose"]]

    dfP_input = dfP_input._append(new_patient, ignore_index=True)

    X = StandardScaler().fit_transform(dfP_input)
    nbrs = NearestNeighbors(n_neighbors=2, algorithm='brute').fit(X)
    distances, indices = nbrs.kneighbors(X)
    result = indices[np.any(indices == 15, axis=1)]
    for r in result[0]:
        if r != 15: nearest_patient = r
    dfP["Class"] = np.load('./clusters/patients/3D.npy')[5][2]
    cluster = dfP["Class"][nearest_patient]

    salient_features = extraction(cluster)
    dict = features_code()

    result = {}
    for i in range(len(salient_features["Salient Features"].to_list())):
        keys = [key for key in dict[salient_features["Salient Features"].iloc[i]]]
        keys.sort()
        if salient_features["Value"].iloc[i] == "High":
            value = dict[salient_features["Salient Features"].iloc[i]][keys[len(keys) - 1]]
        else:
            value = dict[salient_features["Salient Features"].iloc[i]][keys[0]]
        result[salient_features["Salient Features"].iloc[i]] = value

    logger.info(f"Successful prediction subtypes: {result}")
    return jsonify({"subtypes": str(result)})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8002)
