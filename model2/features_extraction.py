import pandas as pd

def extraction(cluster):
    df_features=pd.read_excel('./data/salient_features.xlsx')
    result=df_features[df_features["Cluster"]=="Cluster "+str(cluster)]
    for i in range(len(result["Salient Features"].to_list())):
        if result["Salient Features"].iloc[i].find("+")!=-1:
            result["Salient Features"].iloc[i]=result["Salient Features"].iloc[i].strip("+")
        if result["Salient Features"].iloc[i].find("-")!=-1:
            result["Salient Features"].iloc[i]=result["Salient Features"].iloc[i].strip("-")
    return result

def features_code():
    df_features=pd.read_excel('./data/portugal_data_quest_final.xlsx')
    df_features_code=pd.read_excel('./data/portugal_data_question_pretraitement.xlsx')

    dict={}
    for c in df_features.columns:
        dict_feature={}
        code=df_features_code[c]
        string=df_features[c]
        for i in range(len(code)):
            dict_feature.update({int(code[i]):string[i]})
        dict[c]=dict_feature
    return dict

