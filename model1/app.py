import subprocess

from flask import Flask, request, jsonify
from flask_cors import CORS, cross_origin

from logger import setup_logger

logger = setup_logger("logs/factg.log")

app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": "*"}}, supports_credentials=True)
app.config['CORS_HEADERS'] = 'Content-Type'


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', '*')
    response.headers.add('Access-Control-Allow-Methods', '*')
    response.headers.add('Access-Control-Allow-Credentials', 'true')
    return response


@cross_origin(origin='*')
@app.route('/predict_FACT_G_IUS', methods=['POST'])
def predict_FACT_G_IUS():
    data = request.json

    logger.info(f"Received FACT_G_IUS request with data: {data}")

    IUS_score = data['ius_score']
    age_at_immunotherapy = data['age_at_immunotherapy']
    sex = data['sex']
    cancer_type = data['cancer_type']
    education_level = data['education_level']
    financial_status = data['financial_status']

    # Appeler le script R
    result = subprocess.run(
        ['Rscript', 'models/calcul_FACT_G_IUS.R', str(IUS_score), str(age_at_immunotherapy), str(sex), cancer_type,
         str(education_level), str(financial_status)],
        capture_output=True,
        text=True
    )

    # Obtenir le résultat du script R
    FACT_G_IUS = result.stdout.strip()

    logger.info(f"Successful prediction for fact-G with IUS score: {FACT_G_IUS}")

    return jsonify({'FACT_G_with_Intolerence_to_Uncertainity Score ': str(FACT_G_IUS)})


############################################################################################################
# Modele 2: Effect of Doctor-Patient relationship score on total FACT-G score adjusted for age at immunotherapy, sex, cancer type, education and financial status
@cross_origin(origin='*')
@app.route('/predict_FACT_G_DPR', methods=['POST'])
def predict_FACT_G_DPR():
    data = request.json

    logger.info(f"Received FACT_G_DPR request with data: {data}")

    DPR_score = data['dpr_score']
    age_at_immunotherapy = data['age_at_immunotherapy']
    sex = data['sex']
    cancer_type = data['cancer_type']
    education_level = data['education_level']
    financial_status = data['financial_status']

    # Appeler le script R pour le deuxième modèle
    result = subprocess.run(
        ['Rscript', 'models/calcul_FACT_G_DPR.R', str(DPR_score), str(age_at_immunotherapy), str(sex), cancer_type,
         str(education_level), str(financial_status)],
        capture_output=True,
        text=True
    )

    # Obtenir le résultat du script R
    FACT_G_DPR = result.stdout.strip()

    logger.info(f"Successful prediction for fact-G with DPR score: {FACT_G_DPR}")

    return jsonify({'FACT_G_with_Doctor_Patient_relationship_score ': str(FACT_G_DPR)})


############################################################################################################
# Modele 3 : Effect of Treatment related Hope score on total FACT-G score adjusted for age at immunotherapy, sex, cancer type, education and financial status
@cross_origin(origin='*')
@app.route('/predict_FACT_G_TRH', methods=['POST'])
def predict_FACT_G_TRH():
    data = request.json

    logger.info(f"Received FACT_G_TRH request with data: {data}")

    TRH_score = data['trh_score']
    age_at_immunotherapy = data['age_at_immunotherapy']
    sex = data['sex']
    cancer_type = data['cancer_type']
    education_level = data['education_level']
    financial_status = data['financial_status']

    # Appeler le script R pour le 3eme modèle
    result = subprocess.run(
        ['Rscript', 'models/calcul_FACT_G_TRH.R', str(TRH_score), str(age_at_immunotherapy), str(sex), cancer_type,
         str(education_level), str(financial_status)],
        capture_output=True,
        text=True
    )

    # Obtenir le résultat du script R
    FACT_G_TRH = result.stdout.strip()

    logger.info(f"Successful prediction for fact-G with TRH score: {FACT_G_TRH}")

    return jsonify({'FACT_G_with_Treatment_related_Hope_score ': str(FACT_G_TRH)})


############################################################################################################
# Modele 4 : Effect of Health Literacy score on total FACT-G score adjusted for age at immunotherapy, sex, cancer type, education and financial status
@cross_origin(origin='*')
@app.route('/predict_FACT_G_HLS', methods=['POST'])
def predict_FACT_G_HLS():
    data = request.json

    logger.info(f"Received FACT_G_HLS request with data: {data}")

    HLS_score = data['hls_score']
    age_at_immunotherapy = data['age_at_immunotherapy']
    sex = data['sex']
    cancer_type = data['cancer_type']
    education_level = data['education_level']
    financial_status = data['financial_status']

    # Appeler le script R pour le 4eme modèle
    result = subprocess.run(
        ['Rscript', 'models/calcul_FACT_G_HLS.R', str(HLS_score), str(age_at_immunotherapy), str(sex), cancer_type,
         str(education_level), str(financial_status)],
        capture_output=True,
        text=True
    )

    # Obtenir le résultat du script R
    FACT_G_HLS = result.stdout.strip()

    logger.info(f"Successful prediction for fact-G with HLS score: {FACT_G_HLS}")

    return jsonify({'FACT_G_with_Health_Literacy_score ': str(FACT_G_HLS)})


############################################################################################################
# Modele 5 : Effect of total social support on total FACT-G score adjusted for age at immunotherapy, sex, cancer type, education and financial status
@cross_origin(origin='*')
@app.route('/predict_FACT_G_TSS', methods=['POST'])
def predict_FACT_G_TSS():
    data = request.json

    logger.info(f"Received FACT_G_TSS request with data: {data}")

    TSS_score = data['tss_score']
    age_at_immunotherapy = data['age_at_immunotherapy']
    sex = data['sex']
    cancer_type = data['cancer_type']
    education_level = data['education_level']
    financial_status = data['financial_status']

    # Appeler le script R pour le 5eme modèle
    result = subprocess.run(
        ['Rscript', 'models/calcul_FACT_G_TSS.R', str(TSS_score), str(age_at_immunotherapy), str(sex), cancer_type,
         str(education_level), str(financial_status)],
        capture_output=True,
        text=True
    )

    # Obtenir le résultat du script R
    FACT_G_TSS = result.stdout.strip()

    logger.info(f"Successful prediction for fact-G with TSS score: {FACT_G_TSS}")

    return jsonify({'FACT_G_with_Total_Social_Support_score ': str(FACT_G_TSS)})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8001)
