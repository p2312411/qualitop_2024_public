# calcul_FACT_G_IUS.R

args <- commandArgs(trailingOnly = TRUE)

IUS_score <- as.numeric(args[1])
age_at_immunotherapy <- as.numeric(args[2])
sex <- as.numeric(args[3])
cancer_type <- args[4]
education_level <- as.numeric(args[5])
financial_status <- as.numeric(args[6])

#Modele 1 : Effect of total Intolerence to Uncertainity Score (IUS) on total FACT-G score adjusted for age at immunotherapy, sex, cancer type, education and financial status
calculate_FACT_G_IUS <- function(age_at_immunotherapy, cancer_type, sex, education_level, financial_status, IUS_score) {
    intercept <- 102.58
    coeff_age <- -0.05
    coeff_cancer_melanoma <- 0  # Référence
    coeff_cancer_lung <- -9.09
    coeff_cancer_lymphoma <- -11.40
    coeff_cancer_other <- -3.29
    coeff_sex <- 2.73
    coeff_education <- -2.04
    coeff_financial_status <- -4.99
    coeff_IUS <- -0.57
    
    FACT_G_IUS <- intercept +
             coeff_age * (age_at_immunotherapy / 10) +
             ifelse(cancer_type == "Lymphoma", coeff_cancer_lymphoma,
                    ifelse(cancer_type == "Lung", coeff_cancer_lung,
                           ifelse(cancer_type == "Other", coeff_cancer_other, 0))) +
             coeff_sex * sex +
             coeff_education * education_level +
             coeff_financial_status * financial_status +
             coeff_IUS * IUS_score 
    
    return(FACT_G_IUS)
}



result <- calculate_FACT_G_IUS(age_at_immunotherapy, cancer_type, sex, education_level, financial_status, IUS_score)
cat(result)
