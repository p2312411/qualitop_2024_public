/*
 * Copyright 2019-2023 The Polypheny Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.polypheny.security.mlapi.cmapi.utils;

import io.javalin.http.GatewayTimeoutResponse;
import io.javalin.http.InternalServerErrorResponse;
import org.polypheny.security.mlapi.cmapi.dto.CmApiResult;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class CmUtils {
    public static CmApiResult callCmApi() {
        final HttpClient httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2).connectTimeout(Duration.ofSeconds(20)).build();
        HttpRequest httpRequest = HttpRequest.newBuilder().POST(HttpRequest.BodyPublishers.ofString("{}")).uri(URI.create(System.getenv("CM_API_URL"))).setHeader("Content-Type", "application/json").build();
        CompletableFuture<HttpResponse<String>> httpResponse = null;
        httpResponse = httpClient.sendAsync(httpRequest, HttpResponse.BodyHandlers.ofString());
        String resultBody;
        int resultStatusCode = 0;
        try {
            resultBody = httpResponse.thenApply(HttpResponse::body).get(20, TimeUnit.SECONDS);
            httpResponse.join();
            resultStatusCode = httpResponse.thenApply(HttpResponse::statusCode).get(20, TimeUnit.SECONDS);
            if (resultStatusCode == 200) {
                return new CmApiResult(resultBody);
            } else {
                throw new InternalServerErrorResponse("CM API is unreachable");
            }
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            throw new GatewayTimeoutResponse("An error occurred when trying to call CM API");
        }
    }
}
