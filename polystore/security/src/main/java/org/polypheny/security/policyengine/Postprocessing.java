/*
 * Copyright 2019-2023 The Polypheny Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.polypheny.security.policyengine;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.polypheny.security.policyengine.dto.NbTuplePolicy;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * A class for postprocessing a polySQL query
 * Access control policies are stored in JSON files
 * The JSON files are located  in the folder policy-files
 */
public class Postprocessing {
    private static Postprocessing instance;
    private final ArrayList<NbTuplePolicy> nbTuplePolicies = new ArrayList<>();

    //Singleton instance
    public static Postprocessing getInstance() {
        synchronized (Postprocessing.class) {
            if (instance == null) {
                instance = new Postprocessing();
            }
            return instance;
        }
    }

    private Postprocessing() {
        // Read from json files
        JSONParser jsonParser = new JSONParser();
        try (FileReader reader = new FileReader("./policy-files/dml_policies.json")) {
            Object parser = jsonParser.parse(reader);
            JSONArray rules = (JSONArray) parser;
            //Iterate over source array
            rules.forEach(item -> {
                JSONObject rule = (JSONObject) item;
                String ruleType = (String) rule.get("rule_type");
                if (ruleType != null) {
                    switch (ruleType) {
                        case "nb_tuple":
                            String source = (String) rule.get("source");
                            String nb = (String) rule.get("nb");
                            NbTuplePolicy policy = new NbTuplePolicy(source, Integer.parseInt(nb));
                            nbTuplePolicies.add(policy);
                            break;
                    }
                }
            });
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    public boolean validateQuery(ArrayList<String> from, String[][] data) {
        return validateNbTuplesPolicies(from, data);
    }

    private boolean validateNbTuplesPolicies(ArrayList<String> from, String[][] data) {
        // Check if the result of the execution of the query violates nb tuples policies
        for (int i = 0; i < nbTuplePolicies.size(); i++) {
            NbTuplePolicy policy = nbTuplePolicies.get(i);
            if (from.contains(policy.getSource())) {
                if (data.length < policy.getNb()) {
                    return false;
                }
            }
        }
        return true;
    }
}
