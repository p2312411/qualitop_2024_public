mongod --config ./mongod.conf --fork --logpath /var/log/mongodb/mongod.log

sleep 5

mongosh admin --eval 'db.createUser({ user: "admin", pwd: "adminpassword", roles: [{ role: "userAdminAnyDatabase", db: "admin" }]})'
mongosh admin --eval 'db.createUser({ user: "qualitop", pwd: "qualitoppassword", roles: [{ role: "readWrite", db: "qualitop_2024_1" }]})'
mongod --shutdown

cat <<EOT >> ./mongod.conf
security:
  authorization: enabled
EOT

mongod --config ./mongod.conf --fork --logpath /var/log/mongodb/mongod.log

sleep 5

tail -f /dev/null