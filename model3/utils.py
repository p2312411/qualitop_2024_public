import joblib
from sklearn.preprocessing import StandardScaler


def load_model(filename):
    model, columns, scaler, top_features = joblib.load(filename)
    return model, columns, scaler, top_features
