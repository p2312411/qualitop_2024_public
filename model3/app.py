import pandas as pd
from flask import Flask, request, jsonify
from flask_cors import CORS, cross_origin

from logger import setup_logger
from utils import load_model

logger = setup_logger("logs/saillant.log")

app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": "*"}}, supports_credentials=True)
app.config['CORS_HEADERS'] = 'Content-Type'


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', '*')
    response.headers.add('Access-Control-Allow-Methods', '*')
    response.headers.add('Access-Control-Allow-Credentials', 'true')
    return response


@cross_origin(origin='*')
@app.route('/relationship_with_drug', methods=['POST'])
def predict_relationship_with_drug():
    data = request.json

    logger.info(f"Received relationship_with_drug prediction request : {data}")

    best_response_otherM = float(data['best_response_otherM'])
    mean_medications = float(data['mean_medications'])
    date_of_birth = int(data["date_of_birth"])
    i_am_content_with_the_qual = float(data["i_am_content_with_the_qual"])
    thinking_back_to_the_yearn = float(data["thinking_back_to_the_yearn"])
    immuno_frequency = float(data["immuno_frequency"])
    mean_therapies = float(data["mean_therapies"])
    worrying_thoughts_go_throu = float(data["worrying_thoughts_go_throu"])
    thinking_back_to_the_yearh = float(data["thinking_back_to_the_yearh"])
    i_get_a_sort_of_frightened = float(data["i_get_a_sort_of_frightened"])

    new_data = pd.DataFrame({
        'best_response_otherM': [best_response_otherM],
        'mean_medications': [mean_medications],
        'date_of_birth': [date_of_birth],
        'i_am_content_with_the_qual': [i_am_content_with_the_qual],
        'thinking_back_to_the_yearn': [thinking_back_to_the_yearn],
        'mean_therapies': [mean_therapies],
        'immuno_frequency': [immuno_frequency],
        'worrying_thoughts_go_throu': [worrying_thoughts_go_throu],
        'thinking_back_to_the_yearh': [thinking_back_to_the_yearh],
        'i_get_a_sort_of_frightened': [i_get_a_sort_of_frightened]
    })

    model_name = "models/RandomForest_y_relationship_with_drug_nbM_KNN_Imputation.joblib"
    model, columns, scaler, top_features = load_model(model_name)

    # Ajouter les colonnes manquantes avec des valeurs par défaut
    for col in columns:
        if col not in new_data.columns:
            new_data[col] = 0  # ou une valeur par défaut appropriée

    # Réordonner les colonnes pour correspondre à l'ordre attendu
    new_data = new_data[columns]

    # Normaliser les nouvelles données en utilisant le scaler sauvegardé
    new_data_scaled = pd.DataFrame(scaler.transform(new_data), columns=new_data.columns)

    # Sélectionner uniquement les caractéristiques importantes pour la prédiction
    new_data_top_features = new_data_scaled[top_features]

    # Faire une prédiction
    relationship_with_drug_nbM = model.predict(new_data_top_features)

    logger.info(f"Successful prediction for relationship_with_drug : {relationship_with_drug_nbM[0]}")

    return jsonify({'relationship_with_drug_nbM': str(relationship_with_drug_nbM[0])})


@cross_origin(origin='*')
@app.route('/followup', methods=['POST'])
def predict_followup():
    data = request.json

    logger.info(f"Received followup prediction request : {data}")

    best_response_otherM = float(data['best_response_otherM'])
    mean_medications = float(data['mean_medications'])
    date_of_birth = int(data["date_of_birth"])
    i_am_content_with_the_qual = float(data["i_am_content_with_the_qual"])
    thinking_back_to_the_yearn = float(data["thinking_back_to_the_yearn"])
    immuno_frequency = float(data["immuno_frequency"])
    mean_therapies = float(data["mean_therapies"])
    worrying_thoughts_go_throu = float(data["worrying_thoughts_go_throu"])
    thinking_back_to_the_yearh = float(data["thinking_back_to_the_yearh"])
    i_get_a_sort_of_frightened = float(data["i_get_a_sort_of_frightened"])

    new_data = pd.DataFrame({
        'best_response_otherM': [best_response_otherM],
        'mean_medications': [mean_medications],
        'date_of_birth': [date_of_birth],
        'i_am_content_with_the_qual': [i_am_content_with_the_qual],
        'thinking_back_to_the_yearn': [thinking_back_to_the_yearn],
        'mean_therapies': [mean_therapies],
        'immuno_frequency': [immuno_frequency],
        'worrying_thoughts_go_throu': [worrying_thoughts_go_throu],
        'thinking_back_to_the_yearh': [thinking_back_to_the_yearh],
        'i_get_a_sort_of_frightened': [i_get_a_sort_of_frightened]
    })

    model_name = "models/RandomForest_y_followupM_KNN_Imputation.joblib"
    model, columns, scaler, top_features = load_model(model_name)

    # Ajouter les colonnes manquantes avec des valeurs par défaut
    for col in columns:
        if col not in new_data.columns:
            new_data[col] = 0  # ou une valeur par défaut appropriée

    # Réordonner les colonnes pour correspondre à l'ordre attendu
    new_data = new_data[columns]

    # Normaliser les nouvelles données en utilisant le scaler sauvegardé
    new_data_scaled = pd.DataFrame(scaler.transform(new_data), columns=new_data.columns)

    # Sélectionner uniquement les caractéristiques importantes pour la prédiction
    new_data_top_features = new_data_scaled[top_features]

    # Faire une prédiction
    followupM = model.predict(new_data_top_features)

    logger.info(f"Successful prediction for followupM : {followupM[0]}")

    return jsonify({'followupM': str(followupM[0])})


@cross_origin(origin='*')
@app.route('/content_qual', methods=['POST'])
def predict_content_qual():
    data = request.json

    logger.info(f"Received content_qual prediction request : {data}")

    best_response_otherM = float(data['best_response_otherM'])
    mean_medications = float(data['mean_medications'])
    date_of_birth = int(data["date_of_birth"])
    i_am_content_with_the_qual = float(data["i_am_content_with_the_qual"])
    thinking_back_to_the_yearn = float(data["thinking_back_to_the_yearn"])
    immuno_frequency = float(data["immuno_frequency"])
    mean_therapies = float(data["mean_therapies"])
    worrying_thoughts_go_throu = float(data["worrying_thoughts_go_throu"])
    thinking_back_to_the_yearh = float(data["thinking_back_to_the_yearh"])
    i_get_a_sort_of_frightened = float(data["i_get_a_sort_of_frightened"])

    new_data = pd.DataFrame({
        'best_response_otherM': [best_response_otherM],
        'mean_medications': [mean_medications],
        'date_of_birth': [date_of_birth],
        'i_am_content_with_the_qual': [i_am_content_with_the_qual],
        'thinking_back_to_the_yearn': [thinking_back_to_the_yearn],
        'mean_therapies': [mean_therapies],
        'immuno_frequency': [immuno_frequency],
        'worrying_thoughts_go_throu': [worrying_thoughts_go_throu],
        'thinking_back_to_the_yearh': [thinking_back_to_the_yearh],
        'i_get_a_sort_of_frightened': [i_get_a_sort_of_frightened]
    })

    model_name = "models/GBM_y_i_am_content_with_the_qualT_KNN_Imputation.joblib"
    model, columns, scaler, top_features = load_model(model_name)

    # Ajouter les colonnes manquantes avec des valeurs par défaut
    for col in columns:
        if col not in new_data.columns:
            new_data[col] = 0  # ou une valeur par défaut appropriée

    # Réordonner les colonnes pour correspondre à l'ordre attendu
    new_data = new_data[columns]

    # Normaliser les nouvelles données en utilisant le scaler sauvegardé
    new_data_scaled = pd.DataFrame(scaler.transform(new_data), columns=new_data.columns)

    # Sélectionner uniquement les caractéristiques importantes pour la prédiction
    new_data_top_features = new_data_scaled[top_features]

    # Faire une prédiction
    i_am_content_with_the_qualT = model.predict(new_data_top_features)

    logger.info(f"Successful prediction for i_am_content_with_the_qualT: {i_am_content_with_the_qualT[0]}")

    return jsonify({'i_am_content_with_the_qualT': str(i_am_content_with_the_qualT[0])})


@cross_origin(origin='*')
@app.route('/predict_saillant', methods=['POST'])
def predict_saillant():
    data = request.json

    logger.info(f"Received predict_saillant prediction request : {data}")

    best_response_otherM = float(data['best_response_otherM'])
    mean_medications = float(data['mean_medications'])
    date_of_birth = int(data["date_of_birth"])
    i_am_content_with_the_qual = float(data["i_am_content_with_the_qual"])
    thinking_back_to_the_yearn = float(data["thinking_back_to_the_yearn"])
    immuno_frequency = float(data["immuno_frequency"])
    mean_therapies = float(data["mean_therapies"])
    worrying_thoughts_go_throu = float(data["worrying_thoughts_go_throu"])
    thinking_back_to_the_yearh = float(data["thinking_back_to_the_yearh"])
    i_get_a_sort_of_frightened = float(data["i_get_a_sort_of_frightened"])

    new_data = pd.DataFrame({
        'best_response_otherM': [best_response_otherM],
        'mean_medications': [mean_medications],
        'date_of_birth': [date_of_birth],
        'i_am_content_with_the_qual': [i_am_content_with_the_qual],
        'thinking_back_to_the_yearn': [thinking_back_to_the_yearn],
        'mean_therapies': [mean_therapies],
        'immuno_frequency': [immuno_frequency],
        'worrying_thoughts_go_throu': [worrying_thoughts_go_throu],
        'thinking_back_to_the_yearh': [thinking_back_to_the_yearh],
        'i_get_a_sort_of_frightened': [i_get_a_sort_of_frightened]
    })

    model_name = "models/RandomForest_y_relationship_with_drug_nbM_KNN_Imputation.joblib"
    model, columns, scaler, top_features = load_model(model_name)

    # Ajouter les colonnes manquantes avec des valeurs par défaut
    for col in columns:
        if col not in new_data.columns:
            new_data[col] = 0  # ou une valeur par défaut appropriée

    # Réordonner les colonnes pour correspondre à l'ordre attendu
    new_data = new_data[columns]

    # Normaliser les nouvelles données en utilisant le scaler sauvegardé
    new_data_scaled = pd.DataFrame(scaler.transform(new_data), columns=new_data.columns)

    # Sélectionner uniquement les caractéristiques importantes pour la prédiction
    new_data_top_features = new_data_scaled[top_features]

    # Faire une prédiction
    relationship_with_drug_nbM = model.predict(new_data_top_features)

    model_name = "models/RandomForest_y_followupM_KNN_Imputation.joblib"
    model, columns, scaler, top_features = load_model(model_name)

    # Ajouter les colonnes manquantes avec des valeurs par défaut
    for col in columns:
        if col not in new_data.columns:
            new_data[col] = 0  # ou une valeur par défaut appropriée

    # Réordonner les colonnes pour correspondre à l'ordre attendu
    new_data = new_data[columns]

    # Normaliser les nouvelles données en utilisant le scaler sauvegardé
    new_data_scaled = pd.DataFrame(scaler.transform(new_data), columns=new_data.columns)

    # Sélectionner uniquement les caractéristiques importantes pour la prédiction
    new_data_top_features = new_data_scaled[top_features]

    # Faire une prédiction
    followupM = model.predict(new_data_top_features)

    model_name = "models/GBM_y_i_am_content_with_the_qualT_KNN_Imputation.joblib"
    model, columns, scaler, top_features = load_model(model_name)

    # Ajouter les colonnes manquantes avec des valeurs par défaut
    for col in columns:
        if col not in new_data.columns:
            new_data[col] = 0  # ou une valeur par défaut appropriée

    # Réordonner les colonnes pour correspondre à l'ordre attendu
    new_data = new_data[columns]

    # Normaliser les nouvelles données en utilisant le scaler sauvegardé
    new_data_scaled = pd.DataFrame(scaler.transform(new_data), columns=new_data.columns)

    # Sélectionner uniquement les caractéristiques importantes pour la prédiction
    new_data_top_features = new_data_scaled[top_features]

    # Faire une prédiction
    i_am_content_with_the_qualT = model.predict(new_data_top_features)

    prediction_data = {
        'relationship_with_drug_nbM': relationship_with_drug_nbM[0],
        'followupM': str(followupM[0]),
        'i_am_content_with_the_qualT': str(i_am_content_with_the_qualT[0])
    }
    logger.info(f"Successful prediction for saillant prediction request: {prediction_data}")

    return jsonify({'relationship_with_drug_nbM ': str(relationship_with_drug_nbM[0]), "followupM": str(followupM[0]),
                    "i_am_content_with_the_qualT":
                        str(i_am_content_with_the_qualT[0])})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8003)
