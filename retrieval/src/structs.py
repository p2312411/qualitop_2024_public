from typing import Optional, List
from langchain_core.pydantic_v1 import BaseModel


# Sql query structure in JSON (where fields)
class WhereItems(BaseModel):
    attribute: Optional[str] = None
    operator: Optional[str] = None
    value: Optional[str] = None


# SQL query structure in JSON
class Query(BaseModel):
    models: Optional[List[str]] = ['fact_g_quality_of_life', 'model_2', 'model_3']
    sources: Optional[List[str]] = ['FR', 'ES', 'PT', 'NL']
    attributes: Optional[List[str]] = None
    where: Optional[List[WhereItems]] = None
    group_by: Optional[List[str]] = None
    order_by: Optional[List[str]] = None
    aggregate: Optional[List[str]] = None
    limit: Optional[List[str]] = None
