import json
from time import perf_counter

import requests
from flask import Flask, request, jsonify
from flask_cors import CORS
from groq import Groq
from langchain_community.chat_message_histories import ChatMessageHistory

from logger import setup_logger
from retrieval import Retrieval

logger = setup_logger("logs/factg.log")

app = Flask(__name__)
CORS(app)

llm = Retrieval()
llm.generate_embeddings()
llm.load_embeddings()


@app.route('/api/chat/parse', methods=['POST'])
def parse():
    """
    This function is called when the user sends a query to the model.
    It returns a JSON object describing the query.

    Returns
    -------
    answer : str
        answer to the query made by the user
    """

    logger.info(f"Received parse NL request with content: {str(request.json["query"])}")

    start = perf_counter()
    user_query = request.json["query"]
    user_id = request.json["username"]
    new_chat = request.json["new_chat"]
    chat_history = ChatMessageHistory()

    if new_chat == 0:
        llm.get_chat_history(user_id, chat_history)
    embeddings_df = llm.load_embeddings()
    ls_tables, ls_attributes = llm.semantic_similarity(user_query, embeddings_df)

    selected_api = llm.apis_selector(user_query)
    llm.log.info("selected models/apis : " + str(selected_api))

    if not selected_api:
        sql_query = llm.llm_nl2sql(user_query, ls_tables, llm.schema, chat_history)
        llm.log.info("sql_query : " + sql_query)

        final_query = llm.get_first_query(sql_query)
        llm.log.info("final_query : " + final_query)

        json_query = llm.llm_sql2json(final_query)
        llm.log.info("json_query : " + str(json_query))

    else:
        json_query = llm.llm_function_call(user_query, selected_api, ls_attributes)
        llm.log.info("json_query : " + str(json_query))

    end = perf_counter()
    time = end - start
    llm.log.info("Execution time : " + str(time))

    logger.info(f"Successful parsing for {str(request.json["query"])} with result : {str(json_query)} ( Execution "
                f"time {str(time)}")

    return jsonify(json_query)


@app.route('/api/chat/parse_v2', methods=['POST'])
def parse_v2():
    """
    This function is called when the user sends a query to the model.
    It returns a JSON object describing the query.

    Returns
    -------
    answer : str
        answer to the query made by the user
    """

    logger.info(f"Received parse_v2 NL request with content: {str(request.json["query"])}")

    start = perf_counter()
    query = request.json["query"]

    url = "https://ollama.pagoda.liris.cnrs.fr/api/generate"
    headers = {
        "Content-Type": "application/json"
    }

    prompt = """
    You are a helpful assistant with access to the following function. Use it if required -
    
    {
        'name': 'get_query_param',
        'description': 'extract the fields and the model route to call an appropriate endpoint',
        'parameters': {
            'type': 'object',
            'properties': {
                'model': {
                    'type': 'string',
                    'description': 'the name of the model to call. can only be one.',
                    'enum': ['/predict_FACT_G_IUS', '/predict_FACT_G_DPR', '/predict_FACT_G_TRH', '/predict_FACT_G_HLS', '/predict_FACT_G_TSS' 
                    '/subtypes', '/predict_saillant, '/relationship_with_drug', '/followup', '/content_qual']
                },
                'attributes': {
                    'type': 'array',
                    'description': 'the attributes of the model to call. the FACT_G or quality of life model attributes 
                    are 
                    age_at_immunotherapy(the year he was born), sex(1 for male 0 for female), cancer_type(Melanoma, 
                    Lymphoma, Lung or Other 
                    Types), 
                    education_level (0 for low, 1 for high) 
                    financial_status (0 for tight 1 for comfortable) and 
                    model_score where only model should be replaced 
                    by the suffix of the model called so we have tss_score for predict_FACT_G_TSS or ius_score for predict_FACT_G_IUS and so on and make sure it is lowercase only, or it won't work.
                    The subtypes or patient attributes model has as attributes 
                    age 
                    genre (Male or Female) 
                    height 
                    weight 
                    b_symptoms 
                    lactate 
                    c_reactive_protein 
                    hemoglobin 
                    white_blood_cell 
                    lymphocites 
                    red_blood_cell and 
                    glucose. These attributes are all floats except genre.
                    The saillant features model attributes are 
                    best_response_otherM 
                    mean_medications 
                    date_of_birth (year of birth)
                    i_am_content_with_the_qual 
                    thinking_back_to_the_yearn 
                    mean_therapies
                    immuno_frequency
                    worrying_thoughts_go_throu
                    thinking_back_to_the_yearh and 
                    i_get_a_sort_of_frightened. These attributes are strictly numerical, and their 
                    corresponding values will be given.'
                     'items': {
                         'attribute': 'string', 
                         'value':'string'
                     },
                },
            },
            'required': ['attributes', 'model']
        }
    }
    
    """

    data = {
        "model": "mistral:7b",
        "prompt": prompt + str(query),
        "format": "json",
        "stream": False,
    }

    response = requests.post(url, data=json.dumps(data), headers=headers)
    response_data = response.json()

    end = perf_counter()
    time = end - start

    logger.info(f"Successful parsing for {str(request.json["query"])} with result : {str(response_data)} ( Execution "
                f"time {str(time)}")

    return jsonify(response_data)


@app.route('/api/chat/answer', methods=['POST'])
def answer():
    """
    This function is called when the user sends a query to the model.
    It returns the answer to the query.

    Returns
    -------
    answer : str
        answer to the query made by the user
    """

    logger.info(f"Received parse_v2 NL request with content: {str(request.json["query"])}")
    start = perf_counter()
    query = request.json["query"]

    url = "https://ollama.pagoda.liris.cnrs.fr/api/generate"
    headers = {
        "Content-Type": "application/json"
    }
    data = {
        "model": "mistral:7b",
        "prompt": str(query),
        "stream": False
    }

    response = requests.post(url, data=json.dumps(data), headers=headers)
    response_data = response.json()

    end = perf_counter()
    time = end - start
    logger.info(f"Successful parsing for {str(request.json["query"])} with result : {str(response_data)} ( Execution "
                f"time {str(time)}")

    return jsonify(response_data)


@app.route('/api/chat/sql', methods=['POST'])
def sql():
    """
    This function is called when the user sends a query to the model.
    It returns the answer to the query.

    Returns
    -------
    answer : str
        answer to the query made by the user
    """

    logger.info(f"Received SQL generation request with content: {str(request.json["query"])}")
    start = perf_counter()
    query = request.json['query']

    end = perf_counter()
    time = end - start
    logger.info(f"Successful SQL generation for {str(request.json["query"])} with result : {str(query)} ( Execution "
                f"time {str(time)}")

    return jsonify(query)


@app.route('/api/chat/transcribe', methods=['POST'])
def transcribe():
    """
    This function returns the text transcription of an audio recording

    Returns
    -------
    answer : str
        answer to the query made by the user
    """

    logger.info(f"Received STT transcription request with file: {str(request.files["file"])}")
    start = perf_counter()

    file = request.files["file"]
    file.save('app/src/audio/audio.wav')
    file.flush()
    file.close()

    client = Groq(api_key="")
    filename = "app/src/audio/audio.wav"

    with open(filename, "rb") as file:
        transcription = client.audio.transcriptions.create(
            file=(filename, file.read()),
            model="whisper-large-v3",
            prompt="Specify context or spelling",
            response_format="json",
            language="en",
            temperature=0.0
        )
        print(transcription.text)

    end = perf_counter()
    time = end - start

    logger.info(f"Successful STT transcription for {str(request.files["file"])} with result : "
                f"{str(transcription.text)} ( Execution "
                f"time {str(time)}")

    return jsonify(transcription.text)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=11434)
