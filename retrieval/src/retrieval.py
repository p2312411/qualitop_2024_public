import json
import logging
import os
import warnings
from time import perf_counter

import numpy as np
import pandas as pd
import pyfiglet
from langchain_community.llms import Ollama
from langchain_experimental.llms.ollama_functions import OllamaFunctions
from langchain_core.prompts import ChatPromptTemplate, MessagesPlaceholder, FewShotChatMessagePromptTemplate
from langchain_core.output_parsers import StrOutputParser
from sentence_transformers import SentenceTransformer

from structs import Query


class Retrieval:

    def __init__(self):
        self.schema = None
        self.llm = None
        self.llm_json = None
        self.embeddings_model = None
        self.embeddings_model_name = "all-MiniLM-L12-v2"
        self.similarity_threshold = 0.32
        self.llm_name = "mistral:7b"
        self.context_length = 4096
        self.llm_url = "https://ollama.pagoda.liris.cnrs.fr"
        self.data_dictionary_path = "/app/src/db_models/Data_dictionary.xlsx"
        self.db_model_path = "/app/src/db_models/db_models.xlsx"
        self.embeddings_path = "/app/src/embeddings/data_dictionary_embeddings.csv"
        self.log = logging.getLogger(__name__)
        warnings.simplefilter(action='ignore', category=FutureWarning)
        self.cli_start()
        self.load_schema()
        self.load_transformer()
        self.load_llm()
        self.load_llm_functions()

    def cli_start(self):
        print(pyfiglet.figlet_format("LLM Json Mode".upper(), font="slant"))
        print(("Model : " + str(self.llm_name)) + "\nHosted at : " + str(self.llm_url))
        print("Loaded data dictionary located at " + self.data_dictionary_path)
        print("Loaded database schema located at " + self.db_model_path)

    def generate_embeddings(self):

        start = perf_counter()

        sheet_name = 'DataDictionary IPOL Immun'
        attr_col_name = "Variable / Field Name"
        tables_name = "Form Name"
        ann_col_name = "Field Annotation"

        data = pd.read_excel(self.data_dictionary_path, sheet_name=sheet_name,
                             usecols=[attr_col_name, tables_name, ann_col_name])
        ls_table = []
        dataframe = {'tables': [], 'attributes': [], 'embeddings': []}

        for index, row in data.iterrows():
            table_name = row[tables_name]
            dataframe["tables"].append(table_name)

            if table_name not in ls_table:
                ls_table.append(table_name)

            dataframe["attributes"].append(str(row[attr_col_name]))

            attribute_des = str(row[attr_col_name]) + " : " + str(row[ann_col_name])
            embedding = self.embeddings_model.encode(attribute_des)
            dataframe["embeddings"].append(embedding)

        for table in ls_table:
            dataframe["tables"].append(table)
            dataframe["attributes"].append(table)

            embedding = self.embeddings_model.encode(table)
            dataframe["embeddings"].append(embedding)

        dataframe = pd.DataFrame(data=dataframe)
        dataframe.to_csv(self.embeddings_path, mode="w")

        end = perf_counter()
        time = end - start
        self.log.info("Execution time : " + str(time))

    def load_schema(self):
        """
        Loads the database schema from a data dictionary
        """

        sheet_name = 'DataDictionary IPOL Immun'
        attr_col_name = "Variable / Field Name"
        tables_name = "Form Name"
        ann_col_name = "Field Annotation"

        data_dic = {}
        sub_dic = {}
        dict_table_names = []
        dict_table_attr_desc = []
        table_name = ''

        data = pd.read_excel(self.data_dictionary_path, sheet_name=sheet_name,
                             usecols=[attr_col_name, tables_name, ann_col_name])

        for index, row in data.iterrows():
            data_table_name = row[tables_name]
            if data_table_name != table_name and table_name == '':
                table_name = data_table_name
                dict_table_names.append(table_name)
            elif data_table_name != table_name and table_name != '':
                data_dic[table_name] = sub_dic
                table_name = data_table_name
                dict_table_names.append(table_name)
                sub_dic = {}
                sub_dic["study_id"] = "ID of the patient in the QUALITOP cohort"
            sub_dic[row[attr_col_name]] = row[ann_col_name]
            dict_table_attr_desc.append(str([row[attr_col_name]]) + " : " + str([row[ann_col_name]]))
            if row[attr_col_name] == data[attr_col_name][len(data) - 1] and row[tables_name] == data[tables_name][
                len(data) - 1]:
                data_dic[table_name] = sub_dic

        self.schema = data_dic
        self.log.info(self.data_dictionary_path + " schema loaded and converted into dictionary.")

    def load_embeddings(self):
        """
        Retrieves embeddings of attributes and their descriptions

        Returns
        -------
        data : DataFrame
            dataframe containing the concatenantion of attributes
            and their description, their corresponding table
            and their embeddings
        """
        return pd.read_csv(self.embeddings_path)

    def load_llm(self):
        """
        Loads the chosen llm hosted at the given url
        """
        self.llm = Ollama(model=self.llm_name, base_url=self.llm_url, num_ctx=4096)
        self.log.info(self.llm_name + " hosted at " + self.llm_url + " for text generation loaded.")

    def load_llm_functions(self):
        """
        Load the chosen model from Ollama hosted at the giben base_url
        Have function calling abilities

        """

        self.llm_json = OllamaFunctions(model=self.llm_name, base_url=self.llm_url, num_ctx=self.context_length,
                                        format="json")
        self.log.info(self.llm_name + " hosted at " + self.llm_url + " for function calling loaded.")

    def load_transformer(self):
        """
        Loads the transformer model for embedding based semantic similarity evaluation
        """

        self.embeddings_model = SentenceTransformer(self.embeddings_model_name)

    def get_chat_history(self, user_id, chat_history):
        """
        Retrieves a user's chat history from a file

        Parameters
        ----------
        user_id : str
            id of the user sending the query
        chat_history : ChatHistory
            Object containing the conversation between the user and the llm
        """

        path = str(os.getcwd()) + "/chat-history/" + user_id + ".csv"
        if os.path.exists(path):
            ls_col = []
            data = pd.read_csv(path)

            for x in data.columns:
                ls_col.append(x)

            role = ls_col[1]
            value = ls_col[2]

            for index, row in data.iterrows():
                if row[role] == 'user':
                    chat_history.add_user_message(row[value])
                    self.log.info(str(row[role]) + " message '" + row[value] + "' added to chat_history.")
                else:
                    chat_history.add_ai_message(row[value])
                    self.log.info(str(row[role]) + " message '" + row[value] + "' added to chat_history.")

    def add_message_to_history(self, role, value, user_id, new_chat):
        """
        Adds a message to a user's chat history file

        Parameters
        ----------
        role : str
            Can be User or Ai
        value : str
            Content of the message
        user_id : str
            id of the user sending the query
        new_chat : int
            Can be 0 or 1, meant to know if a conversation is new or current
        """

        data = {'role': [role], 'value': [value]}
        path = "chat-history/" + user_id + ".csv"
        dataframe = pd.DataFrame(data=data)

        if new_chat:
            dataframe.to_csv(path, mode='w')
        else:
            dataframe.to_csv(path, mode='a')

        self.log.info(str(role) + " message '" + value + "' added to chat_history.")

    def semantic_similarity(self, query, embeddings_dataframe):
        """
        Evaluates semantic similarity trough embedding process
        of the given query with the attributes of the database

        Parameters
        ----------
        query : str
            query input from the user
        embeddings_dataframe : DataFrame
            dataframe containing all embbedings

        Returns
        -------
        ls_tables : list[str]
            list of all tables names in which there is a
            semantically similar attribute to the query
        ls_attributes : list[str]
            list of all attributes semantically similar
            to the query
        """

        ls_tables = []
        ls_attributes = []

        query_embedding = self.embeddings_model.encode(query)
        for index, row in embeddings_dataframe.iterrows():

            # Convert the string representation of a ndarray back into a ndarray
            vector = row["embeddings"]
            bf = len(vector) - 1
            vector = vector[1:bf]

            vector_split = vector.split()
            for i in range(len(vector_split)):
                vector_split[i] = float(vector_split[i])

            vector_split = np.array(vector_split, dtype='float32')

            # Test similiraty between query and the concatenation of an attribute and its description
            similarity_score = self.embeddings_model.similarity(vector_split, query_embedding)[0][0].item()
            if similarity_score > self.similarity_threshold:
                if row["tables"] not in ls_tables:
                    ls_tables.append(row["tables"])
                ls_attributes.append(row["attributes"])

        self.log.info("Tables selected : " + str(ls_tables))
        self.log.info("Attributes selected : " + str(ls_attributes))

        return ls_tables, ls_attributes

    def apis_selector(self, query):
        """
        Select the corresponding api

        Parameters
        ----------
        query : str
            query input from the user

        Returns
        -------
        answer : str
            answer generated by the model
        """

        models = []
        apis_name = [
            "fact_g_single",
            "fact_g_loop"
            "model_2",
            "model-3"
        ]
        apis_cluster = [
            "fact_g_quality_of_life, fact-g, fact g, quality of life, qol",
            "model_2, model 2, model-2",
            "model_3, model 3, model-3"
        ]

        query_embedding = self.embeddings_model.encode(query)
        apis_embedding = self.embeddings_model.encode(apis_cluster)
        similarity_score = self.embeddings_model.similarity(apis_embedding, query_embedding)

        self.log.info("Similarity scores : " + similarity_score)

        for i in range(2):
            if similarity_score[i][0].item() > self.similarity_threshold:
                models.append(apis_name[i])

        return models

    def llm_function_call(self, query, models):
        """
        Generates the sql-based json query

        Parameters
        ----------
        query : str
            content of the natural language query
        models : list[str]
            list of models to use

        Returns
        -------
        prompt_template : ChatPromptTemplate
                prompt used for function calling
        """

        answer = ""
        examples = [
            {
                "input": "Give me fact-g of a 30 years old person with lymphoma and 5 at literacy_score. \n ["
                         "'fact_g_quality_of_life']",
                "output":
                    """
                    'models': ['fact_g_quality_of_life'],
                    'attributes': ['age_at_immunotherapy', 'sex', 'cancer_type', 'education_level', 'financial_status', 'literacy_score'],
                    'where': [
                        {'attribute':'age', 'operator':'=', 'value':30},
                        {'attribute':'cancer_type', 'operator':'=', 'value':'lymphoma'},
                        {'attribute':'literacy_store', 'operator':'=', 'value':5},
                    ],
                    'group_by': {}
                    'order_by': {}
                    'aggregate': {}
                    'limit': {}
                    """,
            },
            {
                "input": "What is the quality of life and model 2 prediction for patients under 67 with brain cancer? "
                         "\n ['fact_g_quality_of_life', 'model_2']",
                "output":
                    """
                 'models': ['fact_g_quality_of_life', 'model_2'],
                     'attributes': ['age_at_immunotherapy', 'sex', 'cancer_type', 'education_level', 'financial_status', 'ius_score'],
                     'where': [
                             {'attribute':'age', 'operator':'<', 'value':67},
                             {'attribute':'cancer_type', 'operator':'=', 'value':'brain'},
                     ],
                     'group_by': {}
                     'order_by': {}
                     'aggregate': {}                                                                                                                                                    'limit': {}
                     """,
            },
            {
                "input": "What are the prediction of model 3 of patient above 30 years old? \n ['model_3']",
                "output":
                    """
                    'models': ['model_3'],
                    'attributes': ['age_at_immunotherapy', 'sex', 'cancer_type', 'education_level', 'financial_status', 'ius_score'],
                    'where': [
                        {'attribute':'age', 'operator':'>', 'value':30},
                    ],
                    'group_by': {}
                    'order_by': {}
                    'aggregate': {}                                                                                                              >        """
            }
        ]

        example_prompt = ChatPromptTemplate.from_messages(
            [
                ("user", "{input}"),
                ("assistant", "{output}"),
            ])

        few_shot_prompt = FewShotChatMessagePromptTemplate(
            example_prompt=example_prompt,
            examples=examples,
            input_variables=["input"],
        )

        prompt_template = ChatPromptTemplate.from_messages(
            [
                ("system", """Your task is to generate a SQL based query in JSON with name of model used and parameters.
                The model attribute must equals to this list : {models}"""),
                few_shot_prompt,
                ("human", "{input}"),
            ])

        exit_status = 0
        structured_llm = self.llm.with_structured_output(Query)
        chain = prompt_template | structured_llm

        while exit_status == 0:
            try:
                answer = chain.invoke({"input": query, "models": models})
                exit_status = 1
            except Exception as e:
                print(e)
                pass

        return answer

    def llm_nl2sql(self, query, selected_table, data_dic, chat_history):
        """
        Generation of SQL query from natural language input
        Use few shot injection and chat history

        Parameters
        ----------
        query : str
            query input from the user
        selected_table : list[str]
            list of tables selected for query generation
        data_dic : dict
            dic containing all tables from database
        chat_history : ChatMessageHistory
            contains all messages from user and ai

        Returns
        -------
        answer : str
            answer generated by llm
        """

        examples = [
            {
                "input": "What is the number of medication types?",
                "output": "SELECT DISTINCT medication_type FROM FR JOIN ES JOIN PT JOIN NL;"
            },
            {
                "input": "Give hospital of inclusion of of people having cancer type lymphoma in Spain.",
                "output": "SELECT hospital_of_inclusion FROM ES WHERE cancer_type = 'lymphoma';"
            },
            {
                "input": "Give me all infos on adverse events from patient 1.",
                "output": "SELECT date_ae, end_date_ae_2, type_ae, highest_grade, outcome_of_adverse_event, relationship_with_drug FROM FR JOIN ES JOIN PT JOIN NL WHERE study_id = 1 ;"
            },
            {
                "input": "Give me hospital of inclusion of patient who follow MONOCLONAL ANTIBODIES therapy type in Netherland and Portugal",
                "output": "SELECT hospital_of_inclusion FROM NL JOIN PT WHERE therapy_type = 'MONOCLONAL ANTIBODIES';"
            },
            {
                "input": "Please show the study_id of patients who are dead and who have Lymphoma in Spain, Portugal and France",
                "output": "SELECT s.study_id FROM ES JOIN PT JOIN FR WHERE s.subject_status = 'Dead' "
            }
        ]

        example_prompt = ChatPromptTemplate.from_messages(
            [
                ("user", "{input}"),
                ("assistant", "{output}"),
            ])

        few_shot_prompt = FewShotChatMessagePromptTemplate(
            example_prompt=example_prompt,
            examples=examples,
            input_variables=["input"],
        )

        prompt_template = ChatPromptTemplate.from_messages(
            [
                ("system", """Your task is to convert a natural language sentence to a sql query corresponding to the 
                human input. Do not explain the query generation, simply write the query so that it can be used to 
                query a MySql database directly. To generate the query, use the following attributes infos : {
                table_info}\n\n The tables are FR (France), ES (Spain), PT (Portugal), NL (Netherlands) Use the chat 
                history below."""),
                few_shot_prompt,
                MessagesPlaceholder(variable_name="messages"),
                ("human", "{input}"),
            ])
        sub_dic = {}

        for table in selected_table:
            sub_dic[table] = data_dic[table]

        chain = prompt_template | self.llm | StrOutputParser()
        answer = chain.invoke({"input": query, "messages": chat_history.messages, "table_info": str(sub_dic)})

        return answer

    def llm_sql2json(self, query):
        """
        Generation of JSON query from a template and SQL query input
        Uses pydantic for a better structured answer

        Parameters
        ----------
        query : str
            query input from the user

        Returns
        -------
        answer : str
            answer generated by llm
        """

        examples = [
            {
                "input": "SELECT subject_status FROM ES JOIN FR WHERE hospital_of_inclusion = 'Lyon'",
                "output":
                    """
                    {
                        "models": [],
                        "sources": ['ES', 'FR'],
                        "attributes": ['subject_status'],
                        "where": [
                            {"attribute": "hospital_of_inclusion", "operator":"=", "value":"Lyon"}
                        ],
                        "group_by": [],
                        "order_by": [],
                        "aggregate": [],
                        "limit": []
                    }
                    """
            },
            {
                "input": "SELECT subject_status FROM PT JOIN NL WHERE hospital_of_inclusion = 'Lyon'",
                "output":
                    """
                    {
                        "models": [],
                        "sources": ['PT', 'NL'],
                        "attributes": ['subject_status'],
                        "where": [
                            {"attribute": "hospital_of_inclusion", "operator":"=", "value":"Lyon"}
                        ],
                        "group_by": [],
                        "order_by": [],
                        "aggregate": [],
                        "limit": []
                    }
                    """
            }

        ]

        example_prompt = ChatPromptTemplate.from_messages(
            [
                ("user", "{input}"),
                ("assistant", "{output}"),
            ])

        few_shot_prompt = FewShotChatMessagePromptTemplate(
            example_prompt=example_prompt,
            examples=examples,
            input_variables=["input"],
        )

        prompt_template = ChatPromptTemplate.from_messages(
            [
                ("system",
                 """
                 Your task is to use the SQL Query to fill a corresponding JSON file.
     
                 Here's indication to fill the values:
                 - Sources IN ['FR, ES, PT, NL]
                 - Operator IN ['=', '<', '>']
     
                 """),
                few_shot_prompt,
                ("human", "{input}"),
            ])

        structured_llm = self.llm_json.with_structured_output(Query)
        chain = prompt_template | structured_llm

        exit_status = 0
        answer = "Nope"

        while exit_status < 10:
            try:
                answer = chain.invoke({"input": query})
                exit_status = 11
            except Exception as e:
                exit_status += 1

        if answer != "Nope":
            answer = answer.json()
        return answer

    def get_first_query(self, query):
        """
        From a str containing multiples sql queries,
        create a str from the first sql query

        Parameters
        ----------
        query : str
            str containing multiples sql queries

        Returns
        -------
        final_query : str
            first query to be generated by the llm
        """
        final_query = ""
        i = 1
        query_splited = query.split()
        query = False

        if query_splited[0] == 'SELECT':
            query = True
            final_query += query_splited[0] + " "

        while i < len(query_splited) and ";" not in query_splited[i - 1]:
            if query_splited[i] == "SELECT":
                query = True
            if query:
                final_query += query_splited[i] + " "
            i += 1
        return final_query

    def parse_entry(self, message: str):
        """
        Retrieve the entry data (model used, id of the user,
        content of the query, new chat or no) from a json file

        Parameters
        ----------
        message : str
            path to the file in which the entry data are contained

        Returns
        -------
        user_id : str
            id of the user sending the query
        query : str
            query made by the user to be answered by the model
        new_chat : int
            Can be 0 or 1, meant to know if a conversation is new or current
        """
        self.log.info("Received user input : " + str(message))
        entry = json.loads(str(message))
        user_id = entry["username"]
        query = entry["query"]
        new_chat = entry["new_chat"]

        return user_id, query, new_chat
