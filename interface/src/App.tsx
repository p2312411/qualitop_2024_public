import './index.css';
import {BrowserRouter, Routes, Route} from 'react-router-dom';
import AuthenticationPage from "./pages/AuthenticationPage.tsx";
import {HomePage} from "./pages/HomePage.tsx";
import RegistrationPage from "./pages/RegistrationPage.tsx";
import {FormPage} from "./pages/FormPage.tsx";
import {ChatPage} from "./pages/ChatPage.tsx";
import {FormResponsePage} from "./pages/FormResponsePage.tsx";
import {useEffect, useState} from "react";

function App() {
    const [isAuthenticated, setIsAuthenticated] = useState(false);
    useEffect(() => {
        const user = sessionStorage.getItem('username');
        const password = sessionStorage.getItem('password');
        setIsAuthenticated(!!user && !!password);
    }, []);

    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<AuthenticationPage/>}/>
                <Route path="/login" element={<AuthenticationPage/>}/>
                <Route path="/registration"  element={<RegistrationPage/>}/>
                <Route path="/home" element={isAuthenticated ? <HomePage/>: <AuthenticationPage/>}/>
                <Route path="/form" element={isAuthenticated ? <FormPage/>: <AuthenticationPage/>}/>
                <Route path="/response" element={isAuthenticated ? <FormResponsePage/>: <AuthenticationPage/>}/>
                <Route path="/chat" element={isAuthenticated ? <ChatPage/>: <AuthenticationPage/>}/>
            </Routes>
        </BrowserRouter>)
        ;
}

export default App;

