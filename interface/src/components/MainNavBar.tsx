import {Component} from "react";

export class MainNavBar extends Component<{ activePage: string }> {
    render() {
        let {activePage} = this.props;
        return (
            <nav className="flex items-center space-x-4 lg:space-x-6">
                <a
                    href="/home"
                    className={`text-sm font-medium transition-colors hover:text-primary ${activePage !== 'home' ? 'text-muted-foreground' : ''}`}
                >
                    Home
                </a>
                <a
                    href="/form"
                    className={`text-sm font-medium transition-colors hover:text-primary ${activePage !== 'form' ? 'text-muted-foreground' : ''}`}
                >
                    Form
                </a>
                <a
                    href="/chat"
                    className={`text-sm font-medium transition-colors hover:text-primary ${activePage !== 'chat' ? 'text-muted-foreground' : ''}`}
                >
                    Chat
                </a>
            </nav>
        );
    }
}
