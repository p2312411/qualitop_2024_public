import {Key} from "react";
import {Card} from "../../shadcn/components/ui/card.tsx";
import {Separator} from "../../shadcn/components/ui/separator.tsx";
import {
    Table,
    TableHeader,
    TableBody,
    TableRow,
    TableCell,
    TableHead,
} from "../../shadcn/components/ui/table.tsx";
import DataPlot from "./DataPlot.tsx";
import {useLocation} from "react-router-dom";
import {DataTable} from "./DataTable.tsx";
import {ColumnDef} from "@tanstack/react-table";
import {Button} from "../../shadcn/components/ui/button.tsx";
import {ArrowUpDown} from "lucide-react";
import {Checkbox} from "../../shadcn/components/ui/checkbox.tsx";
import ModelResult from "./ModelResult.tsx";
import RepartitionPlot from "./RepartitionPlot.tsx";

type PolystoreData = {
    study_id: string,
    date_of_birth: string,
    sex: 'Female' | 'Male',
    cancer_type: "1" | '2' | '3' | '4',
    i_have_nausea: "1" | '2' | '3' | '4' | '5',
    i_have_pain: "1" | '2' | '3' | '4' | '5',
    i_am_content_with_the_qual: "1" | '2' | '3' | '4' | '5',
    immuno_frequency: string,
}

// @ts-ignore
const transformData = (dataArray, attributes) => {
    if (attributes.includes("sex")) {
        for (let i = 0; i < dataArray.length; i++) {
            if (dataArray[i]["sex"] == "0") {
                dataArray[i]["sex"] = "Male";
            } else {
                dataArray[i]["sex"] = "Female"
            }
        }
    }
    return dataArray.map((obj) => {
        return attributes.reduce((acc, attr) => {
            if (attr === "i_have_nausea" || attr === "i_have_pain" || attr === "i_am_content_with_the_qual") {
                switch (obj[attr]) {
                    case "1":
                        acc[attr] = "Not at all";
                        break;
                    case "2":
                        acc[attr] = "A little bit";
                        break;
                    case "3":
                        acc[attr] = "Somewhat";
                        break;
                    case "4":
                        acc[attr] = "Quite a bit";
                        break;
                    case "5":
                        acc[attr] = "Very much";
                        break;
                    default:
                        acc[attr] = obj[attr];
                }
            } else if (attr === "cancer_type") {
                switch (obj[attr]) {
                    case "1":
                        acc[attr] = "Melanoma";
                        break;
                    case "2":
                        acc[attr] = "Lymphoma";
                        break;
                    case "3":
                        acc[attr] = "Lung";
                        break;
                    case "4":
                        acc[attr] = "Other types";
                        break;
                    default:
                        acc[attr] = obj[attr];
                }
            } else {
                acc[attr] = obj[attr];
            }
            return acc;
        }, {});
    });
};


const generateArray = (attributes: string[]) => {
    const selectColumn = {
        id: "select",
        header: ({table}) => (
            <Checkbox
                checked={
                    table.getIsAllPageRowsSelected() ||
                    (table.getIsSomePageRowsSelected() && "indeterminate")
                }
                onCheckedChange={(value) => table.toggleAllPageRowsSelected(!!value)}
                aria-label="Select all"
            />
        ),
        cell: ({row}) => (
            <Checkbox
                checked={row.getIsSelected()}
                onCheckedChange={(value) => row.toggleSelected(!!value)}
                aria-label="Select row"
            />
        ),
        enableSorting: false,
        enableHiding: false,
    };
    const columns = attributes.map((col: string) => {
        switch (col) {
            case "i_have_pain":
                return {
                    accessorKey: col,
                    header: ("Pain level"),
                };
                break;
            case "i_have_nausea":
                return {
                    accessorKey: col,
                    header: ("Nausea level"),
                };
                break;
            case "i_am_content_with_the_qual":
                return {
                    accessorKey: col,
                    header: ("Contentement with QoL"),
                };
                break;
        }
        if (col === "date_of_birth" || col === "immuno_frequency") {
            return {
                accessorKey: col,
                header: ({column}) => {
                    return (
                        <Button
                            variant="ghost"
                            onClick={() => column.toggleSorting(column.getIsSorted() === "asc")}
                        >
                            {col.charAt(0).toUpperCase() + col.slice(1).replace(/_/g, ' ')}
                            <ArrowUpDown className="ml-2 h-4 w-4"/>
                        </Button>
                    );
                },
            };
        } else {
            return {
                accessorKey: col,
                header: (col.charAt(0).toUpperCase() + col.slice(1)).replace(/_/g, ' '),
            };
        }
    });
    return [selectColumn, ...columns];
};

export const FormResponse = () => {

    const {state} = useLocation();
    const {rawAttributes, rawData, rawResults} = state;
    const attributes = rawAttributes.slice(0, 5);
    const polystoreData = JSON.parse(rawData);
    const results = JSON.parse(rawResults);
    const columns: ColumnDef<PolystoreData>[] = generateArray(rawAttributes);
    const transformedData = transformData(polystoreData, rawAttributes);

    function calculateResultAverages(results) {
        const resultAverages = {};

        results.forEach((obj) => {
            const model = obj.model;
            let result;
            try {
                result = JSON.parse(obj.result);
            } catch (error) {
                console.error(`Invalid JSON string: ${obj.result}`);
                return;
            }
            if (!resultAverages[model]) {
                resultAverages[model] = {};
            }
            for (const key in result) {
                if (!resultAverages[model][key]) {
                    resultAverages[model][key] = {sum: 0, count: 0};
                }
                resultAverages[model][key].sum += Number(result[key]);
                resultAverages[model][key].count++;
            }
        });
        for (const model in resultAverages) {
            for (const key in resultAverages[model]) {
                resultAverages[model][key] = resultAverages[model][key].sum / resultAverages[model][key].count;
            }
        }
        return resultAverages;
    }

    return (<div className="bg-cover h-fit bg-gradient-to-tr from-lime-300 to-sky-400 p-4">
        <Card className="bg-white backdrop-filter backdrop-blur-lg opacity-90 p-4">
            <div className="flex flex-col p-4">
                <h3 className="text-2xl font-bold text-left">Query results</h3>
            </div>
            <Separator orientation="horizontal"/>
            <div className="flex flex-row p-4 m-2">
                <div className="w-full">
                    <h3 className="text-xl font-bold text-left">Patients data</h3>
                    <DataTable columns={columns} data={transformedData}/>
                </div>
                {/*<div className="flex flex-col w-1/2 space-x-2">
                    <div className="flex flex-row p-6">
                        {attributes.map((attribute: any, index: Key | null | undefined) => (
                            <div key={index}>
                                <DataPlot data={polystoreData} attributes={attributes} name={attribute}/>
                            </div>
                        ))}
                    </div>
                    <div className="flex flex-row p-6">
                        <RepartitionPlot data={polystoreData} attributes={attributes} names={attributes}/>
                    </div>
                </div>*/}
            </div>
            {/*<Separator orientation="horizontal"/>
            <div className="flex flex-col p-4">
                <h3 className="text-xl font-bold text-left">Models results</h3>
                <Table className="border-1 rounded-2xl">
                    <TableHeader>
                        <TableRow>
                            <TableHead>model name</TableHead>
                            <TableHead>prediction</TableHead>
                        </TableRow>
                    </TableHeader>
                    <TableBody>
                        {Object.entries(calculateResultAverages(results)).map(([model, averages]) => (
                            <TableRow key={model}>
                                <TableCell>{model}</TableCell>
                                <TableCell>
                                    {Object.entries(averages).map(([key, value]) => (
                                        <ModelResult key={key} model={model} result={{[key]: value}}/>
                                    ))}
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </div>*/}
        </Card>
    </div>);
}
