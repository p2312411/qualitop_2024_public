import {Input} from "../../shadcn/components/ui/input.tsx";
import {Button} from "../../shadcn/components/ui/button.tsx";
import {Separator} from "../../shadcn/components/ui/separator.tsx";
import {Select} from "../../shadcn/components/ui/select.tsx";
import {SelectContent, SelectItem, SelectTrigger, SelectValue} from "../../shadcn/components/ui/select.tsx";
import {LoadingSpinner} from "./LoadingSpinner.tsx";
import * as React from "react";
import {z} from "zod"
import {zodResolver} from "@hookform/resolvers/zod";
import {useForm} from "react-hook-form";
import {
    FormControl, FormField, FormItem, FormLabel, FormMessage, Form, FormDescription
} from "../../shadcn/components/ui/form.tsx";
import {Card} from "../../shadcn/components/ui/card.tsx";
import {useNavigate} from "react-router-dom";
import {useToast} from "../../shadcn/components/ui/use-toast.ts"
import {Tabs, TabsContent, TabsList, TabsTrigger} from "../../shadcn/components/ui/tabs.tsx";
import {
    Dialog,
    DialogContent,
    DialogDescription,
    DialogHeader
} from "../../shadcn/components/ui/dialog.tsx";
import {Progress} from "../../shadcn/components/ui/progress.tsx";
import {
    TableBody,
    TableCaption,
    TableCell,
    TableHead,
    TableHeader,
    TableRow
} from "../../shadcn/components/ui/table.tsx";
import {Table} from "lucide-react";

const formSchema = z.object({
    study_id: z.string().optional(),
    hospital_of_inclusion: z.string().optional(),
    date_of_birth: z.string().optional(),
    date_of_birth_operator: z.string().optional(),
    sex: z.string().optional(),
    cancer_type: z.string().optional(),
    i_have_nausea: z.string().optional(),
    i_have_pain: z.string().optional(),
    i_am_content_with_the_qual: z.string().optional(),
    immuno_frequency: z.string().optional(),
    model1_loop: z.boolean().optional(),
    model2: z.boolean().optional(),
    model3: z.boolean().optional(),
    model4: z.boolean().optional()
});

const formSchemaFactg = z.object({
    type: z.string(),
    score: z.string(),
    age_at_immunotherapy: z.string(),
    sex: z.string(),
    cancer_type: z.string(),
    education_level: z.string(),
    financial_status: z.string()
});

const formSchemaSailliant = z.object({
    best_response_otherM: z.string(),
    mean_medications: z.string(),
    date_of_birth: z.string(),
    i_am_content_with_the_qual: z.string(),
    thinking_back_to_the_yearn: z.string(),
    mean_therapies: z.string(),
    immuno_frequency: z.string(),
    worrying_thoughts_go_throu: z.string(),
    thinking_back_to_the_yearh: z.string(),
    i_get_a_sort_of_frightened: z.string()
});

const formSchemaSubtypes = z.object({
    age: z.string(),
    sex: z.string(),
    height: z.string(),
    weight: z.string(),
    glucose: z.string(),
    red_blood_cell: z.string(),
    b_symptoms: z.string(),
    lactate: z.string(),
    c_reactive_protein: z.string(),
    hemoglobin: z.string(),
    white_blood_cell: z.string(),
    lymphocites: z.string()

});

const formFieldsPatient = ({
    study_id: '',
    date_of_birth: "",
    sex: 'any',
    cancer_type: "",
    i_have_nausea: 'false',
    i_have_pain: 'false',
    i_am_content_with_the_qual: "false",
    immuno_frequency: ""
})

export function FormQuery() {
    const [isLoading, setIsLoading] = React.useState<boolean>(false)
    const navigate = useNavigate();
    const {toast} = useToast();
    const [isDialogOpen, setIsDialogOpen] = React.useState<boolean>(false);
    const [comment, setComment] = React.useState<string>("");
    const [color, setColor] = React.useState<string>("");
    const [value, setValue] = React.useState<number>(100);
    const [field, setField] = React.useState<string>("");

    let saillant = {
        "relationship_with_drug_nbM": 0, "followupM": 0,
        "i_am_content_with_the_qualT":
            0
    };
    let subtypes = {
        "subtypes": ""
    };

    const [stateSaillant, setStateSaillant] = React.useState(saillant);
    const [stateSubtypes, setStateSubtypes] = React.useState(subtypes);

    const formFieldsPatientsDesc = [
        "A unique identifier assigned to each participant in the study.",
        "The year of birth of the participant.",
        "The gender of the participant. In this case, it can be any gender.",
        "The type of cancer the participant is diagnosed with.",
        "The degree at which the participant is currently experiencing nausea.",
        "The degree at which the participant is currently experiencing pain.",
        "The degree at which the participant is content with the quality of their care.",
        "The frequency at which the participant receives immunotherapy treatment."
    ]

    const formFieldsFactg = {
        score: "",
        age_at_immunotherapy: "",
        sex: "",
        cancer_type: "",
        education_level: "",
        financial_status: ""
    };

    const formFieldsFactgDesc: string[] = [
        "Effect of total intolerance to uncertainty Score (IUS) / Effect of doctor-patient relationship score (DPR)" +
        " / Effect of treatment related hope score (TRH) / Effect of Health Literacy score (HLS) / Effect of total social support (TSS).",
        "Age of the patient at the start of immunotherapy.",
        "Sex of the patient.",
        "Cancer type (Melanoma, Lymphoma, Lung, Other types)",
        "Education level of the patient",
        "Financial status of the patient."
    ];

    const formFieldsSailliant = {
        best_response_otherM: "0",
        mean_medications: "0",
        date_of_birth: "0",
        i_am_content_with_the_qual: "0",
        thinking_back_to_the_yearn: "0",
        mean_therapies: "0",
        immuno_frequency: "0",
        worrying_thoughts_go_throu: "0",
        thinking_back_to_the_yearh: "0",
        i_get_a_sort_of_frightened: "0"
    };
    const formFieldsSailliantDesc: string[] = [
        "Number of responses to other medications.",
        "Mean number of medication taken.",
        "Year of birth.",
        "I am content with the quality of my life right now.",
        "Thinking back to the year before you started immunotherapy, on average, how many medications prescribed by a doctor did you take a day, excluding treatments related to your cancer?",
        "Mean number of therapies.",
        "Frequency of immunotherapy sessions.",
        "Worrying thoughts go through my mind.",
        "Thinking back to the year before you started immunotherapy, to what extent did sadness, depression impact your everyday life?",
        "I get a sort of frightened feeling as if something awful is about to happen."
    ]

    const formFieldsSubtypes = {
        age: "0",
        sex: "0",
        weight: "0",
        height: "0",
        glucose: "0",
        red_blood_cell: "0",
        b_symptoms: "0",
        lactate: "0",
        c_reactive_protein: "0",
        hemoglobin: "0",
        white_blood_cell: "0",
        lymphocites: "0"
    };

    const formFieldsSubtypesDesc: string[] = [
        "The patient's age in years.",
        "The patient's sex.",
        "The patient's weight in kilograms.",
        "The patient's height in centimeters.",
        "The patient's blood glucose level in mg/dL.",
        "The patient's red blood cell count in millions/L.",
        "The presence of B symptoms.",
        "The patient's lactate level in mmol/L.",
        "The patient's C-reactive protein level in mg/L.",
        "The patient's hemoglobin level in g/dL.",
        "The patient's white blood cell count in thousands/µL.",
        "The patient's lymphocyte count in thousands/µL."
    ];

    const form = useForm<z.infer<typeof formSchema>>({
        resolver: zodResolver(formSchema), defaultValues: {
            study_id: '',
            date_of_birth: "",
            date_of_birth_operator: "",
            sex: 'any',
            cancer_type: "",
            i_have_nausea: '',
            i_am_content_with_the_qual: "",
            immuno_frequency: "",
            model1_loop: false,
            model2: false,
            model3: false,
            model4: false
        },
    })

    const formFactg = useForm<z.infer<typeof formSchemaFactg>>({
        resolver: zodResolver(formSchemaFactg), defaultValues: {
            type: "",
            score: "",
            age_at_immunotherapy: "",
            sex: "",
            cancer_type: "",
            education_level: "",
            financial_status: ""
        },
    })

    const formSailliant = useForm<z.infer<typeof formSchemaSailliant>>({
        resolver: zodResolver(formSchemaSailliant), defaultValues: {
            best_response_otherM: "0",
            mean_medications: "0",
            date_of_birth: "0",
            i_am_content_with_the_qual: "0",
            thinking_back_to_the_yearn: "0",
            mean_therapies: "0",
            immuno_frequency: "0",
            worrying_thoughts_go_throu: "0",
            thinking_back_to_the_yearh: "0",
            i_get_a_sort_of_frightened: "0"
        },
    })

    const formSubtypes = useForm<z.infer<typeof formSchemaSubtypes>>({
        resolver: zodResolver(formSchemaSubtypes), defaultValues: {
            age: "0",
            sex: "0",
            weight: "0",
            height: "0",
            glucose: "0",
            red_blood_cell: "0",
            b_symptoms: "0",
            lactate: "0",
            c_reactive_protein: "0",
            hemoglobin: "0",
            white_blood_cell: "0",
            lymphocites: "0"
        },
    })

    type Result = {
        color: string;
        value: string;
        comment: string;
        label: string;
    };

    function analyzeData(data: {
        relationship_with_drug_nbM: string,
        followupM: string,
        i_am_content_with_the_qualT: string
    }): Result[] {
        const relationship = data.relationship_with_drug_nbM === "0" ? 'Not related' : 'Related';
        const relationshipColor = data.relationship_with_drug_nbM === "0" ? 'bg-red-500' : 'bg-green-500';
        let followup = [
            'Not evaluated',
            'Progressive disease',
            'Stable disease',
            'Partial response',
            'Complete response',
        ][parseInt(data.followupM) - 1];
        if (followup === undefined) {
            followup = 'Not evaluated'
        }
        let followupColor = ['bg-purple-500', 'bg-red-500', 'bg-orange-500', 'bg-yellow-500', 'bg-green-500'][parseInt(data.followupM) - 1];
        if (followupColor === undefined) {
            followupColor = 'bg-purple-500'
        }

        let contentment = [
            'Not at all',
            'A little bit',
            'Somewhat',
            'Quite a bit',
            'Very much',
        ][parseInt(data.i_am_content_with_the_qualT) - 1];
        if (contentment === undefined) {
            contentment = 'Not at all'
        }
        let contentmentColor = ['bg-purple-500', 'bg-red-500', 'bg-orange-500', 'bg-yellow-500', 'bg-green-500'][parseInt(data.i_am_content_with_the_qualT) - 1];

        if (contentmentColor === undefined) {
            contentmentColor = 'bg-purple-500'
        }
        return [
            {
                color: relationshipColor,
                comment: relationship,
                value: (100).toString(),
                label: 'Relationship with medication'
            },
            {
                color: followupColor,
                comment: followup,
                value: (parseInt(data.followupM) * 20).toString(),
                label: "Follow-up"
            },
            {
                color: contentmentColor,
                comment: contentment,
                value: (parseInt(data.i_am_content_with_the_qualT) * 20).toString(),
                label: "Degree of contentment"
            },
        ];
    }

    async function onSubmitFactG(values: z.infer<typeof formSchemaFactg>) {
        setIsLoading(true)

        type Data = {
            sex: string;
            type?: string;
            score?: string;
            age_at_immunotherapy: string;
            cancer_type: string;
            education_level: string;
            financial_status: string;
        };
        let data: Data = values
        let score_type = ""
        if ('type' in data) {
            score_type = values.type
            const type = data.type;
            (data as any)[`${type}_score`] = data.score;
            delete data.type;
            delete data.score;
        }
        switch (score_type) {
            case "ius":
                try {
                    const response = await fetch('/predict_FACT_G_IUS', {
                        method: 'POST',
                        headers: {
                            "Access-Control-Allow-Origin": "*",
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify(data)
                    }).then((response) => {
                        return response.json();
                    });
                    setTimeout(() => {
                        setIsDialogOpen(true)
                        setField(Object.keys(response)[0])
                        let result = parseFloat(response[Object.keys(response)[0]]);
                        setValue(parseInt(String((result / 108) * 100)))
                        if (result <= 27) {
                            setComment(" Very poor quality of life (" + result + ")");
                            setColor("bg-red-300");
                        } else if (result >= 28 && result <= 54) {
                            setComment(" Poor quality of life (" + result + ")");
                            setColor("bg-orange-300");
                        } else if (result >= 55 && result <= 81) {
                            setComment(" Moderate quality of life (" + result + ")");
                            setColor("bg-yellow-300");
                        } else if (result >= 82 && result <= 108) {
                            setComment(" Good quality of life (" + result + ")");
                            setColor("bg-green-300");
                        } else {
                            setComment("Invalid prediction");
                        }
                    }, 800)

                } catch (error) {
                    console.log(error)
                    setTimeout(() => {
                        toast({
                            variant: "destructive",
                            title: "Network Error",
                            description: "Check your connection to the platform"
                        });
                    }, 3000)
                }

                setTimeout(() => {
                    setIsLoading(false)
                }, 3000)
                break;
            case "dpr":
                try {
                    const response = await fetch('/predict_FACT_G_DPR', {
                        method: 'POST',
                        headers: {
                            "Access-Control-Allow-Origin": "*",
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify(data)
                    }).then((response) => {
                        console.log(response)
                        return response.json();
                    });
                    setTimeout(() => {
                        setIsDialogOpen(true)
                        setField(Object.keys(response)[0])
                        let result = parseFloat(response[Object.keys(response)[0]]);
                        setValue(parseInt(String((result / 108) * 100)))
                        if (result <= 27) {
                            setComment(" Very poor quality of life (" + result + ")");
                            setColor("bg-red-300");
                        } else if (result >= 28 && result <= 54) {
                            setComment(" Poor quality of life (" + result + ")");
                            setColor("bg-orange-300");
                        } else if (result >= 55 && result <= 81) {
                            setComment(" Moderate quality of life (" + result + ")");
                            setColor("bg-yellow-300");
                        } else if (result >= 82 && result <= 108) {
                            setComment(" Good quality of life (" + result + ")");
                            setColor("bg-green-300");
                        } else {
                            setComment("Invalid prediction");
                        }
                    }, 800)

                } catch (error) {
                    console.log(error)
                    setTimeout(() => {
                        toast({
                            variant: "destructive",
                            title: "Network Error",
                            description: "Check your connection to the platform"
                        });
                    }, 3000)
                }

                setTimeout(() => {
                    setIsLoading(false)
                }, 3000)
                break;
            case "trh":
                try {
                    const response = await fetch('/predict_FACT_G_TRH', {
                        method: 'POST',
                        headers: {
                            "Access-Control-Allow-Origin": "*",
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify(data)
                    }).then((response) => {
                        return response.json();
                    });
                    setTimeout(() => {
                        setIsDialogOpen(true)
                        setField(Object.keys(response)[0])
                        let result = parseFloat(response[Object.keys(response)[0]]);
                        setValue(parseInt(String((result / 108) * 100)))
                        if (result <= 27) {
                            setComment(" Very poor quality of life (" + result + ")");
                            setColor("bg-red-300");
                        } else if (result >= 28 && result <= 54) {
                            setComment(" Poor quality of life (" + result + ")");
                            setColor("bg-orange-300");
                        } else if (result >= 55 && result <= 81) {
                            setComment(" Moderate quality of life (" + result + ")");
                            setColor("bg-yellow-300");
                        } else if (result >= 82 && result <= 108) {
                            setComment(" Good quality of life (" + result + ")");
                            setColor("bg-green-300");
                        } else {
                            setComment("Invalid prediction");
                        }
                    }, 800)

                } catch (error) {
                    console.log(error)
                    setTimeout(() => {
                        toast({
                            variant: "destructive",
                            title: "Network Error",
                            description: "Check your connection to the platform"
                        });
                    }, 3000)
                }

                setTimeout(() => {
                    setIsLoading(false)
                }, 3000)
                break;
            case "hls":
                try {
                    const response = await fetch('/predict_FACT_G_HLS', {
                        method: 'POST',
                        headers: {
                            "Access-Control-Allow-Origin": "*",
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify(data)
                    }).then((response) => {
                        return response.json();
                    });
                    setTimeout(() => {
                        setIsDialogOpen(true)
                        setField(Object.keys(response)[0])
                        let result = parseFloat(response[Object.keys(response)[0]]);
                        setValue(parseInt(String((result / 108) * 100)))
                        if (result <= 27) {
                            setComment(" Very poor quality of life (" + result + ")");
                            setColor("bg-red-300");
                        } else if (result >= 28 && result <= 54) {
                            setComment(" Poor quality of life (" + result + ")");
                            setColor("bg-orange-300");
                        } else if (result >= 55 && result <= 81) {
                            setComment(" Moderate quality of life (" + result + ")");
                            setColor("bg-yellow-300");
                        } else if (result >= 82 && result <= 108) {
                            setComment(" Good quality of life (" + result + ")");
                            setColor("bg-green-300");
                        } else {
                            setComment("Invalid prediction");
                        }
                    }, 800)

                } catch (error) {
                    console.log(error)
                    setTimeout(() => {
                        toast({
                            variant: "destructive",
                            title: "Network Error",
                            description: "Check your connection to the platform"
                        });
                    }, 3000)
                }

                setTimeout(() => {
                    setIsLoading(false)
                }, 3000)
                break;
            case "tss":
                try {
                    const response = await fetch('/predict_FACT_G_TSS', {
                        method: 'POST',
                        headers: {
                            "Access-Control-Allow-Origin": "*",
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify(data)
                    }).then((response) => {
                        return response.json();
                    });
                    setTimeout(() => {
                        setIsDialogOpen(true)
                        setField(Object.keys(response)[0])
                        let result = parseFloat(response[Object.keys(response)[0]]);
                        setValue(parseInt(String((result / 108) * 100)))
                        if (result <= 27) {
                            setComment(" Very poor quality of life (" + result + ")");
                            setColor("bg-red-300");
                        } else if (result >= 28 && result <= 54) {
                            setComment(" Poor quality of life (" + result + ")");
                            setColor("bg-orange-300");
                        } else if (result >= 55 && result <= 81) {
                            setComment(" Moderate quality of life (" + result + ")");
                            setColor("bg-yellow-300");
                        } else if (result >= 82 && result <= 108) {
                            setComment(" Good quality of life (" + result + ")");
                            setColor("bg-green-300");
                        } else {
                            setComment("Invalid prediction");
                        }
                    }, 800)

                } catch (error) {
                    console.log(error)
                    setTimeout(() => {
                        toast({
                            variant: "destructive",
                            title: "Network Error",
                            description: "Check your connection to the platform"
                        });
                    }, 3000)
                }

                setTimeout(() => {
                    setIsLoading(false)
                }, 3000)
                break;
        }
    }

    async function onSubmitSailliant(values: z.infer<typeof formSchemaSailliant>) {
        setIsLoading(true)
        let data = values;

        console.log(values)

        try {
            const response = await fetch('/predict_saillant', {
                method: 'POST',
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data)
            }).then((response) => {
                return response.json();
            });

            setTimeout(() => {
                setStateSaillant(response)
                setField("Saillant Features");
                setIsDialogOpen(true);
            }, 800)


        } catch (error) {
            console.log(error)
            setTimeout(() => {
                toast({
                    variant: "destructive",
                    title: "Network Error",
                    description: "Check your connection to the platform"
                });
            }, 3000)
        }

        setTimeout(() => {
            setIsLoading(false)
        }, 3000)
    }

    async function onSubmitSubtypes(values: z.infer<typeof formSchemaSubtypes>) {
        setIsLoading(true)
        let data = values;

        console.log(values)

        try {
            const response = await fetch('/subtypes', {
                method: 'POST',
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data)
            }).then((response) => {
                return response.json();
            });

            setTimeout(() => {
                setStateSubtypes(response)
                setField("SUBTYPES");
                setIsDialogOpen(true);
            }, 800)


        } catch (error) {
            console.log(error)
            setTimeout(() => {
                toast({
                    variant: "destructive",
                    title: "Network Error",
                    description: "Check your connection to the platform"
                });
            }, 3000)
        }

        setTimeout(() => {
            setIsLoading(false)
        }, 3000)
    }

    async function onSubmit(values: z.infer<typeof formSchema>) {
        setIsLoading(true)

        const username = sessionStorage.getItem('username');
        const password = sessionStorage.getItem('password');
        const attributes: string[] = [];
        const where: any = [];
        const models: string[] = [];
        const params: string = "6";

        const defaultValues = {
            study_id: '',
            date_of_birth: "",
            date_of_birth_operator: "",
            sex: 'any',
            cancer_type: "",
            i_have_nausea: '',
            i_have_pain: '',
            i_am_content_with_the_qual: "",
            immuno_frequency: "",
            model1_loop: false,
            model2: false,
            model3: false,
            model4: false
        };


        Object.entries(values).forEach(([key, value]) => {
            if (key.startsWith('model')) {
                // @ts-ignore
                if (value !== defaultValues[key]) {
                    models.push(key);
                }
            } else {
                // @ts-ignore
                if (key !== 'date_of_birth_operator') {
                    attributes.push(key);
                    // @ts-ignore
                    if (value !== defaultValues[key]) {
                        where.push({
                            attribute: key,
                            operator: key === 'date_of_birth' || key == 'immuno_frequency' ? values.date_of_birth_operator : "=",
                            value: value.toString()
                        });
                    }
                }
            }
        });

        const query = {
            attributes: attributes,
            from: ["csv_data_source"],
            where: where,
            group_by: [],
            order_by: [],
            aggregate: []
        };

        const formattedQuery = {
            username: username,
            password: password,
            query: query,
            params: params,
            models: models
        };

        console.log(formattedQuery)

        try {
            const response = await fetch('/predict', {
                method: 'POST',
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(formattedQuery)
            }).then((response) => {
                return response.json();
            });

            console.log(response)

            if (response.status === "success") {
                try {
                    navigate('/response', {
                        state: {
                            rawAttributes: attributes,
                            rawData: response.data,
                            rawResults: response.results
                        }
                    });
                } catch (error) {
                    setTimeout(() => {
                        toast({
                            variant: "destructive",
                            title: "Query Error",
                            description: "An error occurred while processing the query"
                        });
                    }, 3000)
                }
            }

        } catch (error) {
            setTimeout(() => {
                toast({
                    variant: "destructive",
                    title: "Network Error",
                    description: "Check your connection to the platform"
                });
            }, 3000)
        }

        setTimeout(() => {
            setIsLoading(false)
        }, 3000)
    }

    return (<div className="flex justify-center bg-cover h-screen bg-gradient-to-tr from-lime-300 to-sky-400 p-4">
        <div className="flex h-fit w-screen">
            <Dialog open={isDialogOpen} onOpenChange={setIsDialogOpen}>
                <DialogContent>
                    <DialogHeader>
                        <DialogDescription>
                            <div className="flex flex-row mt-2">
                                <div className="flex flex-row space-x-6 items-center justify-between">
                                    {field.startsWith("FACT_G") ? (
                                        <>
                                            <Progress
                                                value={value}
                                                className="w-[350px]"
                                                indicatorColor={color}
                                            />
                                            <h3 className="text-left italic">
                                                {(comment.charAt(0).toUpperCase() + comment.slice(1)).replace(/_/g, ' ')}
                                            </h3>
                                        </>
                                    ) : field.startsWith("SUBTYPES") ? (
                                        <Table>
                                            <TableCaption>Subtypes Data</TableCaption>
                                            <TableHeader>
                                                <TableRow>
                                                    <TableHead>Question</TableHead>
                                                    <TableHead>Answer</TableHead>
                                                </TableRow>
                                            </TableHeader>
                                            <TableBody>
                                                {Object.entries(JSON.parse(stateSubtypes.subtypes)).map(([key, value], index) => (
                                                    <TableRow key={index}>
                                                        <TableCell>{key}</TableCell>
                                                        <TableCell>{value}</TableCell>
                                                    </TableRow>
                                                ))}
                                            </TableBody>
                                        </Table>
                                    ) : (<div className="flex flex-col space-y-3 mt-2">
                                            {Object.keys(stateSaillant).map((key, index) => (
                                                <div>
                                                    <span>{analyzeData(JSON.parse(JSON.stringify(stateSaillant)))[index].label}</span>
                                                    <div
                                                        className="flex flex-row space-x-6 items-center justify-between mt-2">
                                                        <Progress
                                                            value={parseInt(analyzeData(JSON.parse(JSON.stringify(stateSaillant)))[index].value)}
                                                            className="w-[300px]"
                                                            indicatorColor={analyzeData(JSON.parse(JSON.stringify(stateSaillant)))[index].color}/>
                                                        <h3 className="text-left italic">{(analyzeData(JSON.parse(JSON.stringify(stateSaillant)))[index].comment.charAt(0).toUpperCase() + analyzeData(JSON.parse(JSON.stringify(stateSaillant)))[index].comment.slice(1)).replace(/_/g, ' ')}</h3>
                                                    </div>
                                                </div>
                                            ))}
                                        </div>
                                    )}
                                </div>
                            </div>
                        </DialogDescription>
                    </DialogHeader>
                </DialogContent>
            </Dialog>
            <Card className="bg-white backdrop-filter backdrop-blur-lg opacity-90 w-full">
                <div className="flex flex-col p-4">
                    <h3 className="text-2xl font-bold text-left">Patient form</h3>
                    <p className="text-sm text-left">Please select an appropriate model and fill in the form with the
                        desired patient characteristics.</p>
                </div>
                <Tabs defaultValue="sailliant">
                    <TabsList className="ml-4">
                        <TabsTrigger value="factg">Quality of Life (Fact-G score)</TabsTrigger>
                        <TabsTrigger value="sailliant">Patient's characteristics (Saillant features)</TabsTrigger>
                        <TabsTrigger value="subtypes">Patient's cluster (Subtypes)</TabsTrigger>
                        <TabsTrigger value="other">Data (Only for physicians)</TabsTrigger>
                    </TabsList>
                    <TabsContent value="factg">
                        <Form {...formFactg} >
                            <form onSubmit={formFactg.handleSubmit(onSubmitFactG)} className="p-4">
                                <Separator orientation="horizontal"/>
                                <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-2 gap-8 p-4">
                                    {Object.keys(formFieldsFactg).map((key) => {
                                        if (key === "score") {
                                            return (<FormField
                                                control={formFactg.control}
                                                name={key}
                                                render={({field}) => (<FormItem>
                                                    <div
                                                        className="flex flex-row justify-between items-center space-x-2"
                                                        key={key}>
                                                        <div>
                                                            <FormLabel>
                                                                {"type of score and value".toUpperCase().replace(/_/g, ' ')}
                                                            </FormLabel>
                                                            <FormDescription>{formFieldsFactgDesc[Object.keys(formFieldsFactg).indexOf(key)]}</FormDescription>
                                                        </div>
                                                        <div className="flex flex-row items-center space-x-2" key={key}>
                                                            <FormField
                                                                control={formFactg.control}
                                                                name="type"
                                                                render={({field}) => (<FormItem>
                                                                    <div
                                                                        className="flex flex-row justify-between items-center space-x-2"
                                                                        key={key}>
                                                                        <FormControl>
                                                                            <Select onValueChange={field.onChange}
                                                                                    defaultValue={field.value}>
                                                                                <SelectTrigger
                                                                                    className="h-10 w-24 rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background">
                                                                                    <SelectValue placeholder=""
                                                                                                 defaultValue="any"/>
                                                                                </SelectTrigger>
                                                                                <SelectContent {...field}
                                                                                               className="rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background z-10 p-4">
                                                                                    <SelectItem value="ius"
                                                                                                className="cursor-pointer">IUS</SelectItem>
                                                                                    <SelectItem value="dpr"
                                                                                                className="cursor-pointer">DPR</SelectItem>
                                                                                    <SelectItem value="trh"
                                                                                                className="cursor-pointer">TRH</SelectItem>
                                                                                    <SelectItem value="hls"
                                                                                                className="cursor-pointer">HLS</SelectItem>
                                                                                    <SelectItem value="tss"
                                                                                                className="cursor-pointer">TSS</SelectItem>
                                                                                </SelectContent>
                                                                            </Select>
                                                                        </FormControl>
                                                                    </div>
                                                                    <FormMessage/>
                                                                </FormItem>)}
                                                            />
                                                        </div>
                                                        <FormControl>
                                                            <Input type="number" min="0" className="w-20"
                                                                   {...field}/>
                                                        </FormControl>
                                                    </div>
                                                    <FormMessage/>
                                                </FormItem>)}
                                            />)
                                        }
                                        if (key === "age_at_immunotherapy") {
                                            return (<FormField
                                                control={formFactg.control}
                                                name={key}
                                                render={({field}) => (<FormItem>
                                                    <div
                                                        className="flex flex-row justify-between items-center space-x-2"
                                                        key={key}>
                                                        <div>
                                                            <FormLabel>
                                                                {key.toUpperCase().replace(/_/g, ' ')}
                                                            </FormLabel>
                                                            <FormDescription>{formFieldsFactgDesc[Object.keys(formFieldsFactg).indexOf(key)]}</FormDescription>
                                                        </div>
                                                        <FormControl>
                                                            <Input type="number" min="0" className="w-20"
                                                                   {...field}/>
                                                        </FormControl>
                                                    </div>
                                                    <FormMessage/>
                                                </FormItem>)}
                                            />)
                                        }
                                        if (key === "sex") {
                                            return (
                                                <FormField
                                                    control={formFactg.control}
                                                    name={key}
                                                    render={({field}) => (<FormItem>
                                                        <div
                                                            className="flex flex-row justify-between items-center space-x-2"
                                                            key={key}>
                                                            <div>
                                                                <FormLabel>
                                                                    {key.toUpperCase().replace(/_/g, ' ')}
                                                                </FormLabel>
                                                                <FormDescription>{formFieldsFactgDesc[Object.keys(formFieldsFactg).indexOf(key)]}</FormDescription>
                                                            </div>
                                                            <div className="flex flex-row items-center space-x-2"
                                                                 key={key}>
                                                                <FormField
                                                                    control={formFactg.control}
                                                                    name="sex"
                                                                    render={({field}) => (<FormItem>
                                                                        <div
                                                                            className="flex flex-row justify-between items-center space-x-2"
                                                                            key={key}>
                                                                            <FormControl>
                                                                                <Select onValueChange={field.onChange}
                                                                                        defaultValue={field.value}>
                                                                                    <SelectTrigger
                                                                                        className="h-10 w-24 rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background">
                                                                                        <SelectValue placeholder=""
                                                                                                     defaultValue="any"/>
                                                                                    </SelectTrigger>
                                                                                    <SelectContent {...field}
                                                                                                   className="rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background z-10 p-4">
                                                                                        <SelectItem value="1"
                                                                                                    className="cursor-pointer">Male</SelectItem>
                                                                                        <SelectItem value="0"
                                                                                                    className="cursor-pointer">Female</SelectItem>
                                                                                    </SelectContent>
                                                                                </Select>
                                                                            </FormControl>
                                                                        </div>
                                                                        <FormMessage/>
                                                                    </FormItem>)}
                                                                />
                                                            </div>
                                                        </div>
                                                        <FormMessage/>
                                                    </FormItem>)}
                                                />
                                            );
                                        }
                                        if (key === "cancer_type") {
                                            return (
                                                <FormField
                                                    control={formFactg.control}
                                                    name={key}
                                                    render={({field}) => (<FormItem>
                                                        <div
                                                            className="flex flex-row justify-between items-center space-x-2"
                                                            key={key}>
                                                            <div>
                                                                <FormLabel>
                                                                    {key.toUpperCase().replace(/_/g, ' ')}
                                                                </FormLabel>
                                                                <FormDescription>{formFieldsFactgDesc[Object.keys(formFieldsFactg).indexOf(key)]}</FormDescription>
                                                            </div>
                                                            <div className="flex flex-row items-center space-x-2"
                                                                 key={key}>
                                                                <FormField
                                                                    control={formFactg.control}
                                                                    name="cancer_type"
                                                                    render={({field}) => (<FormItem>
                                                                        <div
                                                                            className="flex flex-row justify-between items-center space-x-2"
                                                                            key={key}>
                                                                            <FormControl>
                                                                                <Select onValueChange={field.onChange}
                                                                                        defaultValue={field.value}>
                                                                                    <SelectTrigger
                                                                                        className="h-10 w-24 rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background">
                                                                                        <SelectValue placeholder=""
                                                                                                     defaultValue="any"/>
                                                                                    </SelectTrigger>
                                                                                    <SelectContent {...field}
                                                                                                   className="rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background z-10 p-4">
                                                                                        <SelectItem value="Melanoma"
                                                                                                    className="cursor-pointer">Melanoma</SelectItem>
                                                                                        <SelectItem value="Lymphoma"
                                                                                                    className="cursor-pointer">Lymphoma</SelectItem>
                                                                                        <SelectItem value="Lung"
                                                                                                    className="cursor-pointer">Lung</SelectItem>
                                                                                        <SelectItem value="Other Types"
                                                                                                    className="cursor-pointer">Other
                                                                                            types</SelectItem>
                                                                                    </SelectContent>
                                                                                </Select>
                                                                            </FormControl>
                                                                        </div>
                                                                        <FormMessage/>
                                                                    </FormItem>)}
                                                                />
                                                            </div>
                                                        </div>
                                                        <FormMessage/>
                                                    </FormItem>)}
                                                />
                                            );
                                        }
                                        if (key === "education_level") {
                                            return (
                                                <FormField
                                                    control={formFactg.control}
                                                    name={key}
                                                    render={({field}) => (<FormItem>
                                                        <div
                                                            className="flex flex-row justify-between items-center space-x-2"
                                                            key={key}>
                                                            <div>
                                                                <FormLabel>
                                                                    {key.toUpperCase().replace(/_/g, ' ')}
                                                                </FormLabel>
                                                                <FormDescription>{formFieldsFactgDesc[Object.keys(formFieldsFactg).indexOf(key)]}</FormDescription>
                                                            </div>
                                                            <div className="flex flex-row items-center space-x-2"
                                                                 key={key}>
                                                                <FormField
                                                                    control={formFactg.control}
                                                                    name="education_level"
                                                                    render={({field}) => (<FormItem>
                                                                        <div
                                                                            className="flex flex-row justify-between items-center space-x-2"
                                                                            key={key}>
                                                                            <FormControl>
                                                                                <Select onValueChange={field.onChange}
                                                                                        defaultValue={field.value}>
                                                                                    <SelectTrigger
                                                                                        className="h-10 w-24 rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background">
                                                                                        <SelectValue placeholder=""
                                                                                                     defaultValue="any"/>
                                                                                    </SelectTrigger>
                                                                                    <SelectContent {...field}
                                                                                                   className="rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background z-10 p-4">
                                                                                        <SelectItem value="0"
                                                                                                    className="cursor-pointer">Low</SelectItem>
                                                                                        <SelectItem value="1"
                                                                                                    className="cursor-pointer">High</SelectItem>
                                                                                    </SelectContent>
                                                                                </Select>
                                                                            </FormControl>
                                                                        </div>
                                                                        <FormMessage/>
                                                                    </FormItem>)}
                                                                />
                                                            </div>
                                                        </div>
                                                        <FormMessage/>
                                                    </FormItem>)}
                                                />
                                            );
                                        }
                                        if (key === "financial_status") {
                                            return (
                                                <FormField
                                                    control={formFactg.control}
                                                    name={key}
                                                    render={({field}) => (<FormItem>
                                                        <div
                                                            className="flex flex-row justify-between items-center space-x-2"
                                                            key={key}>
                                                            <div>
                                                                <FormLabel>
                                                                    {key.toUpperCase().replace(/_/g, ' ')}
                                                                </FormLabel>
                                                                <FormDescription>{formFieldsFactgDesc[Object.keys(formFieldsFactg).indexOf(key)]}</FormDescription>
                                                            </div>
                                                            <div className="flex flex-row items-center space-x-2"
                                                                 key={key}>
                                                                <FormField
                                                                    control={formFactg.control}
                                                                    name="financial_status"
                                                                    render={({field}) => (<FormItem>
                                                                        <div
                                                                            className="flex flex-row justify-between items-center space-x-2"
                                                                            key={key}>
                                                                            <FormControl>
                                                                                <Select onValueChange={field.onChange}
                                                                                        defaultValue={field.value}>
                                                                                    <SelectTrigger
                                                                                        className="h-10 w-24 rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background">
                                                                                        <SelectValue placeholder=""
                                                                                                     defaultValue="any"/>
                                                                                    </SelectTrigger>
                                                                                    <SelectContent {...field}
                                                                                                   className="rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background z-10 p-4">
                                                                                        <SelectItem value="0"
                                                                                                    className="cursor-pointer">Tight</SelectItem>
                                                                                        <SelectItem value="1"
                                                                                                    className="cursor-pointer">Comfortable</SelectItem>
                                                                                    </SelectContent>
                                                                                </Select>
                                                                            </FormControl>
                                                                        </div>
                                                                        <FormMessage/>
                                                                    </FormItem>)}
                                                                />
                                                            </div>
                                                        </div>
                                                        <FormMessage/>
                                                    </FormItem>)}
                                                />
                                            );
                                        }
                                    })}
                                </div>
                                <Separator orientation="horizontal" className="mt-4"/>
                                <div className="flex flex-row space-x-2 mt-6">
                                    <Button disabled={isLoading}
                                            className="bg-[#0074bc] hover:text-white hover:bg-green-600"
                                            type="submit">
                                        {isLoading && (<LoadingSpinner className="mr-2 h-4 w-4 animate-spin"/>)}
                                        Submit
                                    </Button>
                                </div>
                            </form>
                        </Form>
                    </TabsContent>
                    <TabsContent value="sailliant">
                        <Form {...formSailliant} >
                            <form onSubmit={formSailliant.handleSubmit(onSubmitSailliant)} className="p-4">
                                <Separator orientation="horizontal"/>
                                <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-2 gap-8 p-4">
                                    {Object.keys(formFieldsSailliant).map((key) => {
                                        return (<FormField
                                            control={formSailliant.control}
                                            name={key}
                                            render={({field}) => (<FormItem>
                                                <div
                                                    className="flex flex-row justify-between items-center space-x-2"
                                                    key={key}>
                                                    <div>
                                                        <FormLabel>
                                                            {key.toUpperCase().replace(/_/g, ' ')}
                                                        </FormLabel>
                                                        <FormDescription>{formFieldsSailliantDesc[Object.keys(formFieldsSailliant).indexOf(key)]}</FormDescription>
                                                    </div>
                                                    <FormControl>
                                                        <Input type="number" min="0" className="w-20"
                                                               {...field}/>
                                                    </FormControl>
                                                </div>
                                                <FormMessage/>
                                            </FormItem>)}
                                        />);
                                    })}
                                </div>
                                <Separator orientation="horizontal" className="mt-4"/>
                                <div className="flex flex-row space-x-2 mt-6">
                                    <Button disabled={isLoading}
                                            className="bg-[#0074bc] hover:text-white hover:bg-green-600"
                                            type="submit">
                                        {isLoading && (<LoadingSpinner className="mr-2 h-4 w-4 animate-spin"/>)}
                                        Submit
                                    </Button>
                                </div>
                            </form>
                        </Form></TabsContent>
                    <TabsContent value="subtypes">
                        <Form {...formSubtypes} >
                            <form onSubmit={formSubtypes.handleSubmit(onSubmitSubtypes)} className="p-4">
                                <Separator orientation="horizontal"/>
                                <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-2 gap-8 p-4">
                                    {Object.keys(formFieldsSubtypes).map((key) => {
                                        if (key === "sex") {
                                            return (
                                                <FormField
                                                    control={formFactg.control}
                                                    name={key}
                                                    render={({field}) => (<FormItem>
                                                        <div
                                                            className="flex flex-row justify-between items-center space-x-2"
                                                            key={key}>
                                                            <div>
                                                                <FormLabel>
                                                                    {key.toUpperCase().replace(/_/g, ' ')}
                                                                </FormLabel>
                                                                <FormDescription>{formFieldsSubtypesDesc[Object.keys(formFieldsSubtypes).indexOf(key)]}</FormDescription>
                                                            </div>
                                                            <div className="flex flex-row items-center space-x-2"
                                                                 key={key}>
                                                                <FormField
                                                                    control={formFactg.control}
                                                                    name="sex"
                                                                    render={({field}) => (<FormItem>
                                                                        <div
                                                                            className="flex flex-row justify-between items-center space-x-2"
                                                                            key={key}>
                                                                            <FormControl>
                                                                                <Select onValueChange={field.onChange}
                                                                                        defaultValue={field.value}>
                                                                                    <SelectTrigger
                                                                                        className="h-10 w-24 rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background">
                                                                                        <SelectValue placeholder=""
                                                                                                     defaultValue="any"/>
                                                                                    </SelectTrigger>
                                                                                    <SelectContent {...field}
                                                                                                   className="rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background z-10 p-4">
                                                                                        <SelectItem value="1"
                                                                                                    className="cursor-pointer">Male</SelectItem>
                                                                                        <SelectItem value="0"
                                                                                                    className="cursor-pointer">Female</SelectItem>
                                                                                    </SelectContent>
                                                                                </Select>
                                                                            </FormControl>
                                                                        </div>
                                                                        <FormMessage/>
                                                                    </FormItem>)}
                                                                />
                                                            </div>
                                                        </div>
                                                        <FormMessage/>
                                                    </FormItem>)}
                                                />
                                            );
                                        }
                                        if (key === "b_symptoms") {
                                            return (
                                                <FormField
                                                    control={formFactg.control}
                                                    name={key}
                                                    render={({field}) => (<FormItem>
                                                        <div
                                                            className="flex flex-row justify-between items-center space-x-2"
                                                            key={key}>
                                                            <div>
                                                                <FormLabel>
                                                                    {key.toUpperCase().replace(/_/g, ' ')}
                                                                </FormLabel>
                                                                <FormDescription>{formFieldsSubtypesDesc[Object.keys(formFieldsSubtypes).indexOf(key)]}</FormDescription>
                                                            </div>
                                                            <div className="flex flex-row items-center space-x-2"
                                                                 key={key}>
                                                                <FormField
                                                                    control={formFactg.control}
                                                                    name="sex"
                                                                    render={({field}) => (<FormItem>
                                                                        <div
                                                                            className="flex flex-row justify-between items-center space-x-2"
                                                                            key={key}>
                                                                            <FormControl>
                                                                                <Select onValueChange={field.onChange}
                                                                                        defaultValue={field.value}>
                                                                                    <SelectTrigger
                                                                                        className="h-10 w-24 rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background">
                                                                                        <SelectValue placeholder=""
                                                                                                     defaultValue="any"/>
                                                                                    </SelectTrigger>
                                                                                    <SelectContent {...field}
                                                                                                   className="rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background z-10 p-4">
                                                                                        <SelectItem value="1"
                                                                                                    className="cursor-pointer">Yes</SelectItem>
                                                                                        <SelectItem value="0"
                                                                                                    className="cursor-pointer">No</SelectItem>
                                                                                    </SelectContent>
                                                                                </Select>
                                                                            </FormControl>
                                                                        </div>
                                                                        <FormMessage/>
                                                                    </FormItem>)}
                                                                />
                                                            </div>
                                                        </div>
                                                        <FormMessage/>
                                                    </FormItem>)}
                                                />
                                            );
                                        }
                                        return (<FormField
                                            control={formSubtypes.control}
                                            name={key}
                                            render={({field}) => (<FormItem>
                                                <div
                                                    className="flex flex-row justify-between items-center space-x-2"
                                                    key={key}>
                                                    <div>
                                                        <FormLabel>
                                                            {key.toUpperCase().replace(/_/g, ' ')}
                                                        </FormLabel>
                                                        <FormDescription>{formFieldsSubtypesDesc[Object.keys(formFieldsSubtypes).indexOf(key)]}</FormDescription>
                                                    </div>
                                                    <FormControl>
                                                        <Input type="number" min="0" className="w-20"
                                                               {...field}/>
                                                    </FormControl>
                                                </div>
                                                <FormMessage/>
                                            </FormItem>)}
                                        />);
                                    })}
                                </div>
                                <Separator orientation="horizontal" className="mt-4"/>
                                <div className="flex flex-row space-x-2 mt-6">
                                    <Button disabled={isLoading}
                                            className="bg-[#0074bc] hover:text-white hover:bg-green-600"
                                            type="submit">
                                        {isLoading && (<LoadingSpinner className="mr-2 h-4 w-4 animate-spin"/>)}
                                        Submit
                                    </Button>
                                </div>
                            </form>
                        </Form></TabsContent>
                    <TabsContent value="other">
                        <Form {...form} >
                            <form onSubmit={form.handleSubmit(onSubmit)} className="p-4">
                                <Separator orientation="horizontal"/>
                                <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-2 gap-8 p-4">
                                    {Object.keys(formFieldsPatient).map((key) => {
                                        if (key === 'sex') {
                                            return (<FormField
                                                control={form.control}
                                                name={key}
                                                render={({field}) => (<FormItem>
                                                    <div
                                                        className="flex flex-row justify-between items-center space-x-2"
                                                        key={key}>
                                                        <div>
                                                            <FormLabel>
                                                                {key.toUpperCase().replace(/_/g, ' ')}
                                                            </FormLabel>
                                                            <FormDescription>{formFieldsPatientsDesc[Object.keys(formFieldsPatient).indexOf(key)]}</FormDescription>
                                                        </div>
                                                        <FormControl>
                                                            <Select onValueChange={field.onChange}
                                                                    defaultValue={field.value}>
                                                                <SelectTrigger
                                                                    className="h-10 w-24 rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background">
                                                                    <SelectValue placeholder="Any" defaultValue="any"/>
                                                                </SelectTrigger>
                                                                <SelectContent {...field}
                                                                               className="rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background z-10 p-4">
                                                                    <SelectItem value="0"
                                                                                className="cursor-pointer">Male</SelectItem>
                                                                    <SelectItem value="1"
                                                                                className="cursor-pointer">Female</SelectItem>
                                                                </SelectContent>
                                                            </Select>
                                                        </FormControl>
                                                    </div>
                                                    <FormMessage/>
                                                </FormItem>)}
                                            />);
                                        }
                                        if (key === 'date_of_birth' || key === 'immuno_frequency') {
                                            return (<FormField
                                                control={form.control}
                                                name={key}
                                                render={({field}) => (<FormItem>
                                                    <div
                                                        className="flex flex-row justify-between items-center space-x-2"
                                                        key={key}>
                                                        <div>
                                                            <FormLabel>
                                                                {key.toUpperCase().replace(/_/g, ' ')}
                                                            </FormLabel>
                                                            <FormDescription>{formFieldsPatientsDesc[Object.keys(formFieldsPatient).indexOf(key)]}</FormDescription>
                                                        </div>
                                                        <div className="flex flex-row items-center space-x-2" key={key}>
                                                            <FormField
                                                                control={form.control}
                                                                name="date_of_birth_operator"
                                                                render={({field}) => (<FormItem>
                                                                    <div
                                                                        className="flex flex-row justify-between items-center space-x-2"
                                                                        key={key}>
                                                                        <FormControl>
                                                                            <Select onValueChange={field.onChange}
                                                                                    defaultValue={field.value}>
                                                                                <SelectTrigger
                                                                                    className="h-10 w-24 rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background">
                                                                                    <SelectValue placeholder="="
                                                                                                 defaultValue="any"/>
                                                                                </SelectTrigger>
                                                                                <SelectContent {...field}
                                                                                               className="rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background z-10 p-4">
                                                                                    <SelectItem value="="
                                                                                                className="cursor-pointer">=</SelectItem>
                                                                                    <SelectItem value=">"
                                                                                                className="cursor-pointer">{">"}</SelectItem>
                                                                                    <SelectItem value="<"
                                                                                                className="cursor-pointer">{"<"}</SelectItem>
                                                                                </SelectContent>
                                                                            </Select>
                                                                        </FormControl>
                                                                    </div>
                                                                    <FormMessage/>
                                                                </FormItem>)}
                                                            />
                                                            <FormControl>
                                                                <Input type="number" min="0" className="w-20"
                                                                       {...field}/>
                                                            </FormControl>
                                                        </div>
                                                    </div>
                                                    <FormMessage/>
                                                </FormItem>)}
                                            />);
                                        }
                                        if (key === 'study_id') {
                                            return (<FormField
                                                control={form.control}
                                                name={key}
                                                render={({field}) => (<FormItem>
                                                    <div
                                                        className="flex flex-row justify-between items-center space-x-2"
                                                        key={key}>
                                                        <div>
                                                            <FormLabel>
                                                                {key.toUpperCase().replace(/_/g, ' ')}
                                                            </FormLabel>
                                                            <FormDescription>{formFieldsPatientsDesc[Object.keys(formFieldsPatient).indexOf(key)]}</FormDescription>
                                                        </div>
                                                        <div className="flex flex-row items-center space-x-2" key={key}>
                                                            <FormControl>
                                                                <Input type="number" min="0" className="w-20"
                                                                       {...field}/>
                                                            </FormControl>
                                                        </div>
                                                    </div>
                                                    <FormMessage/>
                                                </FormItem>)}
                                            />);
                                        }
                                        if (key === 'cancer_type') {
                                            return (<FormField
                                                control={form.control}
                                                name={key}
                                                render={({field}) => (<FormItem>
                                                    <div
                                                        className="flex flex-row justify-between items-center space-x-2"
                                                        key={key}>
                                                        <div>
                                                            <FormLabel>
                                                                {key.toUpperCase().replace(/_/g, ' ')}
                                                            </FormLabel>
                                                            <FormDescription>{formFieldsPatientsDesc[Object.keys(formFieldsPatient).indexOf(key)]}</FormDescription>
                                                        </div>
                                                        <FormControl>
                                                            <Select onValueChange={field.onChange}
                                                                    defaultValue={field.value}>
                                                                <SelectTrigger
                                                                    className="h-10 w-24 rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background">
                                                                    <SelectValue placeholder="" defaultValue=""/>
                                                                </SelectTrigger>
                                                                <SelectContent {...field}
                                                                               className="rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background z-10 p-4">
                                                                    <SelectItem value="0"
                                                                                className="cursor-pointer">Melanoma</SelectItem>
                                                                    <SelectItem value="1"
                                                                                className="cursor-pointer">Lymphoma</SelectItem>
                                                                    <SelectItem value="2"
                                                                                className="cursor-pointer">Lung</SelectItem>
                                                                    <SelectItem value="3"
                                                                                className="cursor-pointer">Other
                                                                        types</SelectItem>
                                                                </SelectContent>
                                                            </Select>
                                                        </FormControl>
                                                    </div>
                                                    <FormMessage/>
                                                </FormItem>)}
                                            />);
                                        } else {
                                            // @ts-ignore
                                            return (<FormField control={form.control} name={key}
                                                               render={({field}) => (<FormItem>
                                                                   <div
                                                                       className="flex flex-row justify-between items-center space-x-2"
                                                                       key={key}>
                                                                       <div>
                                                                           <FormLabel>
                                                                               {key.toUpperCase().replace(/_/g, ' ')}
                                                                           </FormLabel>
                                                                           <FormDescription>{formFieldsPatientsDesc[Object.keys(formFieldsPatient).indexOf(key)]}</FormDescription>
                                                                       </div>
                                                                       <FormControl>
                                                                           <Select onValueChange={field.onChange}
                                                                                   defaultValue={field.value}>
                                                                               <SelectTrigger
                                                                                   className="h-10 w-24 rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background">
                                                                                   <SelectValue placeholder=""
                                                                                                defaultValue=""/>
                                                                               </SelectTrigger>
                                                                               <SelectContent {...field}
                                                                                              className="rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background z-10 p-4">
                                                                                   <SelectItem value="1"
                                                                                               className="cursor-pointer">Not
                                                                                       at all</SelectItem>
                                                                                   <SelectItem value="2"
                                                                                               className="cursor-pointer">A
                                                                                       little bit</SelectItem>
                                                                                   <SelectItem value="3"
                                                                                               className="cursor-pointer">Somewhat</SelectItem>
                                                                                   <SelectItem value="4"
                                                                                               className="cursor-pointer">Quite
                                                                                       a bit</SelectItem>
                                                                                   <SelectItem value="5"
                                                                                               className="cursor-pointer">Very
                                                                                       much</SelectItem>
                                                                               </SelectContent>
                                                                           </Select>
                                                                       </FormControl>
                                                                   </div>
                                                                   <FormMessage/>
                                                               </FormItem>)}
                                            />);
                                        }
                                    })}
                                </div>
                                <Separator orientation="horizontal" className="mt-4"/>
                                <div className="flex flex-row space-x-2 mt-6">
                                    <Button disabled={isLoading}
                                            className="bg-[#0074bc] hover:text-white hover:bg-green-600"
                                            type="submit">
                                        {isLoading && (<LoadingSpinner className="mr-2 h-4 w-4 animate-spin"/>)}
                                        Submit
                                    </Button>
                                </div>
                            </form>
                        </Form>
                    </TabsContent>
                </Tabs>
            </Card>
        </div>
    </div>);
}