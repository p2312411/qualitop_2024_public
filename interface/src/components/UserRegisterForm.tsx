import * as React from "react"
import {Button} from "../../shadcn/components/ui/button.tsx";
import {Input} from "../../shadcn/components/ui/input.tsx";
import {LoadingSpinner} from "./LoadingSpinner.tsx";
import {
    Select,
    SelectContent,
    SelectItem,
    SelectTrigger,
    SelectValue,
} from "../../shadcn/components/ui/select"
import {useToast} from "../../shadcn/components/ui/use-toast.ts"
import {z} from "zod";
import {useForm} from "react-hook-form";
import {zodResolver} from "@hookform/resolvers/zod";
import {FormControl, FormField, FormItem, Form} from "../../shadcn/components/ui/form.tsx";
import {useNavigate} from "react-router-dom";

const formSchema = z.object({
    username: z.string(), password: z.string(), firstname: z.string(), lastname: z.string(), role: z.string()
});


export function UserRegisterForm() {
    const [isLoading, setIsLoading] = React.useState<boolean>(false)
    const {toast} = useToast();
    const navigate = useNavigate();

    const form = useForm<z.infer<typeof formSchema>>({
        resolver: zodResolver(formSchema), defaultValues: {
            username: "", password: "", firstname: "", lastname: "", role: ""
        },
    })

    async function onSubmit(values: z.infer<typeof formSchema>) {
        setIsLoading(true);

        const new_username = values.username;
        const new_password = values.password;
        const new_firstname = values.firstname;
        const new_lastname = values.lastname;
        const new_role = [values.role]

        try {
            /*const response = await fetch('/register', {
                method: 'POST',
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({username: new_username, password: new_password, roles: new_role}),
            }).then((response) => {
                return response.json();
            });*/

            setTimeout(() => {
                toast({
                    variant: "success",
                    title: `Welcome to the platform ${new_firstname} ${new_lastname} !`,
                    description: "Your request was successfully transmitted to the administrator.",
                })
            }, 3000)
            setTimeout(() => {
                navigate('/login');
            }, 4000)

            /*if (response.status === 201) {
                sessionStorage.setItem('username', new_username);
                sessionStorage.setItem('password', new_password);
                sessionStorage.setItem('name', `${new_firstname} ${new_lastname}`);
                sessionStorage.setItem('role', values.role);
                setTimeout(() => {
                    toast({
                        variant: "success",
                        title: `Welcome to the platform ${new_firstname} ${new_lastname} !`,
                        description: response.message + ".",
                    })
                }, 3000)
                setTimeout(() => {
                    navigate('/home');
                }, 4000)
            } else {
                setTimeout(() => {
                    toast({
                        variant: "destructive",
                        title: response.error,
                        description: response.message,
                    })
                }, 3000)
            }*/
        } catch (error) {
            setTimeout(() => {
                toast({
                    variant: "destructive",
                    title: "Network Error"
                });
            }, 3000)
        }

        setTimeout(() => {
            setIsLoading(false)
        }, 3000)
    }

    return (<Form {...form} >
            <form onSubmit={form.handleSubmit(onSubmit)}>
                <div className="grid gap-2">
                    <div className="grid gap-1">
                        <FormField
                            control={form.control}
                            name="firstname"
                            render={({field}) => (<FormItem>
                                <FormControl>
                                    <Input
                                        id="firstname"
                                        placeholder="John"
                                        type="text"
                                        autoCapitalize="none"
                                        autoCorrect="off"
                                        disabled={isLoading}
                                        {...field}
                                    />
                                </FormControl>
                            </FormItem>)}
                        />
                    </div>
                    <div className="grid gap-1">
                        <FormField
                            control={form.control}
                            name="lastname"
                            render={({field}) => (<FormItem>
                                <FormControl>
                                    <Input
                                        id="lastname"
                                        placeholder="Doe"
                                        type="text"
                                        autoCapitalize="none"
                                        autoCorrect="off"
                                        disabled={isLoading}
                                        {...field}
                                    />
                                </FormControl>
                            </FormItem>)}
                        />
                    </div>
                    <div className="grid gap-1">
                        <FormField
                            control={form.control}
                            name="username"
                            render={({field}) => (<FormItem>
                                <FormControl>
                                    <Input
                                        id="username"
                                        placeholder="john_doe_42"
                                        type="text"
                                        autoCapitalize="none"
                                        autoCorrect="off"
                                        disabled={isLoading}
                                        {...field}
                                    />
                                </FormControl>
                            </FormItem>)}
                        />
                    </div>
                    <div className="grid gap-1">
                        <FormField
                            control={form.control}
                            name="password"
                            render={({field}) => (<FormItem>
                                <FormControl>
                                    <Input
                                        id="password"
                                        placeholder="*****"
                                        type="password"
                                        autoCapitalize="none"
                                        autoCorrect="off"
                                        disabled={isLoading}
                                        {...field}
                                    />
                                </FormControl>
                            </FormItem>)}
                        />
                    </div>

                    <div className="grid gap-1">
                        <FormField
                            control={form.control}
                            name="role"
                            render={({field}) => (<FormItem>
                                <FormControl>
                                    <Select onValueChange={field.onChange}
                                            defaultValue={field.value}>
                                        <SelectTrigger>
                                            <SelectValue placeholder="Role"/>
                                        </SelectTrigger>
                                        <SelectContent {...field}>
                                            <SelectItem value="DOCTOR">Doctor</SelectItem>
                                            <SelectItem value="ADMIN">Admin</SelectItem>
                                            <SelectItem value="NURSE">Nurse</SelectItem>
                                        </SelectContent>
                                    </Select>
                                </FormControl>
                            </FormItem>)}
                        />
                    </div>

                    <Button disabled={isLoading} type="submit">
                        {isLoading && (<LoadingSpinner className="mr-2 h-4 w-4 animate-spin"/>)}
                        Submit
                    </Button>
                </div>
            </form>
        </Form>
    )
}