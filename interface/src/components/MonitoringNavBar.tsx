

import {Component} from "react";

export class MonitoringNavBar extends Component<{ activePage: string }> {
    render() {
        let {activePage} = this.props;
        return (
            <nav className="flex items-center space-x-4 lg:space-x-6">
            <a
                href="/home"
        className={`text-sm font-medium transition-colors hover:text-primary ${activePage !== 'home' ? 'text-muted-foreground' : ''}`}
    >
        Home
        </a>
        <a
        href="/users"
        className={`text-sm font-medium transition-colors hover:text-primary ${activePage !== 'users' ? 'text-muted-foreground' : ''}`}
    >
            User Management
        </a>
        <a
        href="/system"
        className={`text-sm font-medium transition-colors hover:text-primary ${activePage !== 'system' ? 'text-muted-foreground' : ''}`}
    >
        System settings
        </a>
        </nav>
    );
    }
}
