import {Progress} from "../../shadcn/components/ui/progress.tsx";

// @ts-ignore
export const ModelResult = ({model, result}) => {
    let comment = "Invalid prediction";
    let color = "bg-black-300";
    let value = 0;

    switch (model) {
        case "model1" :
            result = result["fact_g"]
            value = (result / 108) * 100
            if (result >= 0 && result <= 27) {
                comment = " Very poor quality of life (avg " + result + ")";
                color = "bg-red-300";
            } else if (result >= 28 && result <= 54) {
                comment = " Poor quality of life (avg " + result + ")";
                color = "bg-orange-300";
            } else if (result >= 55 && result <= 81) {
                comment = " Moderate quality of life (avg " + result + ")";
                color = "bg-yellow-300";
            } else if (result >= 82 && result <= 108) {
                comment =" Good quality of life (avg " + result + ")";
                color = "bg-green-300";
            } else {
                comment = "Invalid prediction";
            }
            break;

        case "model2" :
            for (const key in result) {
                if (key === "highest_grade_max" || key === "highest_grade_min") {
                    value = (result[key] / 108) * 100 // TODO: max value + remove value
                    if (result[key] < 1) {
                        comment = key + " (avg " + result[key] + ")";
                        color = "bg-red-500";
                        value = 100;
                    } else if (result[key] > 1) {
                        comment = key + " (avg " + result[key] + ")";
                        color = "bg-green-500";
                        value = 100
                    } else if (result[key] == 1) {
                        comment = key + " (avg " + result[key] + ")";
                        color = "bg-yellow-300";
                        value = 100
                    } else {
                        comment = "Invalid prediction";
                    }

                } else {
                    if (result[key] == 0) {
                        comment = key + " (" + result[key] * 100 + "%)";
                        color = "bg-red-500";
                        value = 100
                    } else if (result[key] == 1) {
                        comment = key + " (" + result[key] * 100 + "%)";
                        color = "bg-green-500";
                        value = result[key] * 100
                    } else if (result[key] < 0.5) {
                        comment = key + " (" + result[key] * 100 + "%)";
                        color = "bg-orange-500";
                        value = result[key] * 100;
                    } else if (result[key] < 0.5) {
                        comment = key + " (" + result[key] * 100 + "%)";
                        color = "bg-yellow-500";
                        value = result[key] * 100;
                    } else {
                        comment = "Invalid prediction";
                    }

                }
            }
            break;

        case "model3" :
            value = (result / 108) * 100
            comment = "Very poor quality of life";
            color = "bg-royal-300";
            value = result / 100;
            break;

        case "model4" :
            value = (result / 108) * 100
            comment = "Very poor quality of life";
            color = "bg-purple-300";
            value = result / 100;
            break;

    }

    return (
        <div className="flex flex-row">
            <div className="flex flex-row space-x-8 items-center justify-between">
                <Progress
                    value={value}
                    className="w-[400px]"
                    indicatorColor={color} />
                <h3 className="text-left italic">{(comment.charAt(0).toUpperCase() + comment.slice(1)).replace(/_/g, ' ')}</h3>
            </div>
        </div>
    )
}

export default ModelResult;