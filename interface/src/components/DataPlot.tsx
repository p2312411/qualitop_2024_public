import {BarChart, Bar, YAxis, Tooltip, XAxis, ResponsiveContainer} from 'recharts';

// @ts-ignore
function DataPlot({attributes, data, name}) {
    if (name !== "date_of_birth" && name !== "immuno_frequency") {
        return null;
    }

    const extractDataForFreq = (attrName: any) => {
        const index = attributes.indexOf(attrName);
        if (index === -1) return [];

        const countMap = new Map();
        data.forEach((row: { [x: string]: any; }) => {
            const key = row[index];
            countMap.set(key, (countMap.get(key) || 0) + 1);
        });
        return Array.from(countMap, ([name, value]) => ({name, value}))
            .sort((a, b) => a.name - b.name);
    };

    // @ts-ignore
    const extractDataForCat = (attrName) => {
        console.log(attrName)
        const index = attributes.indexOf(attrName);
        const bins = 10;
        if (index === -1) return [];

        const values = data.map((row: { [x: string]: string; }) => parseFloat(row[index]));
        const min = Math.min(...values);
        const max = Math.max(...values);
        const binWidth = (max - min) / bins;

        const countMap = new Map();
        for (let i = 0; i < bins; i++) {
            const binStart = min + i * binWidth;
            const binEnd = binStart + binWidth;
            const binLabel = `${binStart.toFixed(2)} - ${binEnd.toFixed(2)}`;
            countMap.set(binLabel, 0);
        }
        values.forEach((value: number) => {
            const binIndex = Math.min(Math.floor((value - min) / binWidth), bins - 1);
            const binStart = min + binIndex * binWidth;
            const binEnd = binStart + binWidth;
            const binLabel = `${binStart.toFixed(2)} - ${binEnd.toFixed(2)}`;
            countMap.set(binLabel, countMap.get(binLabel) + 1);
        });
        return Array.from(countMap, ([name, value]) => ({name, value}))
            .sort((a, b) => parseFloat(a.name.split(' - ')[0]) - parseFloat(b.name.split(' - ')[0]));
    };


    if (name === "date_of_birth") {
        const chartData = extractDataForCat(name);
        return name ? (
            <div className="flex flex-col justify-center">
                <div className="w-full h-full">
                    <ResponsiveContainer width="100%" height={"100%"} minWidth={350} minHeight={300}>
                        <BarChart data={chartData}>
                            <XAxis dataKey="name" tick={false}/>
                            <YAxis/>
                            <Tooltip cursor={{fill: 'transparent'}}/>
                            <Bar dataKey="value" fill="#8884d8"/>
                        </BarChart>
                    </ResponsiveContainer>
                </div>
            </div>
        ) : <span></span>;

    } else if (name === "immuno_frequency") {
        const chartData = extractDataForFreq(name);
        if (chartData.length === 0) return null;
        return name ? (
            <div className="flex flex-col justify-center">
                <div className="w-full h-full">
                    <ResponsiveContainer width="100%" height={"100%"} minWidth={350} minHeight={300}>
                        <BarChart data={chartData}>
                            <XAxis dataKey="name" tick={false}/>
                            <YAxis/>
                            <Tooltip cursor={{fill: 'transparent'}}/>
                            <Bar dataKey="value" fill="#82ca9d" barSize={20}/>
                        </BarChart>
                    </ResponsiveContainer>
                </div>
            </div>
        ) : <span></span>;
    }
}

export default DataPlot;
