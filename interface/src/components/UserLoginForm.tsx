import * as React from "react"
import {Button} from "../../shadcn/components/ui/button.tsx";
import {Input} from "../../shadcn/components/ui/input.tsx";
import {LoadingSpinner} from "./LoadingSpinner.tsx";
import {useToast} from "../../shadcn/components/ui/use-toast.ts"
import {z} from "zod";
import {useForm} from "react-hook-form";
import {zodResolver} from "@hookform/resolvers/zod";
import {FormControl, FormField, FormItem, Form} from "../../shadcn/components/ui/form.tsx";
import {useNavigate} from "react-router-dom";


const formSchema = z.object({
    username: z.string(), password: z.string()
});

export function UserLoginForm() {
    const [isLoading, setIsLoading] = React.useState<boolean>(false)
    const {toast} = useToast();
    const navigate = useNavigate();

    // to remove
    /*sessionStorage.setItem('username', 'test');
    sessionStorage.setItem('password', 'test');*/
    sessionStorage.setItem('name', 'qualitop user');
    sessionStorage.setItem('role', 'DOCTOR');

    const form = useForm<z.infer<typeof formSchema>>({
        resolver: zodResolver(formSchema), defaultValues: {
            username: '', password: '',
        },
    })

    async function onSubmit(values: z.infer<typeof formSchema>) {
        setIsLoading(true)

        const new_username = values.username;
        const new_password = values.password;

        try {
            const response = await fetch('/login', {
                method: 'POST',
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({username: new_username, password: new_password}),
            }).then((response) => {
                return response.json();
            });

            if (response.status === 200) {
                sessionStorage.setItem('username', new_username);
                sessionStorage.setItem('password', new_password);
                sessionStorage.setItem('name', "qualitop user");

                setTimeout(() => {
                    toast({
                        variant: "success",
                        title: `Welcome back ${new_username} !`,
                        description: response.message,
                    });
                }, 3000)
                setTimeout(() => {
                    navigate('/home');
                }, 4000)
            } else {
                setTimeout(() => {
                    toast({
                        variant: "destructive",
                        title: response.error,
                        description: response.message,
                    });
                }, 3000)
            }
        } catch (error) {
            setTimeout(() => {
                toast({
                    variant: "destructive",
                    title: "Network Error"
                });
            }, 3000)
        }

        setTimeout(() => {
            setIsLoading(false);
        }, 3000)
    }

    return (<div>
        <Form {...form} >
            <form onSubmit={form.handleSubmit(onSubmit)}>
                <div className="grid gap-2">
                    <div className="grid gap-1">
                        <FormField
                            control={form.control}
                            name="username"
                            render={({field}) => (<FormItem>
                                <FormControl>
                                    <Input
                                        id="username"
                                        placeholder="qualitop_user"
                                        type="text"
                                        autoCapitalize="none"
                                        autoCorrect="off"
                                        disabled={isLoading}
                                        {...field}
                                    />
                                </FormControl>
                            </FormItem>)}
                        />
                    </div>
                    <div className="grid gap-1">
                        <FormField
                            control={form.control}
                            name="password"
                            render={({field}) => (<FormItem>
                                <FormControl>
                                    <Input
                                        id="password"
                                        placeholder="*****"
                                        type="password"
                                        autoCapitalize="none"
                                        autoCorrect="off"
                                        disabled={isLoading}
                                        {...field}
                                    />
                                </FormControl>
                            </FormItem>)}
                        />
                    </div>
                    <Button type="submit" disabled={isLoading}>
                        {isLoading && (<LoadingSpinner className="mr-2 h-4 w-4 animate-spin"/>)}
                        Submit
                    </Button>
                </div>
            </form>
        </Form>
    </div>)
}