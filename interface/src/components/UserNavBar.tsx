import {Button} from "../../shadcn/components/ui/button.tsx";
import {
    DropdownMenu,
    DropdownMenuContent,
    DropdownMenuGroup,
    DropdownMenuItem,
    DropdownMenuLabel,
    DropdownMenuSeparator,
    DropdownMenuTrigger,
} from "../../shadcn/components/ui/dropdown-menu"
import {Avatar, AvatarFallback} from "../../shadcn/components/ui/avatar.tsx";
import {useNavigate} from "react-router-dom";
import {useToast} from "../../shadcn/components/ui/use-toast.ts";
import {LogOut, Settings, User} from "lucide-react";
import {Badge} from "../../shadcn/components/ui/badge.tsx";


export const extractAndConcatToUpper = (input: string): string => {
    const words = input.split(' ');
    const concatenated = words.map(word => word.charAt(0)).join('');
    return concatenated.toUpperCase();
};

export const toLowerCaseAndUpperCaseFirstLetter = (input: string): string => {
    return input.charAt(0).toUpperCase() + input.slice(1).toLowerCase();
}


// @ts-ignore
export function UserNavBar({username, name, role}) {
    const navigate = useNavigate();
    const {toast} = useToast();
    const roleBagdeColor = role === 'DOCTOR' ? 'green' : role === 'NURSE' ? 'yellow' : 'red';

    function logout() {
        sessionStorage.removeItem('username');
        sessionStorage.removeItem('password');
        sessionStorage.removeItem('name');
        setTimeout(() => {
            navigate('/login');
        }, 1000)
        setTimeout(() => {
            toast({
                variant: "success",
                title: `Logging out !`,
                description: "You have been logged out successfully.",
            });
        }, 2000)
    }

    return (
        <DropdownMenu>
            <DropdownMenuTrigger asChild>
                <Button variant="ghost" className="relative h-8 w-8 rounded-full">
                    <Avatar className="h-8 w-8">
                        <AvatarFallback>{extractAndConcatToUpper(name)}</AvatarFallback>
                    </Avatar>
                </Button>
            </DropdownMenuTrigger>
            <DropdownMenuContent className="w-56" align="end" forceMount>
                <DropdownMenuLabel className="font-normal">
                    <div className="flex items-center space-x-3">
                        <div className="flex items-center justify-center">
                            <User className="h-6 w-6"/>
                        </div>
                        <div className="flex flex-col space-y-1">
                            <p className="text-sm font-medium leading-none">{name}</p>
                            <p className="text-xs leading-none text-muted-foreground">
                                {username}
                            </p>
                        </div>
                    </div>
                    <p className="text-xs leading-none mt-2">
                        <Badge style={{backgroundColor: roleBagdeColor}}>{toLowerCaseAndUpperCaseFirstLetter(role)}</Badge>
                    </p>
                </DropdownMenuLabel>
                <DropdownMenuSeparator/>
                <DropdownMenuGroup>
                    <DropdownMenuItem>
                        <Settings className="mr-2 h-4 w-4"/>
                        <span className="mr-0">Admin</span>
                    </DropdownMenuItem>
                </DropdownMenuGroup>
                <DropdownMenuSeparator/>
                <DropdownMenuItem onClick={logout}>
                    <LogOut className="mr-2 h-4 w-4"/>
                    <span>Log out</span>
                </DropdownMenuItem>
            </DropdownMenuContent>
        </DropdownMenu>
    )
}