import {BarChart, Bar, YAxis, Tooltip, XAxis, ResponsiveContainer} from 'recharts';

// @ts-ignore
function RepartitionPlot({attributes, data, names}) {

    const convertDataForStackedBarChart = (data) => {
        const counts = data.reduce((acc, item) => {
            const { age, sex, bmi, ...rest } = item;
            Object.entries(rest).forEach(([key, value]) => {
                if (!acc[key]) {
                    acc[key] = { true: 0, false: 0 };
                }
                acc[key][value]++;
            });
            return acc;
        }, {});

        return Object.entries(counts).map(([key, value]) => ({
            name: key,
            true: value.true,
            false: value.false,
        }));
    };


    const chartData = convertDataForStackedBarChart(data);

    return "error" ? (
        <div className="flex flex-col justify-center ">
            <div className="w-full h-full">
                <ResponsiveContainer width="100%" height={"100%"} minWidth={700} minHeight={300}>
                    <BarChart data={chartData}>
                        <XAxis dataKey="name" tick={false}/>
                        <YAxis/>
                        <Tooltip cursor={{fill: 'transparent'}}/>
                        <Bar dataKey="true" stackId="a" fill="#8884d8" />
                        <Bar dataKey="false" stackId="a" fill="#82ca9d" />
                    </BarChart>
                </ResponsiveContainer>
            </div>
        </div>
    ) : <span></span>;
}

export default RepartitionPlot;