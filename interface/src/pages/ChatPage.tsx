import Background from "../assets/qualitop-logo.png";
import {MainNavBar} from "../components/MainNavBar.tsx";
import {Search} from "../components/Search.tsx";
import {UserNavBar} from "../components/UserNavBar.tsx";
import {ChatLayout} from "../../shadcn/chat/ChatLayout.tsx";
import {Card} from "../../shadcn/components/ui/card.tsx";
import {Separator} from "../../shadcn/components/ui/separator.tsx";
import {Select, SelectItem, SelectContent, SelectTrigger, SelectValue} from "../../shadcn/components/ui/select.tsx";


export const ChatPage = () => {

    const username = sessionStorage.getItem('username');
    const name = sessionStorage.getItem('name');
    const role = sessionStorage.getItem('role');
    sessionStorage.setItem('model', "0");

    function setModel(value: string) {
        sessionStorage.setItem('model', value);
        window.dispatchEvent(new Event('storage'));
    }

    return (<div>
        <div
            className="hidden flex-col md:flex sticky bg-white top-0 w-full z-10 backdrop-filter backdrop-blur-lg bg-opacity-30">
            <div>
                <div className="flex h-16 flex-row justify-between items-center px-4">
                    <img src={Background} className="h-6 mr-6" alt="qualitop-logo"/>
                    <MainNavBar activePage="chat"/>
                    <div className="ml-auto flex items-center space-x-4">
                        <Search/>
                        <UserNavBar username={username} name={name} role={role}/>
                    </div>
                </div>
            </div>
        </div>
        <div className="bg-cover h-screen bg-gradient-to-tr from-lime-300 to-sky-400 p-4">
            <Card className="bg-white backdrop-filter backdrop-blur-lg opacity-90">
                <div className="flex flex-row justify-between items-center">
                <div className="flex flex-col p-4">
                    <h3 className="text-2xl font-bold text-left">QualiChat</h3>
                    <p className="text-sm text-left">Chat with our models for patients monitoring or Q&A</p>
                </div>
                    <div className="flex flex-col p-4">
                    <Select onValueChange={setModel} defaultValue="0">
                        <SelectTrigger className="w-[130px]">
                            <SelectValue placeholder="Monitoring" />
                        </SelectTrigger>
                        <SelectContent>
                            <SelectItem value="0">Monitoring</SelectItem>
                            <SelectItem value="1">Q&A</SelectItem>
                        </SelectContent>
                    </Select>
                    </div>
                </div>
                <div className="flex flex-col p-4">
                    <Separator/>
                    <ChatLayout/>
                </div>
            </Card>
        </div>
    </div>)
}