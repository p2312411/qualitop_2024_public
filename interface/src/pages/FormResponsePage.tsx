import {MainNavBar} from "../components/MainNavBar.tsx";
import {UserNavBar} from "../components/UserNavBar.tsx";
import {Search} from "../components/Search.tsx";
import Background from "../assets/qualitop-logo.png";
import {FormResponse} from "../components/FormResponse.tsx";


export const FormResponsePage = () => {

    const username = sessionStorage.getItem('username');
    const name = sessionStorage.getItem('name');
    const role = sessionStorage.getItem('role');

    return (
        <div>
            <div className="hidden flex-col md:flex sticky bg-white top-0 w-full z-10 backdrop-filter backdrop-blur-lg bg-opacity-30">
                <div>
                    <div className="flex h-16 flex-row justify-between items-center px-4">
                        <img src={Background} className="h-6 mr-6" alt="qualitop-logo"/>
                        <MainNavBar activePage="form"/>
                        <div className="ml-auto flex items-center space-x-4">
                            <Search/>
                            <UserNavBar name={name} username={username} role={role}/>
                        </div>
                    </div>
                </div>
            </div>
            <FormResponse />
        </div>
    )
}