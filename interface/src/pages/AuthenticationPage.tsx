import {UserLoginForm} from "../components/UserLoginForm.tsx";
import QualitopLogo from "../assets/qualitop-antibody-300x300.png";
import LirisLogo from "../assets/liris.png";
import Lyon1Logo from "../assets/lyon1.png";
import Lyon2Logo from "../assets/lyon2.png";
import CNRSLogo from "../assets/cnrs.png";
import {Toaster} from "../../shadcn/components/ui/toaster.tsx";

export default function AuthenticationPage() {
    return (<div className="flex h-screen items-center">
            <div className="hidden flex-col w-full h-full bg-gradient-to-tr from-lime-300 to-sky-400 lg:flex p-2">
                <div className="flex flex-row items-center justify-left">
                    <img src={QualitopLogo} alt="qualitop logo" className="w-20 m-2"/>
                    <h3 className="text-2xl font-semibold tracking-tight text-black"> Qualitop Platform</h3>
                </div>
                <div className="flex flex-col items-center justify-end flex-grow">
                    <div
                        className="hidden sm:flex flex-row items-center justify-between space-x-4 sm:space-x-20 p-2 w-full">
                        <img src={LirisLogo} alt="qualitop logo" className="h-20 m-2 flex-grow object-contain"/>
                        <img src={CNRSLogo} alt="cnrs logo" className="h-20 m-2 flex-grow object-contain"/>
                        <img src={Lyon1Logo} alt="lyon1 logo" className="h-20 m-2 flex-grow object-contain"/>
                        <img src={Lyon2Logo} alt="lyon2 logo" className="h-20 m-2 flex-grow object-contain"/>
                    </div>
                </div>
            </div>
            <div className="p-8">
                <div className="m-auto flex w-full flex-col justify-center space-y-6">
                    <div className="flex flex-col text-center">
                        <h1 className="text-2xl font-semibold tracking-tight">
                            Log in to your account
                        </h1>
                    </div>
                    <UserLoginForm/>
                    <p className="mx-auto w-full px-8 text-center text-sm text-muted-foreground">
                        Are you a new user ? Get access to the platform by {" "}
                        <a href="/registration" className="underline underline-offset-4 hover:text-primary">
                            registering
                        </a>
                        {" "}
                        .
                    </p>
                </div>
            </div>
            <Toaster/>
        </div>
    )
}
