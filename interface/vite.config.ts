import path from "path"
import react from "@vitejs/plugin-react"
import {defineConfig} from "vite"

export default defineConfig({
    plugins: [react()],
    resolve: {
        alias: {
            "@": path.resolve(__dirname, "./src"),
        },
    },
    server: {
        port: 80,
        proxy: {
            '/login': {
                target: 'http://qualitop-2024-switcher-1:8000',
                changeOrigin: true,
                secure: false,
            },
            '/register': {
                target: 'http://qualitop-2024-switcher-1:8000',
                changeOrigin: true,
                secure: false,
            },
            '/predict': {
                target: 'http://qualitop-2024-switcher-1:8000',
                changeOrigin: true,
                secure: false,
            },
            '/api/chat/answer': {
                target: 'http://qualitop-2024-switcher-1:8000',
                changeOrigin: true,
                secure: false,
            },
            '/api/chat/parse': {
                target: 'http://qualitop-2024-switcher-1:8000',
                changeOrigin: true,
                secure: false,
            },
            '/api/chat/parse_v2': {
                target: 'http://qualitop-2024-switcher-1:8000',
                changeOrigin: true,
                secure: false,
            },
            '/predict_FACT_G_IUS': {
                target: 'http://qualitop-2024-switcher-1:8000',
                changeOrigin: true,
                secure: false,
            },
            '/predict_FACT_G_DPR': {
                target: 'http://qualitop-2024-switcher-1:8000',
                changeOrigin: true,
                secure: false,
            },
            '/predict_FACT_G_TRH': {
                target: 'http://qualitop-2024-switcher-1:8000',
                changeOrigin: true,
                secure: false,
            },
            '/predict_FACT_G_HLS': {
                target: 'http://qualitop-2024-switcher-1:8000',
                changeOrigin: true,
                secure: false,
            },
            '/predict_FACT_G_TSS': {
                target: 'http://qualitop-2024-switcher-1:8000',
                changeOrigin: true,
                secure: false,
            },
            '/predict_saillant': {
                target: 'http://qualitop-2024-switcher-1:8000',
                changeOrigin: true,
                secure: false,
            },
            '/subtypes': {
                target: 'http://qualitop-2024-switcher-1:8000',
                changeOrigin: true,
                secure: false,
            },
            '/relationship_with_drug': {
                target: 'http://qualitop-2024-switcher-1:8000',
                changeOrigin: true,
                secure: false,
            },
            '/followup': {
                target: 'http://qualitop-2024-switcher-1:8000',
                changeOrigin: true,
                secure: false,
            },
            '/content_qual': {
                target: 'http://qualitop-2024-switcher-1:8000',
                changeOrigin: true,
                secure: false,
            }
        },
    },
})
