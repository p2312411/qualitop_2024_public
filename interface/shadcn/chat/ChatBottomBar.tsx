import {
    Mic, SendHorizontal, Square
} from "lucide-react";
import React, {useRef, useState} from "react";
import {buttonVariants} from "../components/ui/button.tsx";
import {cn} from "../lib/utils";
import {AnimatePresence, motion} from "framer-motion";
import {loggedInUserData, Message} from "./ChatLayout";
import {Textarea} from "../components/ui/textarea.tsx";
import {Dialog, DialogContent, DialogDescription, DialogHeader, DialogTitle} from "../components/ui/dialog.tsx";
import {Progress} from "../components/ui/progress.tsx";

interface ChatBottombarProps {
    messages: Message[];
    sendMessage: (newMessage: Message) => void;
    isMobile: boolean;
}

interface Item {
    attribute: string;
    value: string;
}

function createObject(array: Item[]): { [key: string]: string } {
    return array.reduce((acc, item) => {
        acc[item.attribute] = item.value;
        return acc;
    }, {});
}

export const BottombarIcons = [{icon: Mic}];

export default function ChatBottomBar({
                                          messages,
                                          sendMessage, isMobile,
                                      }: ChatBottombarProps) {
    const [message, setMessage] = useState("");
    const inputRef = useRef<HTMLTextAreaElement>(null);
    const modelType = sessionStorage.getItem("model") ?? "0";
    const mediaRecorder = useRef(new MediaRecorder(new MediaStream()));
    const [audio, setAudio] = useState("");
    const [isRecording, setIsRecording] = useState(false);
    const [isDialogOpen, setIsDialogOpen] = React.useState<boolean>(false);

    const [comment, setComment] = React.useState<string>("");
    const [color, setColor] = React.useState<string>("");
    const [value, setValue] = React.useState<number>(100);
    const [field, setField] = React.useState<string>("");

    const startRecording = async () => {
        const stream = await navigator.mediaDevices.getUserMedia({audio: true});
        const audioChunks: any[] | undefined = [];
        mediaRecorder.current = new MediaRecorder(stream);
        mediaRecorder.current.start();
        mediaRecorder.current.addEventListener("dataavailable", event => {
            audioChunks.push(event.data);
        });

        mediaRecorder.current.addEventListener("stop", async () => {
            const audioBlob = new Blob(audioChunks);
            const audioUrl = URL.createObjectURL(audioBlob);
            setAudio(audioUrl);

            const formData = new FormData();
            formData.append('file', audioBlob, 'recording.wav');

            const transcription = await fetch('/api/chat/transcribe', {
                method: 'POST',
                body: formData
            }).then((response) => {
                return response.json();
            });

            setMessage(transcription)
            console.log(transcription);
        });
    }

    const stopRecording = () => {
        mediaRecorder.current.stop();
    };

    const handleInputChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
        // @ts-ignore
        setMessage(event.target.value);
    };

    let new_chat = 1;

    async function modelResponse(modelType: string, username: string, message: string) {

        const placeholderMessage: Message = {
            id: messages.length,
            name: "monitoring_model",
            message: "processing your request...",
        };
        sendMessage(placeholderMessage);

        if (modelType === "0") {
            const body = JSON.stringify({
                username: username,
                query: message,
                new_chat: new_chat
            });

            new_chat = 0;

            try {
                const responseMain = await fetch("/api/chat/parse_v2", {
                    method: "POST",
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Content-Type": "application/json",
                        //"Authorization": 'Basic dGVzdDpISlM/eSMhRjkydjteUjpVNDZ7ZTdd'
                    },
                    body: body,
                }).then((response) => {
                    return response.json();
                });

                const response = JSON.parse(responseMain.response)

                if (response) {

                    switch (response.model) {

                        case "/predict_saillant" :
                            const response_ = await fetch('/predict_saillant', {
                                method: 'POST',
                                headers: {
                                    "Access-Control-Allow-Origin": "*",
                                    'Content-Type': 'application/json',
                                },
                                body: JSON.stringify(createObject(response.attributes))
                            }).then((response) => {
                                return response.json();
                            });

                            let prompt_: string = "Based on this # 'relationship_with_drug_nbM' : (0 : the AE is not related to medications, 1 : the AE is related to medications)We calculated this variable as : the count of 'Yes' and 'Possibly' / all values ('Yes', 'No', 'Possibly') # 'followupM' : (1 - 5) As an average, did the immunotherapies give response or did the disease progressed ? 1, Stable disease | 2, Progressive disease | 3, Partial response | 4, Complete response | 5, Not evaluatedCalculus : the followup values / the number of followups # 'i_am_content_with_the_qualT' : (1 - 5) Degree of contentment of QoL1, Not at all | 2, A little bit | 3, Somewhat | 4, Quite a bit | 5, Very much Calculus : mean of contentment among all questionnaire times, return a summary of the predicted results to the user :"

                            const body_ = JSON.stringify({
                                username: username,
                                query: prompt_ + JSON.stringify(response_),
                                new_chat: new_chat
                            });

                            const response__ = await fetch("/api/chat/answer", {
                                method: "POST",
                                headers: {
                                    "Access-Control-Allow-Origin": "*",
                                    "Content-Type": "application/json",
                                    //"Authorization": 'Basic dGVzdDpISlM/eSMhRjkydjteUjpVNDZ7ZTdd'
                                },
                                body: body_,
                            }).then((response) => {
                                return response.json();
                            });

                            console.log(response__)

                            const newMessage_: Message = {
                                id: messages.length,
                                name: "monitoring_model",
                                message: response__.response,
                            };
                            sendMessage(newMessage_);
                            console.log(newMessage_);

                            break;

                        case "/predict_FACT_G_IUS":

                            const response2_ = await fetch('/predict_FACT_G_IUS', {
                                method: 'POST',
                                headers: {
                                    "Access-Control-Allow-Origin": "*",
                                    'Content-Type': 'application/json',
                                },
                                body: JSON.stringify(createObject(response.attributes))
                            }).then((response) => {
                                return response.json();
                            });

                            console.log(response2_)

                            let prompt2: string = "Based on this When a patient has a total FACT-G score of 27 or lower, he has a poor QoL\n" +
                                "When a patient has a score from 28 to 53, he has a moderately poor QoL\n" +
                                "When a patient has a score from 54 to 80, he has a moderately high QoL\n" +
                                "When a patient has a score of 81 or higher, he has a high QoL , return a summary of these results to the user :"

                            const body2 = JSON.stringify({
                                username: username,
                                query: prompt2 + JSON.stringify(response2_),
                                new_chat: new_chat
                            });

                            const response2__ = await fetch("/api/chat/answer", {
                                method: "POST",
                                headers: {
                                    "Access-Control-Allow-Origin": "*",
                                    "Content-Type": "application/json",
                                    //"Authorization": 'Basic dGVzdDpISlM/eSMhRjkydjteUjpVNDZ7ZTdd'
                                },
                                body: body2,
                            }).then((response) => {
                                return response.json();
                            });

                            console.log(response2__)

                            const newMessage2_: Message = {
                                id: messages.length,
                                name: "monitoring_model",
                                message: response2__.response,
                            };
                            sendMessage(newMessage2_);
                            console.log(newMessage2_);

                            setTimeout(() => {
                                setIsDialogOpen(true)
                                setField(Object.keys(response2_)[0])
                                let result = parseFloat(response2_[Object.keys(response2_)[0]]);
                                setValue(parseInt(String((result / 108) * 100)))
                                if (result <= 27) {
                                    setComment(" Very poor quality of life (" + result + ")");
                                    setColor("bg-red-300");
                                } else if (result >= 28 && result <= 54) {
                                    setComment(" Poor quality of life (" + result + ")");
                                    setColor("bg-orange-300");
                                } else if (result >= 55 && result <= 81) {
                                    setComment(" Moderate quality of life (" + result + ")");
                                    setColor("bg-yellow-300");
                                } else if (result >= 82 && result <= 108) {
                                    setComment(" Good quality of life (" + result + ")");
                                    setColor("bg-green-300");
                                } else {
                                    setComment("Invalid prediction");
                                }
                            }, 3000)

                            break;

                        case "/predict_FACT_G_DPR":

                            const response3_ = await fetch('/predict_FACT_G_DPR', {
                                method: 'POST',
                                headers: {
                                    "Access-Control-Allow-Origin": "*",
                                    'Content-Type': 'application/json',
                                },
                                body: JSON.stringify(createObject(response.attributes))
                            }).then((response) => {
                                return response.json();
                            });

                            console.log(response3_)

                            let prompt3: string = "Based on this When a patient has a total FACT-G score of 27 or lower, he has a poor QoL\n" +
                                "When a patient has a score from 28 to 53, he has a moderately poor QoL\n" +
                                "When a patient has a score from 54 to 80, he has a moderately high QoL\n" +
                                "When a patient has a score of 81 or higher, he has a high QoL , return a summary of these results to the user :"

                            const body3 = JSON.stringify({
                                username: username,
                                query: prompt3 + JSON.stringify(response3_),
                                new_chat: new_chat
                            });

                            const response3__ = await fetch("/api/chat/answer", {
                                method: "POST",
                                headers: {
                                    "Access-Control-Allow-Origin": "*",
                                    "Content-Type": "application/json",
                                    //"Authorization": 'Basic dGVzdDpISlM/eSMhRjkydjteUjpVNDZ7ZTdd'
                                },
                                body: body3,
                            }).then((response) => {
                                return response.json();
                            });

                            console.log(response3__)

                            const newMessage3_: Message = {
                                id: messages.length,
                                name: "monitoring_model",
                                message: response3__.response,
                            };
                            sendMessage(newMessage3_);
                            console.log(newMessage3_);

                            setTimeout(() => {
                                setIsDialogOpen(true)
                                setField(Object.keys(response3_)[0])
                                let result = parseFloat(response3_[Object.keys(response3_)[0]]);
                                setValue(parseInt(String((result / 108) * 100)))
                                if (result <= 27) {
                                    setComment(" Very poor quality of life (" + result + ")");
                                    setColor("bg-red-300");
                                } else if (result >= 28 && result <= 54) {
                                    setComment(" Poor quality of life (" + result + ")");
                                    setColor("bg-orange-300");
                                } else if (result >= 55 && result <= 81) {
                                    setComment(" Moderate quality of life (" + result + ")");
                                    setColor("bg-yellow-300");
                                } else if (result >= 82 && result <= 108) {
                                    setComment(" Good quality of life (" + result + ")");
                                    setColor("bg-green-300");
                                } else {
                                    setComment("Invalid prediction");
                                }
                            }, 3000)

                            break;

                        case "/predict_FACT_G_TRH":

                            const response4_ = await fetch('/predict_FACT_G_TRH', {
                                method: 'POST',
                                headers: {
                                    "Access-Control-Allow-Origin": "*",
                                    'Content-Type': 'application/json',
                                },
                                body: JSON.stringify(createObject(response.attributes))
                            }).then((response) => {
                                return response.json();
                            });

                            console.log(response4_)

                            let prompt4: string = "Based on this When a patient has a total FACT-G score of 27 or lower, he has a poor QoL\n" +
                                "When a patient has a score from 28 to 53, he has a moderately poor QoL\n" +
                                "When a patient has a score from 54 to 80, he has a moderately high QoL\n" +
                                "When a patient has a score of 81 or higher, he has a high QoL , return a summary of these results to the user :"

                            const body4 = JSON.stringify({
                                username: username,
                                query: prompt4 + JSON.stringify(response4_),
                                new_chat: new_chat
                            });

                            const response4__ = await fetch("/api/chat/answer", {
                                method: "POST",
                                headers: {
                                    "Access-Control-Allow-Origin": "*",
                                    "Content-Type": "application/json",
                                    //"Authorization": 'Basic dGVzdDpISlM/eSMhRjkydjteUjpVNDZ7ZTdd'
                                },
                                body: body4,
                            }).then((response) => {
                                return response.json();
                            });

                            console.log(response4__)

                            const newMessage4_: Message = {
                                id: messages.length,
                                name: "monitoring_model",
                                message: response4__.response,
                            };
                            sendMessage(newMessage4_);
                            console.log(newMessage4_);

                            setTimeout(() => {
                                setIsDialogOpen(true)
                                setField(Object.keys(response4_)[0])
                                let result = parseFloat(response4_[Object.keys(response4_)[0]]);
                                setValue(parseInt(String((result / 108) * 100)))
                                if (result <= 27) {
                                    setComment(" Very poor quality of life (" + result + ")");
                                    setColor("bg-red-300");
                                } else if (result >= 28 && result <= 54) {
                                    setComment(" Poor quality of life (" + result + ")");
                                    setColor("bg-orange-300");
                                } else if (result >= 55 && result <= 81) {
                                    setComment(" Moderate quality of life (" + result + ")");
                                    setColor("bg-yellow-300");
                                } else if (result >= 82 && result <= 108) {
                                    setComment(" Good quality of life (" + result + ")");
                                    setColor("bg-green-300");
                                } else {
                                    setComment("Invalid prediction");
                                }
                            }, 3000)

                            break;
                        case "/predict_FACT_G_HLS":

                            const response5_ = await fetch('/predict_FACT_G_HLS', {
                                method: 'POST',
                                headers: {
                                    "Access-Control-Allow-Origin": "*",
                                    'Content-Type': 'application/json',
                                },
                                body: JSON.stringify(createObject(response.attributes))
                            }).then((response) => {
                                return response.json();
                            });

                            console.log(response5_)

                            let prompt5: string = "Based on this When a patient has a total FACT-G score of 27 or lower, he has a poor QoL\n" +
                                "When a patient has a score from 28 to 53, he has a moderately poor QoL\n" +
                                "When a patient has a score from 54 to 80, he has a moderately high QoL\n" +
                                "When a patient has a score of 81 or higher, he has a high QoL , return a summary of these results to the user :"

                            const body5 = JSON.stringify({
                                username: username,
                                query: prompt5 + JSON.stringify(response5_),
                                new_chat: new_chat
                            });

                            const response5__ = await fetch("/api/chat/answer", {
                                method: "POST",
                                headers: {
                                    "Access-Control-Allow-Origin": "*",
                                    "Content-Type": "application/json",
                                    //"Authorization": 'Basic dGVzdDpISlM/eSMhRjkydjteUjpVNDZ7ZTdd'
                                },
                                body: body5,
                            }).then((response) => {
                                return response.json();
                            });

                            console.log(response5__)

                            const newMessage5_: Message = {
                                id: messages.length,
                                name: "monitoring_model",
                                message: response5__.response,
                            };
                            sendMessage(newMessage5_);
                            console.log(newMessage5_);

                            setTimeout(() => {
                                setIsDialogOpen(true)
                                setField(Object.keys(response5_)[0])
                                let result = parseFloat(response5_[Object.keys(response5_)[0]]);
                                setValue(parseInt(String((result / 108) * 100)))
                                if (result <= 27) {
                                    setComment(" Very poor quality of life (" + result + ")");
                                    setColor("bg-red-300");
                                } else if (result >= 28 && result <= 54) {
                                    setComment(" Poor quality of life (" + result + ")");
                                    setColor("bg-orange-300");
                                } else if (result >= 55 && result <= 81) {
                                    setComment(" Moderate quality of life (" + result + ")");
                                    setColor("bg-yellow-300");
                                } else if (result >= 82 && result <= 108) {
                                    setComment(" Good quality of life (" + result + ")");
                                    setColor("bg-green-300");
                                } else {
                                    setComment("Invalid prediction");
                                }
                            }, 3000)
                            break;

                        case "/predict_FACT_G_TSS":

                            const response6_ = await fetch('/predict_FACT_G_TSS', {
                                method: 'POST',
                                headers: {
                                    "Access-Control-Allow-Origin": "*",
                                    'Content-Type': 'application/json',
                                },
                                body: JSON.stringify(createObject(response.attributes))
                            }).then((response) => {
                                return response.json();
                            });

                            console.log(response6_)

                            let prompt6: string = "Based on this When a patient has a total FACT-G score of 27 or lower, he has a poor QoL\n" +
                                "When a patient has a score from 28 to 53, he has a moderately poor QoL\n" +
                                "When a patient has a score from 54 to 80, he has a moderately high QoL\n" +
                                "When a patient has a score of 81 or higher, he has a high QoL , return a summary of these results to the user :"

                            const body6 = JSON.stringify({
                                username: username,
                                query: prompt6 + JSON.stringify(response6_),
                                new_chat: new_chat
                            });

                            const response6__ = await fetch("/api/chat/answer", {
                                method: "POST",
                                headers: {
                                    "Access-Control-Allow-Origin": "*",
                                    "Content-Type": "application/json",
                                    //"Authorization": 'Basic dGVzdDpISlM/eSMhRjkydjteUjpVNDZ7ZTdd'
                                },
                                body: body6,
                            }).then((response) => {
                                return response.json();
                            });

                            console.log(response6__)

                            const newMessage6_: Message = {
                                id: messages.length,
                                name: "monitoring_model",
                                message: response6__.response,
                            };
                            sendMessage(newMessage6_);
                            console.log(newMessage6_);

                            setTimeout(() => {
                                setIsDialogOpen(true)
                                setField(Object.keys(response6_)[0])
                                let result = parseFloat(response6_[Object.keys(response6_)[0]]);
                                setValue(parseInt(String((result / 108) * 100)))
                                if (result <= 27) {
                                    setComment(" Very poor quality of life (" + result + ")");
                                    setColor("bg-red-300");
                                } else if (result >= 28 && result <= 54) {
                                    setComment(" Poor quality of life (" + result + ")");
                                    setColor("bg-orange-300");
                                } else if (result >= 55 && result <= 81) {
                                    setComment(" Moderate quality of life (" + result + ")");
                                    setColor("bg-yellow-300");
                                } else if (result >= 82 && result <= 108) {
                                    setComment(" Good quality of life (" + result + ")");
                                    setColor("bg-green-300");
                                } else {
                                    setComment("Invalid prediction");
                                }
                            }, 3000)

                            break;

                        case "/relationship_with_drug_nbM":

                            const response8_ = await fetch('/relationship_with_drug_nbM', {
                                method: 'POST',
                                headers: {
                                    "Access-Control-Allow-Origin": "*",
                                    'Content-Type': 'application/json',
                                },
                                body: JSON.stringify(createObject(response.attributes))
                            }).then((response) => {
                                return response.json();
                            });

                            console.log(response8_)

                            let prompt8: string = "Based on this # 'relationship_with_drug_nbM' : (0 : the AE is not related to medications, 1 : the AE is related to medications), return a summary of these results to the user :"

                            const body8 = JSON.stringify({
                                username: username,
                                query: prompt8 + JSON.stringify(response8_),
                                new_chat: new_chat
                            });

                            const response8__ = await fetch("/api/chat/answer", {
                                method: "POST",
                                headers: {
                                    "Access-Control-Allow-Origin": "*",
                                    "Content-Type": "application/json",
                                    //"Authorization": 'Basic dGVzdDpISlM/eSMhRjkydjteUjpVNDZ7ZTdd'
                                },
                                body: body8,
                            }).then((response) => {
                                return response.json();
                            });

                            console.log(response8__)

                            const newMessage8_: Message = {
                                id: messages.length,
                                name: "monitoring_model",
                                message: response8__.response,
                            };
                            sendMessage(newMessage8_);
                            console.log(newMessage8_);

                            /*setTimeout(() => {
                                setIsDialogOpen(true)
                                setField(Object.keys(response6_)[0])
                                let result = parseFloat(response6_[Object.keys(response6_)[0]]);
                                setValue(parseInt(String((result / 108) * 100)))
                                if (result <= 27) {
                                    setComment(" Very poor quality of life (" + result + ")");
                                    setColor("bg-red-300");
                                } else if (result >= 28 && result <= 54) {
                                    setComment(" Poor quality of life (" + result + ")");
                                    setColor("bg-orange-300");
                                } else if (result >= 55 && result <= 81) {
                                    setComment(" Moderate quality of life (" + result + ")");
                                    setColor("bg-yellow-300");
                                } else if (result >= 82 && result <= 108) {
                                    setComment(" Good quality of life (" + result + ")");
                                    setColor("bg-green-300");
                                } else {
                                    setComment("Invalid prediction");
                                }
                            }, 3000)*/

                            break;

                        case "/followupM":

                            const response9_ = await fetch('/subtypes', {
                                method: 'POST',
                                headers: {
                                    "Access-Control-Allow-Origin": "*",
                                    'Content-Type': 'application/json',
                                },
                                body: JSON.stringify(createObject(response.attributes))
                            }).then((response) => {
                                return response.json();
                            });

                            console.log(response9_)

                            let prompt9: string = "Based on this # followupM' : (1 - 5) As an average, did the immunotherapies give response or did the disease progressed ?\n" +
                                "\n" +
                                "1, Stable disease | 2, Progressive disease | 3, Partial response | 4, Complete response | 5, Not evaluated, return a summary of these results to the user :"

                            const body9 = JSON.stringify({
                                username: username,
                                query: prompt9 + JSON.stringify(response9_),
                                new_chat: new_chat
                            });

                            const response9__ = await fetch("/api/chat/answer", {
                                method: "POST",
                                headers: {
                                    "Access-Control-Allow-Origin": "*",
                                    "Content-Type": "application/json",
                                    //"Authorization": 'Basic dGVzdDpISlM/eSMhRjkydjteUjpVNDZ7ZTdd'
                                },
                                body: body9,
                            }).then((response) => {
                                return response.json();
                            });

                            console.log(response9__)

                            const newMessage9_: Message = {
                                id: messages.length,
                                name: "monitoring_model",
                                message: response9__.response,
                            };
                            sendMessage(newMessage9_);
                            console.log(newMessage9_);

                            /*setTimeout(() => {
                                setIsDialogOpen(true)
                                setField(Object.keys(response6_)[0])
                                let result = parseFloat(response6_[Object.keys(response6_)[0]]);
                                setValue(parseInt(String((result / 108) * 100)))
                                if (result <= 27) {
                                    setComment(" Very poor quality of life (" + result + ")");
                                    setColor("bg-red-300");
                                } else if (result >= 28 && result <= 54) {
                                    setComment(" Poor quality of life (" + result + ")");
                                    setColor("bg-orange-300");
                                } else if (result >= 55 && result <= 81) {
                                    setComment(" Moderate quality of life (" + result + ")");
                                    setColor("bg-yellow-300");
                                } else if (result >= 82 && result <= 108) {
                                    setComment(" Good quality of life (" + result + ")");
                                    setColor("bg-green-300");
                                } else {
                                    setComment("Invalid prediction");
                                }
                            }, 3000)*/

                            break;

                        case "/i_am_content_with_the_qualT":

                            const response10_ = await fetch('/subtypes', {
                                method: 'POST',
                                headers: {
                                    "Access-Control-Allow-Origin": "*",
                                    'Content-Type': 'application/json',
                                },
                                body: JSON.stringify(createObject(response.attributes))
                            }).then((response) => {
                                return response.json();
                            });

                            console.log(response10_)

                            let prompt10: string = "Based on this # 'i_am_content_with_the_qualT' : (1 - 5) Degree of contentment of QoL\n" +
                                "\n" +
                                "1, Not at all | 2, A little bit | 3, Somewhat | 4, Quite a bit | 5, Very much, return a summary of these results to the user :"

                            const body10 = JSON.stringify({
                                username: username,
                                query: prompt10 + JSON.stringify(response10_),
                                new_chat: new_chat
                            });

                            const response10__ = await fetch("/api/chat/answer", {
                                method: "POST",
                                headers: {
                                    "Access-Control-Allow-Origin": "*",
                                    "Content-Type": "application/json",
                                    //"Authorization": 'Basic dGVzdDpISlM/eSMhRjkydjteUjpVNDZ7ZTdd'
                                },
                                body: body10,
                            }).then((response) => {
                                return response.json();
                            });

                            console.log(response10__)

                            const newMessage10_: Message = {
                                id: messages.length,
                                name: "monitoring_model",
                                message: response10__.response,
                            };
                            sendMessage(newMessage10_);
                            console.log(newMessage10_);

                            /*setTimeout(() => {
                                setIsDialogOpen(true)
                                setField(Object.keys(response6_)[0])
                                let result = parseFloat(response6_[Object.keys(response6_)[0]]);
                                setValue(parseInt(String((result / 108) * 100)))
                                if (result <= 27) {
                                    setComment(" Very poor quality of life (" + result + ")");
                                    setColor("bg-red-300");
                                } else if (result >= 28 && result <= 54) {
                                    setComment(" Poor quality of life (" + result + ")");
                                    setColor("bg-orange-300");
                                } else if (result >= 55 && result <= 81) {
                                    setComment(" Moderate quality of life (" + result + ")");
                                    setColor("bg-yellow-300");
                                } else if (result >= 82 && result <= 108) {
                                    setComment(" Good quality of life (" + result + ")");
                                    setColor("bg-green-300");
                                } else {
                                    setComment("Invalid prediction");
                                }
                            }, 3000)*/

                            break;

                        case "/subtypes":

                            const response7_ = await fetch('/subtypes', {
                                method: 'POST',
                                headers: {
                                    "Access-Control-Allow-Origin": "*",
                                    'Content-Type': 'application/json',
                                },
                                body: JSON.stringify(createObject(response.attributes))
                            }).then((response) => {
                                return response.json();
                            });

                            console.log(response7_)

                            let prompt7: string = "Based on the list of these questionnaire answers predicted to describe the patient,return a summary of these results to the user :"

                            const body7 = JSON.stringify({
                                username: username,
                                query: prompt7 + JSON.stringify(response7_),
                                new_chat: new_chat
                            });

                            const response7__ = await fetch("/api/chat/answer", {
                                method: "POST",
                                headers: {
                                    "Access-Control-Allow-Origin": "*",
                                    "Content-Type": "application/json",
                                    //"Authorization": 'Basic dGVzdDpISlM/eSMhRjkydjteUjpVNDZ7ZTdd'
                                },
                                body: body7,
                            }).then((response) => {
                                return response.json();
                            });

                            console.log(response7__)

                            const newMessage7_: Message = {
                                id: messages.length,
                                name: "monitoring_model",
                                message: response7__.response,
                            };
                            sendMessage(newMessage7_);
                            console.log(newMessage7_);

                            /*setTimeout(() => {
                                setIsDialogOpen(true)
                                setField(Object.keys(response6_)[0])
                                let result = parseFloat(response6_[Object.keys(response6_)[0]]);
                                setValue(parseInt(String((result / 108) * 100)))
                                if (result <= 27) {
                                    setComment(" Very poor quality of life (" + result + ")");
                                    setColor("bg-red-300");
                                } else if (result >= 28 && result <= 54) {
                                    setComment(" Poor quality of life (" + result + ")");
                                    setColor("bg-orange-300");
                                } else if (result >= 55 && result <= 81) {
                                    setComment(" Moderate quality of life (" + result + ")");
                                    setColor("bg-yellow-300");
                                } else if (result >= 82 && result <= 108) {
                                    setComment(" Good quality of life (" + result + ")");
                                    setColor("bg-green-300");
                                } else {
                                    setComment("Invalid prediction");
                                }
                            }, 3000)*/

                            break;

                        default:
                            const newMessage: Message = {
                                id: messages.length,
                                name: "monitoring_model",
                                message: "Request not supported",
                            };
                            sendMessage(newMessage);
                            console.log(newMessage);
                            break;
                    }

                } else {
                    const newMessage: Message = {
                        id: messages.length,
                        name: "monitoring_model",
                        message: "Invalid request",
                    };
                    sendMessage(newMessage);
                    console.log(newMessage);
                }

            } catch (error) {
                console.log(error);
                const newMessage: Message = {
                    id: messages.length,
                    name: "monitoring_model",
                    message: "Error processing your request ! Please try again.",
                };
                sendMessage(newMessage);

            }

        } else {

            placeholderMessage.name = "q&a_model"
            const body = JSON.stringify({
                username: username,
                query: message,
                new_chat: new_chat
            });

            new_chat = 0;

            try {
                const response = await fetch("/api/chat/answer", {
                    method: "POST",
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Content-Type": "application/json",
                        //"Authorization": 'Basic dGVzdDpISlM/eSMhRjkydjteUjpVNDZ7ZTdd'
                    },
                    body: body,
                }).then((response) => {
                    return response.json();
                });

                console.log(response)

                const newMessage: Message = {
                    id: messages.length,
                    name: "q&a_model",
                    message: response.response,
                };
                sendMessage(newMessage);
                console.log(newMessage);

            } catch (error) {
                console.log(error);
                const newMessage: Message = {
                    id: messages.length,
                    name: "q&a_model",
                    message: "Error processing your request ! Please try again.",
                };
                sendMessage(newMessage);

            }
        }
    }

    const handleSend = () => {
        if (message.trim()) {
            const newMessage: Message = {
                id: messages.length,
                name: loggedInUserData.username ?? "",
                message: message.trim(),
            };
            sendMessage(newMessage);
            console.log(newMessage);
            setMessage("");

            modelResponse(modelType, loggedInUserData.username ?? "", message.trim());

            if (inputRef.current) {
                inputRef.current.focus();
            }
        }
    };

    const handleKeyPress = (event: React.KeyboardEvent<HTMLTextAreaElement>) => {
        if (event.key === "Enter" && !event.shiftKey) {
            event.preventDefault();
            handleSend();
        }

        if (event.key === "Enter" && event.shiftKey) {
            event.preventDefault();
            setMessage((prev) => prev + "\n");
        }
    };

    function handleMicInput() {
        console.log(isRecording)
        if (isRecording) {
            stopRecording();
        } else {
            startRecording();
        }
        setIsRecording(prevState => !prevState);
    }

    return (<div className="flex flex-col">
        <Dialog open={isDialogOpen} onOpenChange={setIsDialogOpen}>
            <DialogContent>
                <DialogHeader>
                    <DialogDescription>
                        <div className="flex flex-row mt-2">
                            <div className="flex flex-row space-x-6 items-center justify-between">
                                <>
                                    <Progress
                                        value={value}
                                        className="w-[350px]"
                                        indicatorColor={color}
                                    />
                                    <h3 className="text-left italic">
                                        {(comment.charAt(0).toUpperCase() + comment.slice(1)).replace(/_/g, ' ')}
                                    </h3>
                                </>
                            </div>
                        </div>
                    </DialogDescription>
                </DialogHeader>
            </DialogContent>
        </Dialog>
        <div className="flex justify-between items-center gap-2 mt-2">
            <div className="flex">
                {!message.trim() && !isMobile && (<div className="flex">
                    {BottombarIcons.map((icon, index) => (<a
                        key={index}
                        className={cn(buttonVariants({
                            variant: "ghost", size: "icon"
                        }), "h-9 w-9", "dark:bg-muted dark:text-muted-foreground dark:hover:bg-muted dark:hover:text-white")}
                        onClick={handleMicInput}
                    >
                        {isRecording ? (
                            <Square size={20} className="text-muted-foreground"/>
                        ) : (
                            <icon.icon size={20} className="text-muted-foreground"/>
                        )}
                    </a>))}
                </div>)}
            </div>
            <AnimatePresence initial={false}>
                <motion.div
                    key="input"
                    className="w-full relative"
                    layout
                    initial={{opacity: 0, scale: 1}}
                    animate={{opacity: 1, scale: 1}}
                    exit={{opacity: 0, scale: 1}}
                    transition={{
                        opacity: {duration: 0.05}, layout: {
                            type: "spring", bounce: 0.15,
                        },
                    }}
                >
                    <Textarea
                        autoComplete="off"
                        value={message}
                        ref={inputRef}
                        onKeyDown={handleKeyPress}
                        onChange={handleInputChange}
                        name="message"
                        placeholder="Type your message here..."
                        className="border flex rounded-full min-h-[9px] resize-none text-xl"
                    ></Textarea>
                </motion.div>
                <a
                    className={cn(buttonVariants({
                        variant: "ghost", size: "icon"
                    }), "h-9 w-9", "dark:bg-muted dark:text-muted-foreground dark:hover:bg-muted dark:hover:text-white shrink-0")}
                    onClick={handleSend}
                >
                    <SendHorizontal size={20} className="text-muted-foreground"/>
                </a>
            </AnimatePresence>
        </div>
    </div>);
}
