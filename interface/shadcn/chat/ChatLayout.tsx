import {useEffect, useState} from "react";
import {Chat} from "./Chat.tsx";

export const conversations = [
    {
        id: 0,
        messages: [
            {
                id: 0,
                name: 'monitoring_model',
                message: "Hello, I'm the monitoring model. You can ask me anything related to patient monitoring and I will try to help you.",
            }
        ],
        name: 'monitoring_model',
    },
    {
        id: 1,
        messages: [
            {
                id: 0,
                name: 'q&a_model',
                message: "Hello, I'm a medical Q&A model. You can ask me anything related to healthcare and I will try to help you.",
            }
        ],
        name: 'q&a_model',
    }
];

export type UserData = (typeof conversations)[number];

export const loggedInUserData = {
    username: sessionStorage.getItem('username'),
};

export interface Message {
    id: number;
    name: string;
    message: string;
}

export function ChatLayout() {
    const [isMobile] = useState(false);
    const [selectedModel, setSelectedModel] = useState("0");
    const [dummyState, setDummyState] = useState(0);
    const handleStorageChange = () => {
        const modelID = sessionStorage.getItem('model') ?? "0";
        setSelectedModel(modelID);
    }

    useEffect(() => {
        window.addEventListener('storage', handleStorageChange);
        handleStorageChange();
        return () => {
            window.removeEventListener('storage', handleStorageChange);
        };
    }, []);

    useEffect(() => {
        setDummyState((prev) => prev + 1);
    }, [selectedModel]);

    return (<div>
        <Chat
            messages={conversations[parseInt(selectedModel)].messages}
            selectedUser={conversations[parseInt(selectedModel)]}
            isMobile={isMobile}
            key={dummyState}/>
    </div>);
}
