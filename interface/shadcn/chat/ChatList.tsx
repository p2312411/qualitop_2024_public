import {Message, UserData} from "./ChatLayout.tsx";
import {cn} from "../lib/utils";
import {useEffect, useRef, useState} from "react";
import {Avatar, AvatarFallback} from "../components/ui/avatar";
import ChatBottomBar from "./ChatBottomBar.tsx";
import {AnimatePresence, motion} from "framer-motion";
import {extractAndConcatToUpper} from "../../src/components/UserNavBar.tsx";
import qualitop from "../../src/assets/qualitop-logo-cropped.png"
import {ScrollArea} from "../components/ui/scroll-area.tsx";

interface ChatListProps {
    messages: Message[];
    selectedUser: UserData;
    sendMessage: (newMessage: Message) => void;
    isMobile: boolean;
}

export function ChatList({
                             messages, selectedUser, sendMessage, isMobile
                         }: ChatListProps) {
    const messagesContainerRef = useRef<HTMLDivElement>(null);
    // @ts-ignore
    const name: string = sessionStorage.getItem("name");
    const [displayedText, setDisplayedText] = useState<string>("");

    const ScrollToBottom = () => {
        if (messagesContainerRef.current) {
            messagesContainerRef.current.scrollIntoView(false);
        }
    };

    useEffect(() => {
        ScrollToBottom();
    }, [messages]);

    useEffect(() => {
        if (messages && messages.length > 0) {
            const lastMessage = messages[messages.length - 1];
            if (lastMessage.name === selectedUser.name) {
                let currentText = "";
                let index = 0;

                const intervalId = setInterval(() => {
                    currentText += lastMessage.message[index];
                    setDisplayedText(currentText);
                    index++;
                    ScrollToBottom();
                    if (index === lastMessage.message.length) {
                        clearInterval(intervalId);
                    }
                }, 30);

                return () => clearInterval(intervalId);
            } else {
                setDisplayedText(lastMessage.message);
            }
        }
    }, [messages]);

    return (<div className="flex flex-col w-full h-full">
        <ScrollArea className="w-full h-[68vh]">
            <div ref={messagesContainerRef}
                 className="w-full overflow-y-auto overflow-x-hidden h-full flex flex-col">
                <AnimatePresence>
                    {messages?.map((message, index) => (<motion.div
                        key={index}
                        layout
                        initial={{opacity: 0, scale: 1, y: 50, x: 0}}
                        animate={{opacity: 1, scale: 1, y: 0, x: 0}}
                        exit={{opacity: 0, scale: 1, y: 1, x: 0}}
                        transition={{
                            opacity: {duration: 0.1}, layout: {
                                type: "spring", bounce: 0.3, duration: messages.indexOf(message) * 0.05 + 0.2,
                            },
                        }}
                        style={{
                            originX: 0.5, originY: 0.5,
                        }}
                        className={cn("flex flex-col gap-2 p-4 whitespace-pre-wrap", message.name !== selectedUser.name ? "items-end" : "items-start")}
                    >
                        <div className="flex gap-3 items-center">
                            {message.name === selectedUser.name && (
                                <Avatar className="flex justify-center items-center">
                                    <img src={qualitop}/>
                                </Avatar>)}
                            <span className="bg-accent p-3 rounded-md max-w-lg text-xl">
                    {message === messages[messages.length - 1] ? displayedText === "processing your request..." ? (
                        <span className="loading">processing your request<span className="ellipsis"></span></span>
                    ) : (displayedText) : message.message}
                  </span>
                            {message.name !== selectedUser.name && (
                                <Avatar className="flex justify-center items-center">
                                    <AvatarFallback>{extractAndConcatToUpper(name)}</AvatarFallback>
                                </Avatar>)}
                        </div>
                    </motion.div>))}
                </AnimatePresence>
            </div>
        </ScrollArea>
        <ChatBottomBar messages={messages} sendMessage={sendMessage} isMobile={isMobile}/>
    </div>);
}
