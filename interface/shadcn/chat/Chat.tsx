import {ChatList} from "./ChatList.tsx";
import {useState} from "react";
import {Message, UserData} from "./ChatLayout.tsx";

interface ChatProps {
    messages?: Message[];
    selectedUser: UserData;
    isMobile: boolean;
}

export function Chat({messages, selectedUser, isMobile}: ChatProps) {
    const [messagesState, setMessages] = useState<Message[]>(
        messages ?? []
    );

    const sendMessage = (newMessage: Message) => {
        if (newMessage.message !== "processing your request..." && (newMessage.name == "q&a_model" || newMessage.name == "monitoring_model")) {
            setMessages((prevMessages) => [...prevMessages.slice(0, -1), newMessage]);
        } else {
            setMessages((prevMessages) => [...prevMessages, newMessage]);
        }
    };

    return (
        <div className="flex flex-col justify-between w-full">
            <div className="flex w-full h-fit">
                <ChatList
                    messages={messagesState}
                    selectedUser={selectedUser}
                    sendMessage={sendMessage}
                    isMobile={isMobile}
                />
            </div>
        </div>
    );
}
