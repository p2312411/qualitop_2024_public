package com.switcher.switcherservice.dto;


import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

@Validated
@Getter
@Setter
public class SaillantDTO {

    private String best_response_otherM;
    private String mean_medications;
    private String date_of_birth;
    private String i_am_content_with_the_qual;
    private String thinking_back_to_the_yearn;
    private String mean_therapies;
    private String immuno_frequency;
    private String worrying_thoughts_go_throu;
    private String thinking_back_to_the_yearh;
    private String i_get_a_sort_of_frightened;

    public SaillantDTO(String best_response_otherM, String mean_medications, String date_of_birth, String i_am_content_with_the_qual, String thinking_back_to_the_yearn, String mean_therapies, String immuno_frequency, String worrying_thoughts_go_throu, String thinking_back_to_the_yearh, String i_get_a_sort_of_frightened) {
        this.best_response_otherM = best_response_otherM;
        this.mean_medications = mean_medications;
        this.date_of_birth = date_of_birth;
        this.i_am_content_with_the_qual = i_am_content_with_the_qual;
        this.thinking_back_to_the_yearn = thinking_back_to_the_yearn;
        this.mean_therapies = mean_therapies;
        this.immuno_frequency = immuno_frequency;
        this.worrying_thoughts_go_throu = worrying_thoughts_go_throu;
        this.thinking_back_to_the_yearh = thinking_back_to_the_yearh;
        this.i_get_a_sort_of_frightened = i_get_a_sort_of_frightened;
    }

    public SaillantDTO() {

    }

    @Override
    public String toString() {
        return "SaillantDTO{" +
                "best_response_otherM='" + best_response_otherM + '\'' +
                ", mean_medications='" + mean_medications + '\'' +
                ", date_of_birth='" + date_of_birth + '\'' +
                ", i_am_content_with_the_qual='" + i_am_content_with_the_qual + '\'' +
                ", thinking_back_to_the_yearn='" + thinking_back_to_the_yearn + '\'' +
                ", mean_therapies='" + mean_therapies + '\'' +
                ", immuno_frequency='" + immuno_frequency + '\'' +
                ", worrying_thoughts_go_throu='" + worrying_thoughts_go_throu + '\'' +
                ", thinking_back_to_the_yearh='" + thinking_back_to_the_yearh + '\'' +
                ", i_get_a_sort_of_frightened='" + i_get_a_sort_of_frightened + '\'' +
                '}';
    }
}
