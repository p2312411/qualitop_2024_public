package com.switcher.switcherservice.dto;

import lombok.Getter;
import org.springframework.validation.annotation.Validated;

import java.util.ArrayList;
import java.util.Arrays;

@Validated
@Getter
public class QueryDetails {
    private String[] attributes;
    private String[] from;
    private QueryCondition[] where;
    private String[] group_by;
    private String[] order_by;
    private String[] aggregate;
    private String[] limit;

    @Override
    public String toString() {
        return "{" +
                "attributes=" + Arrays.toString(attributes) +
                ", from=" + Arrays.toString(from) +
                ", where=" + Arrays.toString(where) +
                ", groupBy=" + Arrays.toString(group_by) +
                ", orderBy=" + Arrays.toString(order_by) +
                ", aggregate=" + Arrays.toString(aggregate) +
                ", limit=" + Arrays.toString(limit) +
                '}';
    }
}