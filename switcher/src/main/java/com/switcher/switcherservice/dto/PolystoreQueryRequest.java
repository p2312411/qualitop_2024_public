package com.switcher.switcherservice.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
public class PolystoreQueryRequest {
    private String username;
    private String password;
    private QueryDetails query;

    public PolystoreQueryRequest(String username, String password, QueryDetails query) {
        this.username = username;
        this.password = password;
        this.query = query;
    }

    @Override
    public String toString() {
        return "PolystoreQueryRequest{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", query='" + query.toString() + '\'' +
                '}';
    }
}
