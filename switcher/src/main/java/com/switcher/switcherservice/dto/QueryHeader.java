package com.switcher.switcherservice.dto;


import lombok.Getter;
import org.springframework.validation.annotation.Validated;

@Validated
@Getter
public class QueryHeader {

    public String name;
    public String dataType;
    public QuerySort sort;
    public String filter;
    public boolean primary;
    public boolean nullable;

    public QueryHeader() {
    }

    @Override
    public String toString() {
        return "{" +
                "name=" + name +
                ", dataType=" + dataType +
                ", sort=" + sort +
                ", filter=" + filter +
                ", primary=" + primary +
                ", nullable=" + nullable +
                '}';
    }
}
