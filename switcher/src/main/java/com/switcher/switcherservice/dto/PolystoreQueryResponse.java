package com.switcher.switcherservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.util.*;


@Getter
public class PolystoreQueryResponse {

    private QueryHeader[] header;
    private List<Map<String, String>> data;

    public PolystoreQueryResponse() {
    }

    @Override
    public String toString() {
        return "{" +
                "header=" + header +
                ", data=" + data +
                '}';
    }

    @JsonProperty("data")
    private void unpackData(String[][] dataArray) {
        data = new ArrayList<>();
        for (String[] row : dataArray) {
            Map<String, String> rowMap = new HashMap<>();
            for (int i = 0; i < header.length; i++) {
                rowMap.put(header[i].getName(), row[i]);
            }
            data.add(rowMap);
        }
    }

    private List<String> getColumnNames() {
        List<String> columnNames = new ArrayList<>();
        for (QueryHeader header : this.getHeader()) {
            columnNames.add(header.getName());
        }
        return columnNames;
    }
}
