package com.switcher.switcherservice.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Setter
@Getter
@Validated
public class QueryRequestDTO {

    private String api_key;
    private String username;
    private String password;
    private QueryDetails query;
    private String params;
    private String[] models;

    public QueryRequestDTO() {
    }

    @Override
    public String toString() {
        return "QueryRequestDTO{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", query='" + query.toString() + '\'' +
                ", params='" + params + '\'' +
                ", models='" + models + '\'' +
                '}';
    }
}
