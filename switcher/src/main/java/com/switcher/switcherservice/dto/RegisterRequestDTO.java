package com.switcher.switcherservice.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RegisterRequestDTO {

    private String username;
    private String password;
    private String[] roles;

    @Override
    public String toString() {
        return "LoginRequestDTO{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", roles=" + roles + '\'' +
                '}';
    }
}
