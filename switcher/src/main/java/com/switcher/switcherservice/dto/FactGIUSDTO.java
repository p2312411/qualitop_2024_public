package com.switcher.switcherservice.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;


@Validated
@Getter
@Setter
public class FactGIUSDTO {

        private String ius_score;
        private String age_at_immunotherapy;
        private String sex;
        private String cancer_type;
        private String education_level;
        private String financial_status;

        public FactGIUSDTO(String ius_score, String age_at_immunotherapy, String sex, String cancer_type, String education_level, String financial_status) {
                this.ius_score = ius_score;
                this.age_at_immunotherapy = age_at_immunotherapy;
                this.sex = sex;
                this.cancer_type = cancer_type;
                this.education_level = education_level;
                this.financial_status = financial_status;
        }

        public FactGIUSDTO() {
        }
}
