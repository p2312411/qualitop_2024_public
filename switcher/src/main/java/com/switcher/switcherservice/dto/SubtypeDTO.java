package com.switcher.switcherservice.dto;


import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

@Validated
@Getter
@Setter
public class SubtypeDTO {
    private String age;
    private String sex;
    private String height;
    private String weight;
    private String glucose;
    private String redBloodCell;
    private String bSymptoms;
    private String lactate;
    private String cReactiveProtein;
    private String hemoglobin;
    private String whiteBloodCell;
    private String lymphocites;


    public SubtypeDTO(String age, String sex, String height, String weight, String glucose, String redBloodCell, String bSymptoms, String lactate, String cReactiveProtein, String hemoglobin, String whiteBloodCell, String lymphocites) {
        this.age = age;
        this.sex = sex;
        this.height = height;
        this.weight = weight;
        this.glucose = glucose;
        this.redBloodCell = redBloodCell;
        this.bSymptoms = bSymptoms;
        this.lactate = lactate;
        this.cReactiveProtein = cReactiveProtein;
        this.hemoglobin = hemoglobin;
        this.whiteBloodCell = whiteBloodCell;
        this.lymphocites = lymphocites;
    }

    public SubtypeDTO() {
    }

    @Override
    public String toString() {
        return "SubtypeDTO{" +
                "age=" + age +
                ", sex=" + sex +
                ", height=" + height +
                ", weight=" + weight +
                ", glucose=" + glucose +
                ", redBloodCell=" + redBloodCell +
                ", bSymptoms=" + bSymptoms +
                ", lactate=" + lactate +
                ", cReactiveProtein=" + cReactiveProtein +
                ", hemoglobin=" + hemoglobin +
                ", whiteBloodCell=" + whiteBloodCell +
                ", lymphocites=" + lymphocites +
                '}';
    }
}
