package com.switcher.switcherservice.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class QueryResponseDTO {

    private String status;
    private String data;
    private String results;

    public QueryResponseDTO() {
    }

    public QueryResponseDTO(String status, String data, String results) {
        this.status = status;
        this.data = data;
        this.results = results;
    }

    @Override
    public String toString() {
        return "QueryRequestDTO{" + "data=" + data + ", results=" + results + '}';
    }
}
