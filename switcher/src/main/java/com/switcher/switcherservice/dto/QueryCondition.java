package com.switcher.switcherservice.dto;

import lombok.Getter;
import org.springframework.validation.annotation.Validated;

import java.util.ArrayList;

@Getter
@Validated
public class QueryCondition {
    private String attribute;
    private String operator;
    private String value;


    @Override
    public String toString() {
        return "{" +
                "attribute='" + attribute + '\'' +
                ", operator='" + operator + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
