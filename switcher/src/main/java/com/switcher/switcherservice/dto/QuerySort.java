package com.switcher.switcherservice.dto;


import lombok.Getter;
import org.springframework.validation.annotation.Validated;

@Validated
@Getter
public class QuerySort {

    public String direction;
    public boolean sorting;

    public QuerySort() {
    }

    @Override
    public String toString() {
        return "{" +
                "direction=" + direction +
                ", sorting=" + sorting +
                '}';
    }
}
