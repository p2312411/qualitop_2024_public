package com.switcher.switcherservice.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

@Validated
@Getter
@Setter
public class RetrievalQueryDTO {

    private String username;
    public String query;
    private String new_chat;

    public RetrievalQueryDTO(String username, String query, String new_chat) {
        this.username = username;
        this.query = query;
        this.new_chat = new_chat;
    }
    public RetrievalQueryDTO() {
    }

    @Override
    public String toString() {
        return "RetrievalQueryDTO{" +
                "username='" + username + '\'' +
                ", query='" + query + '\'' +
                ", new_chat='" + new_chat + '\'' +
                '}';
    }



}
