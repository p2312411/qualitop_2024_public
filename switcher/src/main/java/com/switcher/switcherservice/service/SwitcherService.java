package com.switcher.switcherservice.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.switcher.switcherservice.dto.*;
import com.switcher.switcherservice.response.ErrorResponse;
import com.switcher.switcherservice.response.ResponseMessage;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@Service
@Slf4j
@Setter
@Getter
public class SwitcherService {

    @Value("${POLYSTORE_URL}")
    private String POLYSTORE_URL;

    @Value("${MODEL1_URL}")
    private String MODEL1_URL;

    @Value("${MODEL2_URL}")
    private String MODEL2_URL;

    @Value("${MODEL3_URL}")
    private String MODEL3_URL;

    @Value("${RETRIEVAL_URL}")
    private String RETRIEVAL_URL;

    @Value("${API_KEY}")
    private String API_KEY;

    private HttpHeaders headers = new HttpHeaders();

    @Autowired
    private RestTemplate restTemplate;

    private ObjectMapper objectMapper = new ObjectMapper();


    public SwitcherService() {

        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("api_key", this.API_KEY);
        headers.set("Content-Type", "application/json");
        headers.set("user-agent", "Java/1.8.0_181");
        headers.set("accept", "*/*");
        headers.set("accept-encoding", "gzip, deflate, br");
        headers.set("connection", "keep-alive");
    }

    public String polystoreRegister(RegisterRequestDTO registerRequestDTO) throws JsonProcessingException {
        HttpEntity<RegisterRequestDTO> RequestEntity = new HttpEntity<>(registerRequestDTO, headers);
        log.info("Registering in to the polystore : " + registerRequestDTO.toString());
        try {
            ResponseEntity<String> response = restTemplate.postForEntity(POLYSTORE_URL + "/auth/user/signup", RequestEntity, String.class);
            ResponseMessage successResponse = new ResponseMessage(response.getStatusCodeValue(), response.getBody());
            return objectMapper.writeValueAsString(successResponse);
        } catch (HttpClientErrorException e) {
            log.error("Client error while registering in to the polystore: ", e);
            ErrorResponse errorResponse = new ErrorResponse("Client error", e.getRawStatusCode(), "User already exists.");
            return objectMapper.writeValueAsString(errorResponse);
        } catch (Exception e) {
            log.error("Error while registering in to the polystore: ", e);
            ErrorResponse errorResponse = new ErrorResponse("Server error", 500, "Registering failed due to server error");
            return objectMapper.writeValueAsString(errorResponse);
        }
    }

    public String polystoreLogin(LoginRequestDTO loginRequestDTO) throws JsonProcessingException {
        HttpEntity<LoginRequestDTO> RequestEntity = new HttpEntity<>(loginRequestDTO, headers);
        log.info("Logging in to the polystore : " + loginRequestDTO.toString());
        try {
            ResponseEntity<String> response = restTemplate.postForEntity(POLYSTORE_URL + "/auth/user/login", RequestEntity, String.class);
            ResponseMessage successResponse = new ResponseMessage(response.getStatusCodeValue(), response.getBody());
            return objectMapper.writeValueAsString(successResponse);
        } catch (HttpClientErrorException e) {
            log.error("Client error while logging in to the polystore: ", e);
            ErrorResponse errorResponse = new ErrorResponse("Client error", e.getRawStatusCode(), "Credentials invalid. User not found.");
            return objectMapper.writeValueAsString(errorResponse);
        } catch (Exception e) {
            log.error("Error while logging in to the polystore: ", e);
            ErrorResponse errorResponse = new ErrorResponse("Server error", 500, "Login failed due to server error");
            return objectMapper.writeValueAsString(errorResponse);
        }
    }

    public String getPolystoreData(PolystoreQueryRequest polystoreQueryRequest) {

        HttpEntity<PolystoreQueryRequest> RequestEntity = new HttpEntity<>(polystoreQueryRequest, headers);
        log.info("Fetching data from the polystore : " + polystoreQueryRequest.toString());
        try {
            return restTemplate.postForObject(POLYSTORE_URL + "/query/sql", RequestEntity, String.class);
        } catch (Exception e) {
            log.error("Error while fetching data from the polystore : ", e);
            return "Error while fetching data";
        }
    }

    public List<Map<String, String>> formatPolystoreResponse(String json) {
        log.info("Formatting polystore response : " + json);
        try {
            List<PolystoreQueryResponse> containers = objectMapper.readValue(json, objectMapper.getTypeFactory().constructCollectionType(List.class, PolystoreQueryResponse.class));
            List<Map<String, String>> data = containers.get(0).getData();
            log.info("Polystore data : " + data.toString());
            return data;
        } catch (Exception e) {
            log.error("Error while formatting polystore response : ", e);
            return new ArrayList<>();
        }
    }

    @Async("asyncTaskExecutor")
    public CompletableFuture<String> predictFactGIUS(FactGIUSDTO factGIUSDTO) {
        try {
            HttpEntity<FactGIUSDTO> RequestEntity = new HttpEntity<>(factGIUSDTO, headers);
            log.info("Prediction request with model 1 : " + factGIUSDTO.toString());
            String response = restTemplate.postForObject(this.MODEL1_URL + "/predict_FACT_G_IUS", RequestEntity, String.class);
            log.info("Predicting with model 1 : " + response);
            return CompletableFuture.completedFuture(response);
        } catch (Exception e) {
            log.error("Error while predicting with model 1 : ", e);
            return CompletableFuture.completedFuture("Error while predicting with model 1");
        }
    }

    @Async("asyncTaskExecutor")
    public CompletableFuture<String> predictFactGDPR(FactGDPRDTO factGDPRDTO) {
        try {
            HttpEntity<FactGDPRDTO> RequestEntity = new HttpEntity<>(factGDPRDTO, headers);
            log.info("Prediction request with model 1 : " + factGDPRDTO.toString());
            String response = restTemplate.postForObject(this.MODEL1_URL + "/predict_FACT_G_DPR", RequestEntity, String.class);
            log.info("Predicting with model 1 : " + response);
            return CompletableFuture.completedFuture(response);
        } catch (Exception e) {
            log.error("Error while predicting with model 1 : ", e);
            return CompletableFuture.completedFuture("Error while predicting with model 1");
        }
    }

    @Async("asyncTaskExecutor")
    public CompletableFuture<String> predictFactGTRH(FactGTRHDTO factGTRHDTO) {
        try {
            HttpEntity<FactGTRHDTO> RequestEntity = new HttpEntity<>(factGTRHDTO, headers);
            log.info("Prediction request with model 1 : " + factGTRHDTO.toString());
            String response = restTemplate.postForObject(this.MODEL1_URL + "/predict_FACT_G_TRH", RequestEntity, String.class);
            log.info("Predicting with model 1 : " + response);
            return CompletableFuture.completedFuture(response);
        } catch (Exception e) {
            log.error("Error while predicting with model 1 : ", e);
            return CompletableFuture.completedFuture("Error while predicting with model 1");
        }
    }

    @Async("asyncTaskExecutor")
    public CompletableFuture<String> predictFactGHLS(FactGHLSDTO factGHLSDTO) {
        try {
            HttpEntity<FactGHLSDTO> RequestEntity = new HttpEntity<>(factGHLSDTO, headers);
            log.info("Prediction request with model 1 : " + factGHLSDTO.toString());
            String response = restTemplate.postForObject(this.MODEL1_URL + "/predict_FACT_G_HLS", RequestEntity, String.class);
            log.info("Predicting with model 1 : " + response);
            return CompletableFuture.completedFuture(response);
        } catch (Exception e) {
            log.error("Error while predicting with model 1 : ", e);
            return CompletableFuture.completedFuture("Error while predicting with model 1");
        }
    }

    @Async("asyncTaskExecutor")
    public CompletableFuture<String> predictFactGTSS(FactGTSSDTO factGTSSDTO) {
        try {
            HttpEntity<FactGTSSDTO> RequestEntity = new HttpEntity<>(factGTSSDTO, headers);
            log.info("Prediction request with model 1 : " + factGTSSDTO.toString());
            String response = restTemplate.postForObject(this.MODEL1_URL + "/predict_FACT_G_TSS", RequestEntity, String.class);
            log.info("Predicting with model 1 : " + response);
            return CompletableFuture.completedFuture(response);
        } catch (Exception e) {
            log.error("Error while predicting with model 1 : ", e);
            return CompletableFuture.completedFuture("Error while predicting with model 1");
        }
    }

    public CompletableFuture<String> predictSubtype(SubtypeDTO subtypeDTO) {

        try {
            HttpEntity<SubtypeDTO> RequestEntity = new HttpEntity<>(subtypeDTO, headers);
            log.info("Prediction request with model 2 : " + subtypeDTO.toString());
            String response = restTemplate.postForObject(this.MODEL2_URL + "/subtypes", RequestEntity, String.class);
            log.info("Predicting with model 2 : " + response);
            return CompletableFuture.completedFuture(response);
        } catch (Exception e) {
            log.error("Error while predicting with model 2 : ", e);
            return CompletableFuture.completedFuture("Error while predicting with model 2");
        }
    }

    @Async("asyncTaskExecutor")
    public CompletableFuture<String> predictSaillant(SaillantDTO saillantDTO) {
        try {
            HttpEntity<SaillantDTO> RequestEntity = new HttpEntity<>(saillantDTO, headers);
            log.info("Prediction request with model 3 : " + saillantDTO.toString());
            String response = restTemplate.postForObject(this.MODEL3_URL + "/predict_saillant", RequestEntity, String.class);
            log.info("Predicting with model 3 : " + response);
            return CompletableFuture.completedFuture(response);
        } catch (Exception e) {
            log.error("Error while predicting with model 3 : ", e);
            return CompletableFuture.completedFuture("Error while predicting with model 3");
        }
    }

    @Async("asyncTaskExecutor")
    public CompletableFuture<String> predictSaillant1(SaillantDTO saillantDTO) {
        try {
            HttpEntity<SaillantDTO> RequestEntity = new HttpEntity<>(saillantDTO, headers);
            log.info("Prediction request with model 3 : " + saillantDTO.toString());
            String response = restTemplate.postForObject(this.MODEL3_URL + "/relationship_with_drug", RequestEntity, String.class);
            log.info("Predicting with model 3 : " + response);
            return CompletableFuture.completedFuture(response);
        } catch (Exception e) {
            log.error("Error while predicting with model 3 : ", e);
            return CompletableFuture.completedFuture("Error while predicting with model 3");
        }
    }

    @Async("asyncTaskExecutor")
    public CompletableFuture<String> predictSaillant2(SaillantDTO saillantDTO) {
        try {
            HttpEntity<SaillantDTO> RequestEntity = new HttpEntity<>(saillantDTO, headers);
            log.info("Prediction request with model 3 : " + saillantDTO.toString());
            String response = restTemplate.postForObject(this.MODEL3_URL + "/followup", RequestEntity, String.class);
            log.info("Predicting with model 3 : " + response);
            return CompletableFuture.completedFuture(response);
        } catch (Exception e) {
            log.error("Error while predicting with model 3 : ", e);
            return CompletableFuture.completedFuture("Error while predicting with model 3");
        }
    }

    @Async("asyncTaskExecutor")
    public CompletableFuture<String> predictSaillant3(SaillantDTO saillantDTO) {
        try {
            HttpEntity<SaillantDTO> RequestEntity = new HttpEntity<>(saillantDTO, headers);
            log.info("Prediction request with model 3 : " + saillantDTO.toString());
            String response = restTemplate.postForObject(this.MODEL3_URL + "/content_qual", RequestEntity, String.class);
            log.info("Predicting with model 3 : " + response);
            return CompletableFuture.completedFuture(response);
        } catch (Exception e) {
            log.error("Error while predicting with model 3 : ", e);
            return CompletableFuture.completedFuture("Error while predicting with model 3");
        }
    }

    @Async("asyncTaskExecutor")
    public CompletableFuture<String> processParse(RetrievalQueryDTO retrievalQueryDTO) {
        try {
            HttpEntity<RetrievalQueryDTO> RequestEntity = new HttpEntity<>(retrievalQueryDTO, headers);
            log.info("Prediction request with parse : " + retrievalQueryDTO.toString());
            String response = restTemplate.postForObject(this.RETRIEVAL_URL + "/api/chat/parse", RequestEntity, String.class);
            log.info("Predicting with parse : " + response);
            return CompletableFuture.completedFuture(response);
        } catch (Exception e) {
            log.error("Error while predicting with parse : ", e);
            return CompletableFuture.completedFuture("Error while predicting with parse");
        }
    }

    @Async("asyncTaskExecutor")
    public CompletableFuture<String> processParseV2(RetrievalQueryDTO retrievalQueryDTO) {
        try {
            HttpEntity<RetrievalQueryDTO> RequestEntity = new HttpEntity<>(retrievalQueryDTO, headers);
            log.info("Prediction request with parse : " + retrievalQueryDTO.toString());
            String response = restTemplate.postForObject(this.RETRIEVAL_URL + "/api/chat/parse_v2", RequestEntity, String.class);
            log.info("Predicting with parse : " + response);
            return CompletableFuture.completedFuture(response);
        } catch (Exception e) {
            log.error("Error while predicting with parse : ", e);
            return CompletableFuture.completedFuture("Error while predicting with parse");
        }
    }

    @Async("asyncTaskExecutor")
    public CompletableFuture<String> processAnswer(RetrievalQueryDTO retrievalQueryDTO) {
        try {
            HttpEntity<RetrievalQueryDTO> RequestEntity = new HttpEntity<>(retrievalQueryDTO, headers);
            log.info("Prediction request with answer : " + retrievalQueryDTO.toString());
            String response = restTemplate.postForObject(this.RETRIEVAL_URL + "/api/chat/answer", RequestEntity, String.class);
            log.info("Predicting with answer : " + response);
            return CompletableFuture.completedFuture(response);
        } catch (Exception e) {
            log.error("Error while predicting with answer : ", e);
            return CompletableFuture.completedFuture("Error while predicting with answer");
        }
    }

    public static <T> T createDTO(Class<T> dtoClass, List<JSONModeDTO.AttributeDTO> attributes) throws IllegalAccessException, InstantiationException {
        T dto = dtoClass.newInstance();

        for (JSONModeDTO.AttributeDTO attribute : attributes) {
            Field field = getField(dtoClass, attribute.getAttribute());
            if (field != null) {
                field.setAccessible(true);
                field.set(dto, attribute.getValue());
            }
        }

        return dto;
    }

    private static Field getField(Class<?> clazz, String fieldName) {
        try {
            return clazz.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            Class<?> superClass = clazz.getSuperclass();
            if (superClass == null) {
                return null;
            } else {
                return getField(superClass, fieldName);
            }
        }
    }
}
