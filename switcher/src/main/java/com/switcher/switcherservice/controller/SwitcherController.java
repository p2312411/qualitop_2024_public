package com.switcher.switcherservice.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.switcher.switcherservice.dto.*;
import lombok.extern.slf4j.Slf4j;

import com.switcher.switcherservice.service.SwitcherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import static com.switcher.switcherservice.service.SwitcherService.createDTO;

@RestController
@Slf4j
@CrossOrigin(origins = "*")
public class SwitcherController {

    @Autowired
    SwitcherService switcherService;

    ObjectMapper objectMapper = new ObjectMapper();


    @PostMapping("/register")
    public String processRegister(@Validated @RequestBody RegisterRequestDTO registerRequestDTO) throws JsonProcessingException {
        log.info("Processing register request : " + registerRequestDTO.toString());
        return switcherService.polystoreRegister(registerRequestDTO);
    }

    @PostMapping("/login")
    public String processLogin(@Validated @RequestBody LoginRequestDTO loginRequestDTO) throws JsonProcessingException {
        log.info("Processing login request : " + loginRequestDTO.toString());
        return switcherService.polystoreLogin(loginRequestDTO);
    }


    @PostMapping("/predict")
    public QueryResponseDTO processPredictionRequest(@Validated @RequestBody QueryRequestDTO queryRequestDTO) throws JsonProcessingException {
        log.info("Processing prediction request : " + queryRequestDTO.toString());

        String username = queryRequestDTO.getUsername();
        String password = queryRequestDTO.getPassword();
        QueryDetails queryDetails = queryRequestDTO.getQuery();

        PolystoreQueryRequest polystoreQueryRequest = new PolystoreQueryRequest(username, password, queryDetails);

        List<Map<String, String>> dataPolystore;

        try {
            String responsePolystore = switcherService.getPolystoreData(polystoreQueryRequest);
            dataPolystore = switcherService.formatPolystoreResponse(responsePolystore);

        } catch (Exception e) {
            log.error("Error occurred while fetching data from the polystore :", e);
            return new QueryResponseDTO("error : " + e.getMessage(), "", "");
        }

        String dataPolystoreJson = objectMapper.writeValueAsString(dataPolystore);

        QueryResponseDTO response = new QueryResponseDTO();
        response.setStatus("success");
        response.setData(dataPolystoreJson);

        return response;
    }

    @PostMapping("/predict_FACT_G_IUS")
    public CompletableFuture<String> processfactg1(@Validated @RequestBody FactGIUSDTO factGIUSDTO) {
        log.info("Processing register request : " + factGIUSDTO.toString());
        return switcherService.predictFactGIUS(factGIUSDTO);
    }

    @PostMapping("/predict_FACT_G_DPR")
    public CompletableFuture<String> processfactg2(@Validated @RequestBody FactGDPRDTO factGDPRDTO) {
        log.info("Processing register request : " + factGDPRDTO.toString());
        return switcherService.predictFactGDPR(factGDPRDTO);
    }

    @PostMapping("/predict_FACT_G_TRH")
    public CompletableFuture<String> processfactg3(@Validated @RequestBody FactGTRHDTO factGTRHDTO) {
        log.info("Processing register request : " + factGTRHDTO.toString());
        return switcherService.predictFactGTRH(factGTRHDTO);
    }

    @PostMapping("/predict_FACT_G_HLS")
    public CompletableFuture<String> processfactg4(@Validated @RequestBody FactGHLSDTO factGHLSDTO) {
        log.info("Processing register request : " + factGHLSDTO.toString());
        return switcherService.predictFactGHLS(factGHLSDTO);
    }

    @PostMapping("/predict_FACT_G_TSS")
    public CompletableFuture<String> processfactg5(@Validated @RequestBody FactGTSSDTO factGTSSDTO) {
        log.info("Processing register request : " + factGTSSDTO.toString());
        return switcherService.predictFactGTSS(factGTSSDTO);
    }

    @PostMapping("/subtypes")
    public CompletableFuture<String> processsubtypes(@Validated @RequestBody SubtypeDTO subtypeDTO) {
        log.info("Processing register request : " + subtypeDTO.toString());
        return switcherService.predictSubtype(subtypeDTO);
    }

    @PostMapping("/predict_saillant")
    public CompletableFuture<String> processsaillant(@Validated @RequestBody SaillantDTO saillantDTO) {
        log.info("Processing register request : " + saillantDTO.toString());
        return switcherService.predictSaillant(saillantDTO);
    }

    @PostMapping("/relationship_with_drug")
    public CompletableFuture<String> processsaillant1(@Validated @RequestBody SaillantDTO saillantDTO) {
        log.info("Processing register request : " + saillantDTO.toString());
        return switcherService.predictSaillant1(saillantDTO);
    }

    @PostMapping("/followup")
    public CompletableFuture<String> processsaillant2(@Validated @RequestBody SaillantDTO saillantDTO) {
        log.info("Processing register request : " + saillantDTO.toString());
        return switcherService.predictSaillant2(saillantDTO);
    }

    @PostMapping("/content_qual")
    public CompletableFuture<String> processsaillant3(@Validated @RequestBody SaillantDTO saillantDTO) {
        log.info("Processing register request : " + saillantDTO.toString());
        return switcherService.predictSaillant3(saillantDTO);
    }

    @PostMapping("/api/chat/parse_v2")
    public CompletableFuture<String> processRetrievalRequest(@Validated @RequestBody RetrievalQueryDTO query) {
        log.info("Processing parse_v2 retrieval request : " + query.toString());
        try {

            String prompt_factg = "Based on this : When a patient has a total FACT-G score of 27 or lower, he has a poor QoL\n" +
                    "When a patient has a score from 28 to 53, he has a moderately poor QoL\n" +
                    "When a patient has a score from 54 to 80, he has a moderately high QoL\n" +
                    "When a patient has a score of 81 or higher, he has a high QoL , return a summary of these results to the user :";

            String prompt_subtypes = "Based on the list of these questionnaire answers predicted to describe the patient, return a summary of these results to the user :";

            String prompt_saillant = "Based on this # 'relationship_with_drug_nbM' : (0 : the AE is not related to medications, 1 : the AE is related to medications) " +
                    "We calculated this variable as : the count of 'Yes' and 'Possibly' / all values ('Yes', 'No', 'Possibly') " +
                    "# 'followupM' : (1 - 5) As an average, did the immunotherapies give response or did the disease progress? " +
                    "1, Stable disease | 2, Progressive disease | 3, Partial response | 4, Complete response | 5, Not evaluated " +
                    "Calculus : the followup values / the number of followups " +
                    "# 'i_am_content_with_the_qualT' : (1 - 5) Degree of contentment of QoL " +
                    "1, Not at all | 2, A little bit | 3, Somewhat | 4, Quite a bit | 5, Very much " +
                    "Calculus : mean of contentment among all questionnaire times, return a summary of the predicted results to the user :";

            String prompt_error = "Respond to the user that his request is not supported and that he should correct his input and try again";

            CompletableFuture<String> prediction = null;
            RetrievalQueryDTO response = new RetrievalQueryDTO();

            CompletableFuture<String> JSON_response = switcherService.processParseV2(query);
            String responseJson = JSON_response.get();

            JSONModeDTO result = objectMapper.readValue(responseJson, JSONModeDTO.class);
            String responseContent = result.getResponse();
            JSONModeDTO.ResponseDTO responseDTO = objectMapper.readValue(responseContent, JSONModeDTO.ResponseDTO.class);
            String model = responseDTO.getModel();
            List<JSONModeDTO.AttributeDTO> attributes = responseDTO.getAttributes();

            switch (model) {
                case "/predict_FACT_G_IUS":
                    FactGIUSDTO factGIUSDTO = createDTO(FactGIUSDTO.class, attributes);
                    log.info(factGIUSDTO.toString());
                    prediction = switcherService.predictFactGIUS(factGIUSDTO);
                    response.setQuery(prompt_factg + prediction.get());
                    break;
                case "/predict_FACT_G_DPR":
                    FactGDPRDTO factGDPRDTO = createDTO(FactGDPRDTO.class, attributes);
                    prediction = switcherService.predictFactGDPR(factGDPRDTO);
                    response.setQuery(prompt_factg + prediction.get());
                    break;
                case "/predict_FACT_G_TRH":
                    FactGTRHDTO factGTRHDTO = createDTO(FactGTRHDTO.class, attributes);
                    prediction = switcherService.predictFactGTRH(factGTRHDTO);
                    response.setQuery(prompt_factg + prediction.get());
                    break;
                case "/predict_FACT_G_HLS":
                    FactGHLSDTO factGHLSDTO = createDTO(FactGHLSDTO.class, attributes);
                    prediction = switcherService.predictFactGHLS(factGHLSDTO);
                    response.setQuery(prompt_factg + prediction.get());
                    break;
                case "/predict_FACT_G_TSS":
                    FactGTSSDTO factGTSSDTO = createDTO(FactGTSSDTO.class, attributes);
                    prediction = switcherService.predictFactGTSS(factGTSSDTO);
                    response.setQuery(prompt_factg + prediction.get());
                    break;
                case "/subtypes":
                    SubtypeDTO subtypeDTO = createDTO(SubtypeDTO.class, attributes);
                    prediction = switcherService.predictSubtype(subtypeDTO);
                    response.setQuery(prompt_subtypes + prediction.get());
                    break;
                case "/predict_saillant":
                    SaillantDTO saillantDTO = createDTO(SaillantDTO.class, attributes);
                    prediction = switcherService.predictSaillant(saillantDTO);
                    response.setQuery(prompt_saillant + prediction.get());
                    break;
                case "/relationship_with_drug_nbM":
                    SaillantDTO saillantDTO1 = createDTO(SaillantDTO.class, attributes);
                    prediction = switcherService.predictSaillant1(saillantDTO1);
                    response.setQuery(prompt_saillant + prediction.get());
                    break;
                case "/followupM":
                    SaillantDTO saillantDTO2 = createDTO(SaillantDTO.class, attributes);
                    prediction = switcherService.predictSaillant2(saillantDTO2);
                    response.setQuery(prompt_saillant + prediction.get());
                    break;
                case "/i_am_content_with_the_qualT":
                    SaillantDTO saillantDTO3 = createDTO(SaillantDTO.class, attributes);
                    prediction = switcherService.predictSaillant3(saillantDTO3);
                    response.setQuery(prompt_saillant + prediction.get());
                    break;
                default:
                    response.setQuery(prompt_error + "Request not supported");
                    break;
            }

            log.info(response.toString());
            CompletableFuture<String> answer = switcherService.processAnswer(response);
            assert prediction != null;
            String combinedResult = "model: " + model + prediction.get() + " " + answer.get();
            log.info("Combined result : " + combinedResult);
            return CompletableFuture.completedFuture(combinedResult);



        } catch (Exception e) {
            log.error("Error while predicting with answer : ", e);
            return CompletableFuture.completedFuture("Error while predicting with answer");
        }
    }
}
