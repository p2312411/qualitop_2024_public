package com.switcher.switcherservice.response;

public class ResponseMessage {
    public int status;
    public String message;

    public ResponseMessage(int status, String message) {
        this.status = status;
        this.message = message;
    }
}